[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c1c988ebcba94fb1874ea7b54c780300)](https://www.codacy.com?utm_source=gitlab.com&utm_medium=referral&utm_content=Torador/surfer&utm_campaign=Badge_Grade) [![coverage report](https://gitlab.com/Torador/surfer/badges/develop/coverage.svg)](https://gitlab.com/Torador/surfer/-/commits/develop)

![](/public/surfer.png)

# Welcome to Surfer Front-end 🏄‍♂️

Professional Social Network build by **INTECH STUDENT**.

## TEAM

- Jordan Kagmeni (Torador): Technical Lead
- Youcef Laoudi: Developer
- Eddy Koko: Developer

## COORDONATEURS

- BENEDETTI Leonard
- SESSA Romain
- KERBRAT Thomas
- CALDERAN Julien

### STACK 📝

- [Next JS](https://nextjs.org)
- [React](https://fr.reactjs.org)
- [Jest](https://github.com/thetutlage/japa)
- [Tailwind CSS](https://tailwindcss.com)

### SOCIALS MEDIAS AUTH 🪄

- **Google** ✅
- **Facebook** 🕐
- **Twitter** 🕐
- **Linkedin** 🕐
- **Discord** 🕐
- **GitHub** 🕐

## Getting Started

**Note**: After clone the project on your local machine, create a .env.local file if he don't exist and define _NEXT_PUBLIC_HOST_API=YOUR_DOMAIN_API_.
Default: NEXT_PUBLIC_HOST_API=http://localhost:8080/api/v1 for [API-SURFER](https://gitlab.com/Torador/surfer-api)

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Run tests

```bash
npm run test
# or
yarn test
```

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
