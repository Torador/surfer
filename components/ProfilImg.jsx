import { PencilIcon } from '@heroicons/react/outline'
import Image from 'next/image'
import React, { memo, useCallback, useState } from 'react'
import { useSetRecoilState } from 'recoil'
import { currentUserLogin } from '../atoms'
import { DEFAULT_AVATAR } from '../utils/const'

/**
 *  ProfilImg
 * @param {{saveProfil?: () => void, filePickerRef: any, photoUrl: string | ArrayBuffer, isEdit?: boolean }} props
 * @returns {JSX.Element}
 */
function ProfilImg({ saveProfil, filePickerRef, photoUrl, isEdit = true }) {
  const [fileToUpload, setFileToUpload] = useState(null)
  const setCurrentUserInfo = useSetRecoilState(currentUserLogin)

  const onFileChange = useCallback(
    (e) => {
      if (e.target.files.length <= 0) return
      const reader = new FileReader()
      reader.readAsDataURL(e.target.files[0])
      reader.onload = (readerEvent) => {
        const resultBuffer = readerEvent.target.result
        setFileToUpload(resultBuffer)
        setCurrentUserInfo((s) => ({
          ...s,
          accounts: [{ ...s.accounts[0], photo: resultBuffer }],
        }))

        saveProfil && saveProfil()
      }
    },
    [saveProfil, setCurrentUserInfo]
  )

  return (
    <>
      {isEdit && (
        <input
          type="file"
          multiple
          accept="image/*"
          name="picture"
          ref={filePickerRef}
          onChange={onFileChange}
          className="hidden"
        />
      )}
      <Image
        src={fileToUpload || photoUrl || DEFAULT_AVATAR}
        layout="fill"
        alt="picture_account"
        objectFit="cover"
        className="rounded-full"
      />
      {isEdit && (
        <div className="hidden absolute inset-0 group-hover:flex justify-center items-center bg-gray-100 bg-opacity-10 dark:bg-gray-800 dark:bg-opacity-10 rounded-full">
          <button
            onClick={() => filePickerRef.current?.click()}
            title="Change your picture"
            type="button"
            className="p-2 rounded-full text-white bg-indigo-500 hover:bg-indigo-600 transition-colors"
          >
            <PencilIcon className="h-5" />
          </button>
        </div>
      )}
    </>
  )
}

export default memo(ProfilImg)
