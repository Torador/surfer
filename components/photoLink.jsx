//@ts-check
import Link from 'next/link'
import Image from 'next/image'
import { DEFAULT_AVATAR } from '../utils/const'

/**
 * PHOTO LINK
 * @param {{account: Account}} props
 * @returns {JSX.Element}
 */
function PhotoLink({ account }) {
  return (
    <Link href={`/profil/${account.slug}`}>
      <a className="flex-shrink w-14 h-14 relative mr-3 shadow rounded-full border-2 border-white dark:border-gray-800 dark:border-opacity-50">
        <Image
          src={account?.photo || DEFAULT_AVATAR}
          layout="fill"
          alt="profil"
          objectFit="cover"
          className="rounded-full"
        />
      </a>
    </Link>
  )
}

export default PhotoLink
