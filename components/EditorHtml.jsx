import dynamic from 'next/dynamic'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import { useRecoilValue } from 'recoil'
import { accountListState } from '../atoms'

const Editor = dynamic(
  () => import('react-draft-wysiwyg').then((module) => module.Editor),
  {
    ssr: false,
  }
)

function EditorHtml({
  placeholder = 'Enter your content',
  content,
  setContent,
}) {
  const accounts = useRecoilValue(accountListState)
  return (
    <Editor
      editorState={content}
      onEditorStateChange={setContent}
      toolbarClassName="dark:bg-gray-900 dark:border-gray-900 !rounded-lg"
      editorClassName="transition duration-300 focus:outline-none border px-2 border-indigo-100 dark:border-gray-800 rounded-lg shadow-inner focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 bg-indigo-50 dark:bg-gray-800 text-md focus-visible:ring-1 focus-visible:ring-surfer 
      focus-visible:ring-opacity-75 w-full min-h-[300px]"
      placeholder={placeholder}
      mention={{
        separator: ' ',
        trigger: '@',
        suggestions: accounts
          .filter((account) => !!account.shortName)
          .map((account) => ({
            text: account.lastname,
            value: account.shortName,
            url: `/profil/${account.slug}`,
          })),
      }}
    />
  )
}

export default EditorHtml
