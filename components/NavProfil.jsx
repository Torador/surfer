import {
  SearchIcon,
  UserGroupIcon,
  PaperAirplaneIcon,
  MenuIcon,
  HomeIcon,
} from '@heroicons/react/outline'
import Link from 'next/link'
import ModalCreatePost from './ModalCreatePost'

function NavProfil() {
  return (
    <div className="mx-auto max-w-none">
      <div className="flex justify-between max-w-6xl lg:space-x-72 space-x-6 mx-5 xl:mx-auto">
        {/* LEFT */}
        <div className="cursor-pointer flex justify-start relative space-x-1 md:space-x-10 flex-grow items-center animate-fade">
          <Link href="/">
            <a className="text-2xl font-extrabold italic text-indigo-500 tracking-normal flex space-x-2 items-center">
              <span>Surfer</span>
              <span className="text-xs font-semibold bg-indigo-100 px-2 py-1 rounded-full mt-1">
                beta
              </span>
            </a>
          </Link>
        </div>

        {/**MIDDLE - SEARCH INPUT FIELDS  */}
        <div className="max-w-xs">
          <div className="relative mt-1 p-3 rounded-md ">
            <div className="absolute inset-y-0 pl-3 flex items-center pointer-events-none">
              <SearchIcon className="h-5 w-5 text-gray-500" />
            </div>
            <input
              className="bg-gray-100 block w-60 pl-10 sm:text-sm rounded-md border-gray-300 focus:border-indigo-600 focus:ring-indigo-600"
              type="text"
              placeholder="search"
            />
          </div>
        </div>
        {/** RIGHT */}
        <div className="flex items-center justify-end space-x-4">
          <HomeIcon className="navBtn" />
          <MenuIcon className="h-6 md:hidden cursor-pointer" />
          <PaperAirplaneIcon className="navBtn" />
          <ModalCreatePost />
          <UserGroupIcon className="navBtn" />
        </div>
      </div>
    </div>
  )
}

export default NavProfil
