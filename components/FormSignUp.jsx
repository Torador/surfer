import React, { memo } from 'react'
import Input from './Input'
import InputPassword from './InputPassword'
import Loader from './Loader'

/**
 * FORM SIGN UP COMPONENT
 * @param {{ handleSubmit: () => void, isLoading: boolean }} props
 * @returns {JSX.Element}
 */
function FormSignUp({ handleSubmit, isLoading }) {
  return (
    <form onSubmit={handleSubmit} className="flex flex-col space-y-8">
      <div className="flex flex-col space-y-3">
        <Input required type="email" placeholder="Enter email" />
        <Input required placeholder="Enter username" />

        <InputPassword placeholder="Password" />

        <InputPassword
          id="confPass"
          name="confPass"
          placeholder="Confirm Password"
        />
      </div>

      <button
        type="submit"
        className="transform bg-indigo-500 hover:-translate-y-1 hover:shadow-md hover:bg-indigo-600 py-3 px-2 text-white rounded-md transition duration-300 flex justify-center font-semibold w-full shadow-2xl"
      >
        {isLoading ? <Loader /> : 'Sign Up'}
      </button>
    </form>
  )
}

export default memo(FormSignUp)
