//@ts-check
import Image from 'next/image'
import { memo, useCallback, useMemo, useState } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { formatDistanceToNow } from 'date-fns'
import { accountListState, currentUserLogin, postListState } from '../atoms'
import Rating from 'react-rating-stars-component'
import { COLORS_RATING, DEFAULT_AVATAR, DELETE_ENTITY } from '../utils/const'
import Dropdown from './Dropdown'
import {
  DotsVerticalIcon,
  ExclamationIcon,
  PencilIcon,
  StarIcon,
  TrashIcon,
} from '@heroicons/react/outline'
import { endPointDelete, endPointRate } from '../utils/api.config'
import Toast from './Toast'
import { toast } from 'react-toastify'
import Tippy from '@tippyjs/react'
import InteractionUsers from './InteractionUsers'
import useRateAverage from '../utils/hooks/useRateAverage'
import { formatNumber, myDbQuery } from '../utils'
import EditReply from './EditReply'
import LinkPreviewProfil from './LinkPreviewProfil'

/**
 * REPLY COMPONENT
 * @param {{reply: Reply, postId: number, token: string}} props
 * @returns {JSX.Element}
 */
function Reply({ reply, postId, token }) {
  const replies = reply.replies
  const accounts = useRecoilValue(accountListState)
  const user = useRecoilValue(currentUserLogin)
  const [rating, setRating] = useRateAverage(replies)
  const setPostList = useSetRecoilState(postListState)
  const [isEdit, setIsEdit] = useState(false)
  const [account] = useMemo(
    () => accounts.filter((acc) => acc.id === reply.account_id),
    [accounts, reply.account_id]
  )

  const submitRate = useCallback(
    async (rate) => {
      setRating(rate)
      const data = {
        reply_id: reply.id,
        rating: rate,
      }
      try {
        const r = await myDbQuery(endPointRate('reply'), token, data, 'POST')

        if (r.errors || r.error)
          throw new Error(r.error || JSON.stringify(r.errors))
        setPostList((posts) => {
          return posts.map((post) => {
            if (post.id !== postId) return post

            return {
              ...post,
              comments: post.comments.map((cmt) => {
                if (reply.comment_id !== cmt.id) return cmt

                return {
                  ...cmt,
                  replies: cmt.replies.map((repl) => {
                    if (repl.id !== reply.id) return repl

                    return r
                  }),
                }
              }),
            }
          })
        })
      } catch (error) {
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [setRating, reply.id, reply.comment_id, token, setPostList, postId]
  )

  const opts = [
      [
        {
          icon: <ExclamationIcon className="w-5 h-5 mr-2" />,
          label: 'Report',
          action: () => {},
        },
      ],
    ],
    moreOptions = [
      {
        icon: <PencilIcon className="w-5 h-5 mr-2" />,
        label: 'Edit reply',
        action: async () => {
          setIsEdit(true)
        },
      },
      {
        icon: <TrashIcon className="w-5 h-5 mr-2" />,
        label: 'Delete reply',
        color: 'red',
        action: async () => {
          if (!confirm(DELETE_ENTITY('reply'))) return
          setPostList((p) =>
            p.map((p) => {
              if (p.id !== postId) return p
              return {
                ...p,
                comments: p.comments.map((cmt) => {
                  if (cmt.id !== reply.comment_id) return cmt

                  return {
                    ...cmt,
                    replies: cmt.replies.filter((r) => r.id !== reply.id),
                  }
                }),
              }
            })
          )
          try {
            const response = await myDbQuery(
              endPointDelete('reply', reply.id),
              token,
              null,
              'DELETE'
            )
            if (response.errors || response.error)
              throw new Error(response.error || 'ERROR OCCURED !')
            toast(
              <Toast
                title="Système"
                message={`La reponse a bien été supprimée.`}
                type="success"
              />
            )
          } catch (error) {
            toast(
              <Toast title="Système" message={error.message} type="error" />
            )
          }
        },
      },
    ],
    options =
      user.accounts[0].id === reply.account_id ? [...opts, moreOptions] : opts

  const isEdited = reply.created_at.toString() == reply.updated_at.toString()
  return (
    <div
      className={`flex flex-col rounded-md shadow hover:bg-gray-50 dark:shadow-sm dark:border dark:border-gray-800 ${
        isEdit
          ? 'dark:hover:bg-transparent dark:border-gray-700'
          : 'dark:hover:bg-gray-800'
      } dark:hover:bg-opacity-50 transition-colors p-2`}
    >
      <div className="flex items-center justify-between">
        <div className="flex items-center">
          <div className="relative w-8 h-8 rounded-full shadow mr-2">
            <Image
              src={account?.photo || DEFAULT_AVATAR}
              layout="fill"
              className="rounded-full object-cover"
              alt={account.email}
            />
          </div>
          <div className="flex flex-col -space-y-0.5 max-w-[400px]">
            <div className="flex space-x-1 items-center">
              <LinkPreviewProfil account={account} />
              {isEdited ? null : (
                <>
                  <span className="text-gray-500">&middot;</span>
                  <p className="text-gray-500 text-xs flex items-center space-x-1">
                    Edited
                  </p>
                </>
              )}
            </div>
            <p className="text-gray-500 text-xs">
              {formatDistanceToNow(new Date(reply.created_at), {
                addSuffix: true,
              })}
            </p>
          </div>
        </div>
        <Dropdown
          trigger={
            <button
              title="options"
              className="text-gray-500 p-1 rounded-full hover:text-indigo-500 flex justify-center items-center transition-colors"
            >
              <DotsVerticalIcon className="h-5" />
            </button>
          }
          options={options}
        />
      </div>
      <div className="w-11/12 mx-auto mr-2 mt-1">
        {isEdit ? (
          <EditReply
            postId={postId}
            reply={reply}
            token={token}
            closeEdit={setIsEdit}
          />
        ) : (
          <p
            className="text-gray-600 dark:text-gray-300 text-sm line-clamp-3 hover:line-clamp-none"
            dangerouslySetInnerHTML={{ __html: reply.content }}
          />
        )}
      </div>
      <div className="flex w-full h-4 mr-2 mt-2 justify-between items-center px-4">
        <Tippy
          content={`${formatNumber(reply?.replies?.length || 0)} reactions`}
        >
          <div>
            <Rating
              count={5}
              value={rating}
              emptyIcon={<StarIcon className="h-1 w-1" />}
              halfIcon={<StarIcon className="h-1 w-1" />}
              onChange={submitRate}
              activeColor={COLORS_RATING[COLORS_RATING.length - 1]}
            />
          </div>
        </Tippy>
        <div className="flex justify-end items-center space-x-2 font-semibold text-xs text-indigo-500">
          <InteractionUsers user={user.id} interators={reply?.replies} />
        </div>
      </div>
    </div>
  )
}

export default memo(Reply)
