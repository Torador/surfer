import FormSignIn from '../components/FormSignIn'
import { useCallback, useState } from 'react'
import { toast } from 'react-toastify'
import Toast from '../components/Toast'
import { SYS_AUTH, LOGIN_URL } from '../utils/const'
import router from 'next/router'
import { jsonFetch } from '../utils'
//@ts-check

function Auth() {
  const [isLoading, setIsLoading] = useState(false)

  const handleSubmit = useCallback(
    /**
     * @param {import('react').FormEvent} e
     */
    async (e) => {
      e.stopPropagation()
      e.preventDefault()

      /**
       * @type {{username: string, password: string}}
       */
      const dataUser = {
        email: e.target[0].value,
        username: 'surferCode',
        password: e.target[1].value,
      }

      if (dataUser.email.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_AUTH}
            message="Veuillez entrer votre nom d'utilisateur SVP."
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: 5000,
          }
        )

      if (dataUser.password.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_AUTH}
            message="Veuillez entrer votre mot de passe SVP."
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: 5000,
          }
        )

      if (dataUser.password.toString().trim().length <= 8)
        return toast(
          <Toast
            title={SYS_AUTH}
            message="Votre mot de passe doit être de 08 caractères minimum."
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: 5000,
          }
        )

      /**
       * @type {{type: 'bearer' ,token: string, expires_at: Date | string}}
       */
      const token = await jsonFetch(
        LOGIN_URL,
        {
          method: 'POST',
          body: dataUser,
        },
        false,
        SYS_AUTH,
        setIsLoading
      )

      if (!token) return

      await fetch('/api/auth', {
        method: 'POST',
        body: JSON.stringify({ token: token.token }),
      }).then((_) => {
        toast(
          <Toast
            title={SYS_AUTH}
            message={`Welcome ${dataUser.email} !`}
            type="success"
          />,
          {
            closeButton: false,
            autoClose: 5000,
          }
        )

        router.push('/home')
      })
    },
    []
  )

  return (
    <div className="min-h-screen flex items-center justify-center xl:justify-end">
      <FormSignIn isLoading={isLoading} handleSubmit={handleSubmit} />
    </div>
  )
}

export default Auth
