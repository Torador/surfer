import { HomeIcon } from '@heroicons/react/solid'
import {
  LogoutIcon,
  SearchIcon,
  UserGroupIcon,
  ChatAlt2Icon,
  BellIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  ChatIcon,
  BookmarkIcon,
  ChartBarIcon,
} from '@heroicons/react/outline'
import Tippy from '@tippyjs/react'
import router from 'next/router'
import Image from 'next/image'
import Link from 'next/link'
import { jsonFetch } from '../utils'
import { DEFAULT_AVATAR, LINKS_FOOTER, LOGOUT_URL } from '../utils/const'
import { Fragment, memo, useCallback, useEffect, useState } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import io from 'socket.io-client'
import { currentUserLogin, isOpenDialogPostState } from '../atoms'
import HeaderIcon from './HeaderIcon'
import Popup from './Popup'
import ButtonItem from './Buttons/ButtonItem'
import ButtonSmallItem from './Buttons/ButtonSmallItem'
import Badge from './Badge'
import Modal from './Modal'
import ThemeMode from './ThemeMode'
import useTheme from '../utils/hooks/useTheme'
import { toast } from 'react-toastify'
import Toast from './Toast'

/**
 *  NAVBAR COMPONENT
 * @param {{active?: 'home' | 'contacts' | 'chat' | 'notifications' | '#', token: string}} props
 * @returns {JSX.Element}
 */
function NavBar({ token, active = 'home' }) {
  const me = useRecoilValue(currentUserLogin)
  const [show, setShow] = useState(false)
  const [theme, setTheme] = useTheme()
  const setOpenModal = useSetRecoilState(isOpenDialogPostState)
  const logout = async () => {
    await jsonFetch(LOGOUT_URL, { method: 'GET' }, token)
    await fetch('/api/logout', {
      method: 'POST',
      body: JSON.stringify({}),
    }).then((_) => router.push('/'))
  }

  useEffect(() => {
    // connect to socket server
    const socket = io(process.env.NEXT_PUBLIC_HOST_SERVER)

    socket.on('load:user', (data) => {
      console.log(socket.id)
      console.log(data)
      toast(<Toast title="System" message={data.msg} type="info" />, {
        autoClose: 5000,
        closeButton: false,
      })
    })

    // socket disconnet onUnmount if exists
    return () => socket.disconnect()
  }, [])

  const openModalPost = useCallback(() => setOpenModal(true), [setOpenModal])

  const toggleModalTheme = useCallback(() => setShow((s) => !s), [setShow])

  const goToHomeOrRefresh = useCallback(() => {
    console.log(router.pathname)
    if (router.pathname == '/home') router.reload()
    else router.push('/home')
  }, [])

  return (
    <>
      <nav className="bg-white dark:bg-gray-900 z-40 fixed md:sticky top-0 right-0 left-0 flex items-center pl-2 py-2 lg:px-5 shadow dark:border dark:border-gray-800 dark:shadow-xl">
        <Modal
          isOpen={show}
          title="Theme setting"
          colorTitle="gray"
          className="w-full md:w-[700px]"
          onClose={toggleModalTheme}
        >
          <div className="flex flex-col space-y-5">
            <h2 className="text-sm text-gray-500">
              Adjust how you&apos;d like Surfer to appear on this browser.
            </h2>
            <div className="flex flex-wrap items-center md:justify-center space-x-2">
              <ThemeMode
                label="Light"
                urlImg="/Mode/Light Mode.png"
                isChecked={theme === 'light'}
                setTheme={setTheme}
              />
              <ThemeMode
                label="Dark"
                urlImg="/Mode/Dark Mode.png"
                isChecked={theme === 'dark'}
                setTheme={setTheme}
              />
              <ThemeMode
                label="Auto"
                urlImg="/Mode/Auto Mode.png"
                isChecked={theme === 'auto'}
                setTheme={setTheme}
              />
            </div>
            <div className="flex items-center justify-end">
              <button
                onClick={toggleModalTheme}
                className="px-3 py-2 font-semibold text-white rounded-md shadow-sm bg-indigo-500 hover:bg-indigo-600 transition-colors"
              >
                Done
              </button>
            </div>
          </div>
        </Modal>
        {/* LEFT */}
        <div className="grid grid-cols-2 sm:grid-cols-3 w-full">
          <div className="flex items-center">
            <button
              onClick={goToHomeOrRefresh}
              className="text-2xl font-extrabold italic text-indigo-500 tracking-wide"
            >
              Surfer
            </button>
            <div className="flex ml-2 items-center justify-center rounded-full bg-gray-100 dark:bg-gray-800 dark:text-white focus:shadow-sm transition-shadow p-1 lg:px-1">
              <SearchIcon className="h-5 text-gray-500 ml-1" />
              <input
                type="text"
                className="hidden xl:inline-flex bg-transparent outline-none placeholder-gray-500 flex-grow border-none focus:ring-0 -ml-1.5"
                placeholder="Search Surfer"
              />
            </div>
          </div>

          {/* CENTER */}
          <div className="sm:flex justify-center flex-grow z-10 hidden">
            <div className="flex space-x-12 md:space-x-2">
              <Tippy content="Home">
                <div className="relative">
                  <button type="button" onClick={goToHomeOrRefresh}>
                    <HeaderIcon active={active === 'home'} Icon={HomeIcon} />
                  </button>

                  {active === 'home' && (
                    <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                  )}
                </div>
              </Tippy>

              <Tippy content="Contacts">
                <div className="relative">
                  <Link href="#">
                    <a>
                      <HeaderIcon
                        active={active === 'contacts'}
                        Icon={UserGroupIcon}
                      />
                    </a>
                  </Link>
                  {active === 'contacts' && (
                    <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                  )}
                </div>
              </Tippy>

              <Tippy content="Chat">
                <div className="relative">
                  <Link href="/chat">
                    <a>
                      <HeaderIcon
                        active={active === 'chat'}
                        Icon={ChatAlt2Icon}
                      />
                    </a>
                  </Link>
                  {active === 'chat' && (
                    <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                  )}
                </div>
              </Tippy>

              <Tippy content="Notifications">
                <button className="relative">
                  <HeaderIcon
                    active={active === 'notifications'}
                    Icon={BellIcon}
                  />
                  {active === 'notifications' && (
                    <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                  )}
                </button>
              </Tippy>

              <Tippy content="Logout">
                <button onClick={logout}>
                  <HeaderIcon Icon={LogoutIcon} />
                </button>
              </Tippy>
            </div>
          </div>

          {/* RIGHT */}
          <div className="flex items-center justify-end">
            <div className="flex items-center space-x-1">
              <button
                onClick={openModalPost}
                className="s_btn_primary hidden lg:inline"
              >
                Share your passion
              </button>
              <div className="flex items-center cursor-pointer space-x-3">
                <Popup
                  classSize="max-w-[260px]"
                  trigger={
                    <div className="group flex items-center px-2 py-1 hover:bg-gray-100 dark:hover:bg-gray-800 text-gray-500 dark:text-gray-500 hover:text-indigo-500 dark:hover:text-indigo-500 rounded-full transition duration-300">
                      <Tippy
                        content={
                          me?.accounts[0]?.shortName
                            ? me.username
                            : me?.accounts[0].shortName
                        }
                      >
                        <div className="relative w-8 h-8 rounded-full border-none border-gray-100 dark:border-gray-800 dark:group-hover:border-indigo-500 transition-colors">
                          <Image
                            layout="fill"
                            objectFit="cover"
                            className="rounded-full"
                            src={me?.accounts[0]?.photo || DEFAULT_AVATAR}
                            alt="photo"
                          />
                        </div>
                      </Tippy>
                      <ChevronDownIcon className="h-5 transform group-hover:translate-y-0.5 transition-transform" />
                    </div>
                  }
                >
                  <div className="flex flex-col bg-white shadow-lg rounded-sm dark:bg-gray-900">
                    <Link href={`/profil/${me?.accounts[0]?.slug}`}>
                      <a
                        className="group relative border-b border-gray-200 dark:border-gray-800"
                        title="Go to your profile"
                      >
                        <div className="absolute inset-0 rounded-t-sm group-hover:bg-white dark:group-hover:bg-gray-800 group-hover:bg-opacity-40 dark:group-hover:bg-opacity-40 z-10 transition duration-300" />
                        <div className="flex flex-col space-y-2 py-4 w-[230px] mx-auto">
                          <div className="flex items-center">
                            <Image
                              width="50"
                              height="50"
                              objectFit="cover"
                              className="rounded-full"
                              src={me?.accounts[0]?.photo || DEFAULT_AVATAR}
                              alt="photo"
                            />
                          </div>
                          <div className="flex flex-col -space-y-0.5 px-2 text-gray-700 dark:text-gray-100">
                            <div className="flex items-center justify-between ">
                              <h1 className="font-bold text-xl">
                                {!me?.accounts[0]?.shortName
                                  ? me?.username
                                  : me?.accounts[0].shortName}
                              </h1>
                              <ChevronRightIcon className="h-5" />
                            </div>
                            <p className="text-sm">{me?.accounts[0]?.email}</p>
                          </div>
                        </div>
                      </a>
                    </Link>
                    <div className="flex flex-col py-1 border-b border-gray-200 dark:border-gray-800">
                      <ButtonItem url="/chat" title="Messages">
                        <ChatIcon className="h-5" />
                        <span>Messages</span>
                      </ButtonItem>
                      <ButtonItem url="/chat" title="Your content & stat">
                        <ChartBarIcon className="h-5" />
                        <span>Your content & stat</span>
                      </ButtonItem>
                      <ButtonItem url="#" title="Bookmarks">
                        <BookmarkIcon className="h-5" />
                        <span>Bookmarks</span>
                      </ButtonItem>
                    </div>
                    <div className="flex flex-col py-1 border-b border-gray-200 dark:border-gray-800">
                      <ButtonSmallItem
                        title="Dark Mode"
                        onClick={toggleModalTheme}
                      >
                        <span>Dark Mode</span>
                        <Badge shape label={theme === 'dark' ? 'ON' : 'OFF'} />
                      </ButtonSmallItem>
                      <ButtonSmallItem title="Settings">
                        <span>Settings</span>
                      </ButtonSmallItem>
                      <ButtonSmallItem title="Languages">
                        <span>Languages</span>
                      </ButtonSmallItem>
                      <ButtonSmallItem title="Help">
                        <span>Help</span>
                      </ButtonSmallItem>
                      <ButtonSmallItem title="Logout" onClick={logout}>
                        <span>Logout</span>
                      </ButtonSmallItem>
                    </div>
                    <div className="bg-gray-100 dark:bg-gray-800 text-gray-500 text-[12px]">
                      <div className="flex items-center justify-center space-x-1 flex-wrap p-2">
                        {LINKS_FOOTER.map((link, i) => (
                          <Fragment key={i}>
                            <Link href={link.url}>
                              <a className="hover:underline">{link.label}</a>
                            </Link>
                            {LINKS_FOOTER.length - 1 !== i && (
                              <span>&middot;</span>
                            )}
                          </Fragment>
                        ))}
                      </div>
                    </div>
                  </div>
                </Popup>
              </div>
            </div>
          </div>
        </div>
      </nav>

      <nav
        id="mobile nav"
        className="bg-white dark:bg-gray-900 z-40 fixed md:sticky bottom-0 right-0 left-0 flex items-center py-2 lg:px-5 shadow border-t border-gray-100 dark:border dark:border-gray-800 dark:shadow-xl sm:hidden"
      >
        {/* CENTER */}
        <div className="flex justify-center flex-grow z-10">
          <div className="flex space-x-16 md:space-x-2">
            <Tippy content="Home">
              <div className="relative">
                <button type="button" onClick={goToHomeOrRefresh}>
                  <HeaderIcon active={active === 'home'} Icon={HomeIcon} />
                </button>

                {active === 'home' && (
                  <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                )}
              </div>
            </Tippy>

            <Tippy content="Contacts">
              <div className="relative">
                <Link href="#">
                  <a>
                    <HeaderIcon
                      active={active === 'contacts'}
                      Icon={UserGroupIcon}
                    />
                  </a>
                </Link>
                {active === 'contacts' && (
                  <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                )}
              </div>
            </Tippy>

            <Tippy content="Chat">
              <div className="relative">
                <Link href="/chat">
                  <a>
                    <HeaderIcon
                      active={active === 'chat'}
                      Icon={ChatAlt2Icon}
                    />
                  </a>
                </Link>
                {active === 'chat' && (
                  <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                )}
              </div>
            </Tippy>

            <Tippy content="Notifications">
              <button className="relative">
                <HeaderIcon
                  active={active === 'notifications'}
                  Icon={BellIcon}
                />
                {active === 'notifications' && (
                  <div className="absolute -bottom-2 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-14" />
                )}
              </button>
            </Tippy>
          </div>
        </div>
      </nav>
    </>
  )
}

export default memo(NavBar)
