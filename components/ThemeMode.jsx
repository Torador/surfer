import Image from 'next/image'
import { useCallback } from 'react'

/**
 * THEME CHECKBOX MODE COMPONENT
 * @param {{label: string, urlImg: string, isChecked: boolean, setTheme: (v: string) => void }} props
 * @returns {JSX.Element}
 */
function ThemeMode({ label, urlImg, id, isChecked, setTheme }) {
  const handleCheck = useCallback(
    () => setTheme(label.toLowerCase()),
    [label, setTheme]
  )
  return (
    <label
      htmlFor={id}
      onClick={handleCheck}
      className={`flex flex-col p-2 rounded-md hover:bg-gray-50 dark:hover:bg-gray-800 transition-colors cursor-pointer ${
        isChecked ? 'bg-indigo-50 dark:bg-indigo-700 dark:bg-opacity-10' : ''
      }`}
    >
      <div className="flex items-center space-x-1">
        <input
          id={id}
          type="checkbox"
          checked={isChecked}
          className="border-indigo-100 checked:bg-indigo-500 dark:border-indigo-900 group-hover:border-indigo-200 dark:group-hover:border-indigo-800 focus:border-surfer bg-indigo-50 focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 rounded-md"
        />
        <span
          className={`text-gray-800 dark:text-gray-100 font-bold ${
            isChecked ? 'text-indigo-600 dark:text-indigo-400' : ''
          }`}
        >
          {label}
        </span>
      </div>
      <div className="relative w-48 h-32 border border-gray-100 dark:border-gray-800 rounded-md shadow-sm">
        <Image layout="fill" src={urlImg} alt={label + 'Mode'} />
      </div>
    </label>
  )
}

export default ThemeMode
