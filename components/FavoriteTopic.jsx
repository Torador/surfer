import Image from 'next/image'
import { memo, useCallback } from 'react'
import Tippy from '@tippyjs/react'
import { selectedTopic } from '../atoms'
import { useRecoilState } from 'recoil'

/**
 * FAVORITE TOPICS COMPONENT
 * @param {{ thumb: string, topic: Topic}} props
 * @returns {JSX.Element}
 */
function FavoriteTopic({ thumb, topic }) {
  const [topicSelected, setSelectedTopic] = useRecoilState(selectedTopic)

  const _toggleSelected = useCallback(() => {
    setSelectedTopic((s) => (!s ? topic : s.id === topic.id ? null : topic))
  }, [setSelectedTopic, topic])
  return (
    <Tippy content={topic.description} placement="right">
      <button
        onClick={_toggleSelected}
        type="button"
        className="group flex items-center cursor-pointer px-3 py-2 w-11/12 mx-auto hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:bg-opacity-20 hover:shadow-sm rounded-full dark:hover:shadow transition duration-300"
      >
        <div className="relative w-6 h-6 bg-white rounded-md shadow mr-2">
          <Image
            layout="fill"
            src={thumb}
            className="h-6 w-6 flex-shrink"
            alt={topic.label}
          />
          <span className="absolute top-0 right-0 rounded-full bg-gradient-to-tr from-red-400 to-pink-500 w-1 h-1" />
        </div>
        <h5
          className={`${
            topicSelected && topic.id === topicSelected.id
              ? 'text-indigo-500 dark:text-indigo-500'
              : 'text-gray-600'
          } dark:text-gray-500 text-sm group-hover:text-indigo-500`}
        >
          {topic.label}
        </h5>
      </button>
    </Tippy>
  )
}

export default memo(FavoriteTopic)
