import Image from 'next/image'
import router from 'next/router'
import { useCallback, useState } from 'react'
import { toast } from 'react-toastify'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import {
  accountListState,
  currentUserLogin,
  suggestionListState,
} from '../atoms'
import { myDbQuery } from '../utils'
import { endPointCustom } from '../utils/api.config'
import { DEFAULT_AVATAR } from '../utils/const'
import Spinner from './Spinner'
import Toast from './Toast'

function Suggestion({ suggestion, token }) {
  const [isLoading, setIsLoading] = useState(false)
  const setAccounts = useSetRecoilState(accountListState)
  const user = useRecoilValue(currentUserLogin)
  const setSuggestion = useSetRecoilState(suggestionListState)

  const follow = useCallback(
    async (e) => {
      e.stopPropagation()
      try {
        setIsLoading(true)
        const ctc = await myDbQuery(
          endPointCustom('follow', `create/${suggestion.id}`),
          token
        )
        setIsLoading(false)

        if (ctc.errors || ctc.error)
          throw new Error(ctc.error || JSON.stringify(ctc.errors))

        const [newFollower] = ctc.followers.filter(
          (item) => item.account_id === user.accounts[0].id
        )

        setAccounts((accs) => {
          return accs.map((acc) => {
            if (acc.id !== suggestion.id) return acc

            return {
              ...acc,
              contacts: [
                {
                  ...acc.contacts[0],
                  followers: [
                    ...acc.contacts[0].followers,
                    newFollower || user.accounts[0].followers[0],
                  ],
                },
              ],
            }
          })
        })

        setSuggestion((accs) => {
          return accs.map((acc) => {
            if (acc.id !== suggestion.id) return acc

            return {
              ...acc,
              contacts: [
                {
                  ...acc.contacts[0],
                  followers: [
                    ...acc.contacts[0].followers,
                    newFollower || user.accounts[0].followers[0],
                  ],
                },
              ],
            }
          })
        })
      } catch (error) {
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [setAccounts, setSuggestion, suggestion.id, token, user.accounts]
  )
  return (
    <button
      type="button"
      onClick={() => router.push(`/profil/${suggestion?.slug}`)}
      className="flex justify-between w-full items-center rounded-md shadow-sm dark:shadow-none dark:bg-transparent border-b dark:border-gray-800 p-2 hover:bg-gray-100 dark:hover:bg-gray-800 dark:bg-opacity-60 transition-colors bg-white"
      key={suggestion.id}
    >
      <div className="flex items-center space-x-1">
        <div className="relative h-14 w-14 rounded-full">
          <Image
            src={suggestion?.photo || DEFAULT_AVATAR}
            layout="fill"
            alt={suggestion.email}
            className="object-cover rounded-full"
          />
        </div>
        <div className="flex flex-col items-start">
          <h1 className="text-sm font-bold text-gray-800 dark:text-gray-50 max-w-[150px] truncate">
            {suggestion?.lastname} {suggestion?.firstname}
          </h1>
          {suggestion.isFollowBackShow && (
            <p className="text-gray-500 text-xs max-w-[190px] truncate">
              Follows you
            </p>
          )}
        </div>
      </div>
      <button
        type="button"
        onClick={follow}
        className="px-2 py-1 rounded-full text-white font-semibold text-xs shadow bg-gradient-to-tr from-indigo-500 to-blue-400 dark:to-blue-600 hover:shadow-lg transform active:scale-95 transition duration-300"
      >
        {isLoading && <Spinner />}
        {!isLoading && 'Follow'}
      </button>
    </button>
  )
}

export default Suggestion
