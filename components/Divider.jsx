/**
 * DIVIDER COMPONENT
 * @param {{text: string, className?: string, orientation: 'horizontal' | 'vertical', hasParent?: boolean}} props
 * @returns {JSX.Element}
 * @author Jordan Kagmeni (Torador)
 * @description If the Divider has parent element, ignore hasParent props. Otherwise, mark this prop to have it responsive on each breakpoint.
 */
function Divider({
  text = 'Ou',
  className,
  orientation = 'horizontal',
  hasParent = false,
}) {
  let responsiveW = []
  let responsiveH = []

  const w_h = orientation === 'horizontal' ? 'w' : 'h'
  const col = orientation === 'vertical' ? 'flex-col' : ''

  if (!className && orientation === 'horizontal') {
    className = hasParent
      ? 'bg-transparent text-gray-500'
      : 'bg-transparent text-gray-500 sm:w-1/2 md:w-96'
  }

  if (!className && orientation === 'vertical') {
    className = 'bg-transparent text-gray-500'
  }

  if (!className.includes('px') || !className.includes('py')) {
    className += orientation === 'horizontal' ? ' px-2' : ' py-2'
  }

  if (className.includes('sm:w') || className.includes('md:w')) {
    const rw = className.split(' ')
    rw.filter((item) => {
      if (
        item.startsWith('sm:w') ||
        item.startsWith('md:w') ||
        item.startsWith('lg:w')
      ) {
        responsiveW.push(item)
        className = className.replace(item, '')
      }
      if (
        item.startsWith('sm:h') ||
        item.startsWith('md:h') ||
        item.startsWith('lg:h')
      ) {
        responsiveH.push(item)
        className = className.replace(item, '')
      }
    })
  }

  return (
    <div
      className={`${w_h}-full ${
        w_h === 'h' ? 'my-auto' : 'mx-auto'
      } rounded-full relative flex ${col} ${responsiveW.join(
        ' '
      )} ${responsiveH.join(' ')} items-center ${
        w_h === 'h' ? 'mx-2' : 'my-2'
      } ${w_h == 'h' ? 'w-[1px]' : 'h-[1px]'}`}
    >
      <div
        className={`bg-gray-300 dark:bg-gray-800 ${
          w_h === 'w' ? 'h' : 'w'
        }-[1px] flex-grow`}
      />
      <div className={`flex-shrink ${className} text-sm`}>{text}</div>
      <div
        className={`flex-grow bg-gray-300 dark:bg-gray-800 ${
          w_h === 'w' ? 'h' : 'w'
        }-[1px]`}
      />
    </div>
  )
}

export default Divider
