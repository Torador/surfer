import Image from 'next/image'
import { memo } from 'react'

/**
 * SMALL CONTACT COMPONENT
 * @param {{name: string, thumb: string, time: string, status?: boolean}} props
 * @returns {JSX.Element}
 */
function SmallContact({ thumb, name, status = false, time }) {
  return (
    <div className="group flex items-center justify-between cursor-pointer px-3 py-2 hover:bg-gray-100 rounded-md hover:shadow-sm transition duration-300">
      <div className="flex items-center">
        <div className="relative w-9 h-9 bg-white rounded-full shadow-sm group-hover:shadow-md mr-2">
          <Image layout="fill" src={thumb} className="flex-shrink" alt={name} />
        </div>
        <h5 className="text-gray-700 group-hover:text-indigo-500 text-xs font-semibold truncate max-w-[200px]">
          {name}
        </h5>
      </div>
      {status ? (
        <span className="h-2 w-2 rounded-full bg-indigo-500 animate-pulse" />
      ) : (
        <div className="text-gray-500 text-[10px]">{time}</div>
      )}
    </div>
  )
}

export default memo(SmallContact)
