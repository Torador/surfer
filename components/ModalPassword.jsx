import { useState, useCallback } from 'react'
import { toast } from 'react-toastify'
import { jsonFetch } from '../utils'
import { endPointPut } from '../utils/api.config'
import { SYS_PASSWORD } from '../utils/const'
import InputPassword from './InputPassword'
import Loader from './Loader'
import Modal from './Modal'
import Toast from './Toast'

/**
 *
 * @param {{className: string, title?: string, triggerButton: string | JSX.Element}} props
 * @returns {JSX.Element}
 */

function ModalPassword({
  me,
  token,
  className,
  title = 'Edit Password',
  triggerButton,
}) {
  const [isLoading, setIsLoading] = useState(false)
  const [isModal, setIsModal] = useState(false)

  const handlePassword = useCallback(
    /**
     * @param {import('react').FormEvent} e
     */
    async (e) => {
      e.stopPropagation()
      e.preventDefault()

      /**
       * @type {{oldPassword: string, newPassword: string}}
       */

      const dataPassword = {
        oldPassword: e.target[0].value,
        newPassword: e.target[1].value,
      }
      const confirmPassword = e.target[2].value

      if (dataPassword.oldPassword.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_PASSWORD}
            message="Please enter your old password"
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (dataPassword.oldPassword.toString() !== me.password.toString())
        return toast(
          <Toast
            title={SYS_PASSWORD}
            message="The password written doesn't match your current password"
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )
      if (dataPassword.newPassword.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_PASSWORD}
            message="Please enter your new password"
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (confirmPassword.toString().trim() === '')
        return toast(
          <Toast
            title="SYS_PASSWORD"
            message="Please confirm your new password"
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (
        confirmPassword.toString().trim() !==
        dataPassword.newPassword.toString().trim()
      )
        return toast(
          <Toast
            title={SYS_PASSWORD}
            message="your passwords aren't identical"
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      const response = await jsonFetch(
        endPointPut('accounts', 'me'),
        {
          method: 'PUT',
          body: JSON.stringify({ ...dataPassword }),
        },
        token,
        SYS_PASSWORD,
        setIsLoading
      )

      if (!response) {
        e.target[0].value = ''
        e.target[0].focus()
        return
      }

      toast(
        <Toast
          title="SYS_PASSWORD"
          message="Congrats, your have successfully changed your password"
          type="success"
        />,
        {
          closeButton: false,
          autoClose: true,
        }
      )
      setIsModal(false)
    },
    [me, token]
  )
  const _toggleModal = useCallback(() => {
    setIsModal((s) => !s)
  }, [])

  const t = (
    <>
      <button className={`sm:inline ${className}`} onClick={_toggleModal}>
        {triggerButton}
      </button>
    </>
  )

  return (
    <Modal trigger={t} title={title} isOpen={isModal} onClose={_toggleModal}>
      <div className="flex relative mx-2 items-center justify-center">
        <form
          onSubmit={handlePassword}
          className="flex flex-col space-y-6 w-full"
        >
          <div className="flex flex-col space-y-2 w-96 mx-auto">
            <label
              htmlFor="oldPassword"
              className="flex-auto justify-start text-left text-black"
            >
              Old password :
            </label>
            <InputPassword placeholder="Enter your old password" />
          </div>
          <div className="flex flex-col space-y-2 w-96 mx-auto">
            <label
              htmlFor="newPassword"
              className="flex-auto justify-start text-left text-black"
            >
              New password :
            </label>
            <InputPassword placeholder="Enter your new password" />
          </div>
          <div className="flex flex-col space-y-2 w-96 mx-auto">
            <label
              htmlFor="confirmPassword"
              className="flex-auto justify-start text-left text-black"
            >
              Confirm new password :
            </label>
            <InputPassword placeholder="confirm your new password" />
          </div>
          <div className="flex flex-col space-y-2 w-32 justify-center items-center mx-auto">
            <button
              type="submit"
              className="transform bg-indigo-500 hover:-translate-y-1 hover:shadow-md hover:bg-indigo-600 py-3 px-2 text-white rounded-md transition duration-300 flex justify-center font-semibold w-full shadow-2xl"
            >
              {isLoading ? <Loader /> : 'Edit password'}
            </button>
          </div>
        </form>
      </div>
    </Modal>
  )
}

export default ModalPassword
