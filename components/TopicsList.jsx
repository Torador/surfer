import { useState } from 'react'
import { useRecoilValue } from 'recoil'
import { topicListState } from '../atoms'
import Loader from './Loader'
import Topic from './Topic'

//@ts-check

/**
 * TOPICS LIST COMPONENT
 * @returns {JSX.Element}
 */
function TopicsList() {
  const topics = useRecoilValue(topicListState)

  if (!topics) return null

  return (
    <div className="flex space-x-3 overflow-scroll scrollbar-hide py-3 last:mr-0 max-w-[340px] mx-auto mt-20 md:hidden">
      {topics?.map((t) => (
        <Topic key={t.id} topic={t} />
      ))}
    </div>
  )
}

export default TopicsList
