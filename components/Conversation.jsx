import { PhoneIcon, VideoCameraIcon } from '@heroicons/react/outline'
import { memo, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
//import TextareaAutosize from 'react-textarea-autosize'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
  currentUserLogin,
  isOpenModalConversation,
  pagePaginate,
  showConversationState,
  userListState,
} from '../atoms'
import { myDbQuery } from '../utils'
import { endPointCustom } from '../utils/api.config'
import CreateMessage from './CreateMessage'
import EmptyConversation from './EmptyConversation'
import ListMessage from './ListMessage'
import Toast from './Toast'

function Conversation({ token }) {
  const setOpenConvModal = useSetRecoilState(isOpenModalConversation)
  const [loading, setLoading] = useState(false)
  const conversation = useRecoilValue(showConversationState)
  const currentUser = useRecoilValue(currentUserLogin)
  const users = useRecoilValue(userListState)
  const [{ messagesPage }, setPage] = useRecoilState(pagePaginate)
  const [messages, setMessages] = useState([])
  const [meta, setMeta] = useState({})

  useEffect(() => {
    if (!conversation) return
    setLoading(true)
    myDbQuery(endPointCustom('messages', conversation.id), token)
      .then((messages) => {
        setLoading(false)
        if (messages.errors || messages.error)
          throw new Error(messages?.error || 'Error Occured !')
        setMessages(messages)
      })
      .catch((err) => {
        setLoading(false)
        console.log(err)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      })
  }, [conversation, messagesPage, token])

  let interlocutorId = 0
  if (conversation) {
    interlocutorId =
      conversation?.account_inter === currentUser.accounts[0]?.id
        ? conversation.account_init
        : conversation.account_inter
  }

  const username = users.filter(
    (user) => user.accounts[0]?.id === interlocutorId
  )[0]?.username

  return (
    <main className="flex flex-col w-full">
      <div className="flex items-center px-4 pt-4 pb-1">
        <input
          placeholder="Search contacts..."
          className="p-3 rounded-full shadow dark:shadow-none text-sm focus:outline-none border dark:border-gray-700 focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 dark:bg-gray-800 text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 w-full"
        />
      </div>
      {!conversation ? (
        <div className="flex flex-col space-y-2 justify-center items-center min-h-[80vh]">
          <EmptyConversation />

          <p className="text-gray-500">
            Create or click on one conversation for chat...
          </p>
          <button
            onClick={() => setOpenConvModal((s) => !s)}
            type="button"
            className="s_btn_primary w-[230px] mx-auto"
          >
            New conversation
          </button>
        </div>
      ) : (
        <>
          <div className="flex items-center border-b border-gray-100 dark:border-gray-800 px-4 py-1 justify-between">
            <div className="flex flex-col mt-1 space-y-0.5">
              <h1 className="text-3xl font-bold text-gray-800 dark:text-gray-50">
                {username?.split(' ').length > 2
                  ? username?.split(' ')[0] + ' ' + username?.split(' ')[1]
                  : username}
              </h1>
              {/* <p className="text-gray-500 text-xs">Online</p> */}
            </div>
            <div className="flex items-center space-x-2">
              <button
                type="button"
                className="p-2 rounded-md hover:bg-gray-200 dark:hover:bg-gray-800 dark:hover:bg-opacity-40 transition-colors text-gray-700 dark:text-gray-200"
              >
                <VideoCameraIcon className="h-5" />
              </button>
              <button
                type="button"
                className="p-2 rounded-md hover:bg-gray-200 dark:hover:bg-gray-800 dark:hover:bg-opacity-40 transition-colors text-gray-700 dark:text-gray-200"
              >
                <PhoneIcon className="h-5" />
              </button>
            </div>
          </div>
          <div className="flex-1 flex-col px-6 space-y-2 overflow-y-auto max-h-[520px] py-3 bg-white dark:bg-gray-800 dark:bg-opacity-40">
            <ListMessage conversation={conversation} token={token} />
          </div>
          <CreateMessage token={token} conversation={conversation} />
        </>
      )}
    </main>
  )
}

export default memo(Conversation)
