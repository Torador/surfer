import { useRecoilState, useRecoilValue } from 'recoil'
import { currentUserLogin, messageListState } from '../atoms'
import { myDbQuery } from '../utils'
import MessageUser from './MessageUser'
import MessageRemoteUser from './MessageRemoteUser'
import { useEffect, useState } from 'react'
import { endPointCustom } from '../utils/api.config'
import Toast from './Toast'
import { toast } from 'react-toastify'
import Spinner from './Spinner'

function ListMessage({ conversation, token }) {
  const user = useRecoilValue(currentUserLogin)
  const [messages, setMessages] = useRecoilState(messageListState)
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    setLoading(true)
    myDbQuery(endPointCustom('messages', `${conversation.id}`), token)
      .then((messages) => {
        if (messages.errors || messages.error)
          throw new Error(messages?.error || 'Error Occured !')

        setMessages(messages)
      })
      .catch((err) => {
        console.log(err)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      })
      .finally(() => setLoading(false))
  }, [conversation.id, setMessages, token])

  if (loading) return <Spinner color="indigo" double />

  return (
    <>
      {messages.map((message) => {
        if (message.conversation_id !== conversation.id) return
        if (message.from_account_id === user.accounts[0].id) {
          return <MessageUser key={message.id} message={message} />
        }

        return <MessageRemoteUser key={message.id} message={message} />
      })}
    </>
  )
}

export default ListMessage
