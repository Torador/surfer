import { LightningBoltIcon } from '@heroicons/react/outline'
import { useState } from 'react'
import { useRecoilValue } from 'recoil'
import { topicListState } from '../atoms'
import CreateTopic from './CreateTopic'
import FavoriteTopic from './FavoriteTopic'

function Sidebar({ token }) {
  const topics = useRecoilValue(topicListState)
  const [showMore, setShowMore] = useState(false)

  return (
    <div className="fixed top-18 flex flex-col space-y-8 pt-7 pb-24 max-h-screen z-10 w-[300px] mx-auto overflow-y-auto">
      <div className="flex flex-col space-y-1">
        <CreateTopic token={token} />
        {topics.map((topic, i) => {
          if (showMore)
            return (
              <FavoriteTopic key={topic.id} thumb="/surfer.png" topic={topic} />
            )
          if (i < 12) {
            return (
              <FavoriteTopic key={topic.id} thumb="/surfer.png" topic={topic} />
            )
          }
        })}

        {showMore ||
          (topics?.length > 12 && (
            <button
              onClick={() => setShowMore(true)}
              className="group flex items-center w-full space-x-1  text-sm bg-gray-100 bg-opacity-50 dark:bg-gray-800 dark:hover:shadow-xl hover:bg-indigo-50 text-indigo-500 hover:shadow-sm px-3 py-2 rounded-full transition duration-300"
            >
              <span className="flex items-center justify-center p-1 rounded-full">
                <LightningBoltIcon className="h-2 group-hover:text-indigo-500" />
              </span>
              <span>Discover all topics !</span>
            </button>
          ))}
      </div>
    </div>
  )
}

export default Sidebar
