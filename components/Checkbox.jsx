import { Fragment } from 'react'

/**
 * CHECKBOX COMPONENT
 * @param {{label: string, name: string}} param0
 * @returns {JSX.Element}
 */
function Checkbox({ label, name }) {
  return (
    <Fragment>
      <input
        type="checkbox"
        title="Se souvemir de moi"
        name={name}
        id={name}
        className="text-surfer rounded-md cursor-pointer border-surfer sm:border-gray-200 dark:border-gray-800 focus-visible:ring-2 focus-visible:ring-surfer dark:focus-visible:ring-surfer focus-visible:ring-opacity-75"
      />{' '}
      <label
        htmlFor={name}
        className="text-xs md:text-sm cursor-pointer select-none"
      >
        {label}
      </label>
    </Fragment>
  )
}

export default Checkbox
