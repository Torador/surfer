//@ts-check
import Link from 'next/link'

/**
 * BUTTON ITEM SMALL COMPONENT
 * @param {{url?: string, children: string | JSX.Element, title?: string, onClick?: (e: any) => void}} props
 * @returns {JSX.Element}
 */
function ButtonSmallItem({ url, children, title = '', onClick = null }) {
  if (!url) {
    return (
      <button onClick={onClick} className="s_btn_item_small" title={title}>
        {children}
      </button>
    )
  }
  return (
    <Link href={url}>
      <a title={title} className="s_btn_item_small">
        {children}
      </a>
    </Link>
  )
}

export default ButtonSmallItem
