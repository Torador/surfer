import Link from 'next/link'

/**
 * BUTTON ITEM COMPONENT
 * @param {{url: string, children: string | JSX.Element, title?: string}} props
 * @returns {JSX.Element}
 */
function ButtonItem({ url, children, title = '' }) {
  return (
    <Link href={url}>
      <a
        title={title}
        className="flex items-center text-sm py-2 px-3 hover:bg-gray-100 text-gray-700 dark:hover:bg-gray-800 dark:text-gray-400 space-x-1 transition duration-300"
      >
        {children}
      </a>
    </Link>
  )
}

export default ButtonItem
