import { Tab } from '@headlessui/react'
import { Fragment, useState } from 'react'
import Post from './Post'
import Spinner from './Spinner'

/**
 * TABS COMPONENT
 * @param {{tabs: string[], panels: Array<Post>, token: string, user: User}} props
 * @returns {JSX.Element}
 */
function Tabs({ tabs = [], panels, token, user }) {
  const [loading, setLoading] = useState(true)
  return (
    <Tab.Group>
      <Tab.List className="flex items-center py-4 border-b border-gray-200 space-x-4">
        {tabs.map((tab, i) => (
          <Tab as={Fragment} key={i}>
            {({ selected }) => (
              <button
                type="button"
                className={`text-sm font-semibold relative ${
                  selected ? 'text-indigo-500' : 'text-gray-500'
                }`}
              >
                {tab}
                {selected && <BorderTabs />}
              </button>
            )}
          </Tab>
        ))}
      </Tab.List>
      <Tab.Panels className="mt-2">
        {loading ? (
          <Spinner />
        ) : (
          panels?.map((panel) => (
            <Post key={panel.id} post={panel} user={user} token={token} />
          ))
        )}
      </Tab.Panels>
    </Tab.Group>
  )
}

export function BorderTabs() {
  return (
    <div className="absolute -bottom-4 right-1/2 transform translate-x-1/2  rounded-tr-xl rounded-tl-xl bg-indigo-500 h-1 w-full" />
  )
}
export default Tabs
