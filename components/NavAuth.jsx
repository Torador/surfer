import {
  FingerPrintIcon,
  LoginIcon,
  MenuIcon,
  PhoneIcon,
  QuestionMarkCircleIcon,
  UserGroupIcon,
} from '@heroicons/react/outline'
import Link from 'next/link'
import useTheme from '../utils/hooks/useTheme'
import ModalSignUp from './ModalSignUp'

function NavAuth() {
  const [theme, setTheme] = useTheme()
  return (
    <header className="flex h-16 w-full justify-between items-center text-indigo-800 dark:text-gray-400 text-sm px-7 lg:px-0 font-medium">
      <button className="inline md:hidden p-2 rounded-lg hover:bg-indigo-50 transition-colors appearance-none">
        <MenuIcon className="h-5" />
      </button>
      <div className="flex space-x-1 md:space-x-10 flex-grow items-center animate-fade">
        <Link href="/">
          <a className="text-2xl font-extrabold italic text-indigo-500 tracking-wide flex space-x-2 items-center">
            <span>Surfer</span>
            <span className="text-xs font-semibold bg-indigo-100 dark:bg-indigo-900 dark:bg-opacity-40 px-2 py-1 rounded-full mt-1">
              beta
            </span>
          </a>
        </Link>
        <Link href="/topUp">
          <a className="hidden md:flex space-x-1 transform hover:scale-105 transition-transform">
            <UserGroupIcon className="h-5" />
            <span>Teams</span>
          </a>
        </Link>
        <Link href="/contact">
          <a className="hidden md:flex space-x-1 transform hover:scale-105 transition-transform">
            <PhoneIcon className="h-5" />
            <span>Contact Us</span>
          </a>
        </Link>
        <Link href="/help">
          <a className="hidden lg:flex space-x-1 transform hover:scale-105 transition-transform">
            <QuestionMarkCircleIcon className="h-5" />
            <span>Help</span>
          </a>
        </Link>
      </div>
      <div className="flex justify-end space-x-6 md:space-x-10 items-center flex-grow animate-fade">
        <Link href="/politique">
          <a className="hidden lg:flex space-x-1 transform hover:scale-105 transition-transform">
            <FingerPrintIcon className="h-5" />
            <span>Politique</span>
          </a>
        </Link>
        <div className="relative flex justify-center flex-col items-center cursor-pointer">
          <Link href="/">
            <a className="font-semibold text-blue-900 dark:text-indigo-500 transition-colors flex space-x-1">
              <LoginIcon className="h-5" />
              <span className="hidden sm:inline">Sign In</span>
            </a>
          </Link>
          <div className="absolute -bottom-2 h-1 w-4 rounded-full bg-indigo-500" />
        </div>

        <ModalSignUp triggerButton="Register" className="s_btn_register" />
      </div>
    </header>
  )
}

export default NavAuth
