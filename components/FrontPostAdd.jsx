//@ts-check
import Image from 'next/image'
import Tippy from '@tippyjs/react'
import { useCallback } from 'react'
import { useSetRecoilState } from 'recoil'
import { isOpenDialogPostState } from '../atoms'
import { DEFAULT_AVATAR } from '../utils/const'

function FrontPostAdd({ isCompleteFullName, me }) {
  const setOpenModal = useSetRecoilState(isOpenDialogPostState)
  const openModalPost = useCallback(
    () => setOpenModal((s) => !s),
    [setOpenModal]
  )

  return (
    <Tippy content="Click to add new post 🚀">
      <button
        onClick={openModalPost}
        title={
          !isCompleteFullName
            ? me.username +
              ', veuillez completer les informations de votre profil pour nous aider à améliorer votre expérience sur Surfer.'
            : null
        }
        disabled={!isCompleteFullName}
        className={`flex flex-col border border-gray-200 dark:border-gray-800 ${
          !isCompleteFullName
            ? 'cursor-not-allowed bg-gray-100 dark:bg-gray-900'
            : 'hover:border-gray-300 dark:hover:border-gray-700 bg-white dark:bg-gray-900'
        } rounded-md p-2 transition-colors min-w-[300px] w-full mx-auto md:mx-0`}
      >
        <div className="flex space-x-1 items-center text-xs font-semibold text-gray-500 dark:text-gray-400">
          <Image
            src={me.accounts[0].photo || DEFAULT_AVATAR}
            width="25"
            height="25"
            objectFit="cover"
            className="rounded-full shadow"
            alt={me.username}
          />
          <h1>
            {!isCompleteFullName ? me.username : me.accounts[0].shortName}
          </h1>
        </div>
        <p className="text-xl text-left font-bold text-gray-400 dark:text-gray-600 my-2">
          What would you like to share ?🚀
        </p>
      </button>
    </Tippy>
  )
}

export default FrontPostAdd
