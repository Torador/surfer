//@ts-check
import Link from 'next/link'

/**
 * LINK PREVIEW PROFIL
 * @param {{account: Account}} props
 * @returns {JSX.Element}
 */
function LinkPreviewProfil({ account }) {
  return (
    <Link href={`/profil/${account.slug}`}>
      <a className="truncate font-semibold hover:underline text-sm dark:text-gray-50">
        {!account?.shortName ? account.email : account?.shortName}
      </a>
    </Link>
  )
}

export default LinkPreviewProfil
