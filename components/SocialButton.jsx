/* eslint-disable @next/next/no-img-element */
import router from 'next/router'
import { useCallback, useEffect, useRef } from 'react'

/**
 * SOCIAL BUTTON COMPONENT
 * @param {{path: string, title: string, link: string}} props
 * @returns {JSX.Element}
 */
const SocialButton = ({ path, title, link = '' }) => {
  const externalWindow = useRef()
  const timeout = useRef()
  useEffect(() => {
    return () => {
      if (externalWindow.current && !externalWindow.current.closed)
        externalWindow.current.close()
      if (timeout.current) clearInterval(timeout.current)
    }
  }, [])
  const _authDelegate = useCallback(async () => {
    if (externalWindow.current && !externalWindow.current.closed) {
      externalWindow.current.focus()
      return
    }
    externalWindow.current = window.open(
      link,
      title,
      'menubar=no, scrollbars=no, top=100, left=100, width=900, height=500'
    )

    timeout.current = setInterval(() => {
      router.push('/home')
    }, 1000)
  }, [link, title])
  return (
    <button
      onClick={_authDelegate}
      title={title}
      className="px-4 py-2 border border-gray-200 dark:border-gray-800 hover:border-white hover:bg-white dark:hover:bg-gray-800 hover:shadow-2xl transition duration-300 rounded-lg"
    >
      <img src={path} className="h-10" alt={path} />
    </button>
  )
}
export default SocialButton
