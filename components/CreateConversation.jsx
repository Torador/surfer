import { useCallback, useState } from 'react'
import Image from 'next/image'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
  conversationListState,
  currentUserLogin,
  isOpenModalConversation,
  showConversationState,
  userListState,
} from '../atoms'
import Modal from './Modal'
import { DEFAULT_AVATAR } from '../utils/const'
import { ChatIcon } from '@heroicons/react/outline'
import { myDbQuery } from '../utils'
import { endPointPost } from '../utils/api.config'
import Spinner from './Spinner'

function CreateConversation({ token }) {
  const [loading, setLoading] = useState(false)
  const users = useRecoilValue(userListState)
  const user = useRecoilValue(currentUserLogin)
  const setConversations = useSetRecoilState(conversationListState)
  const showConversation = useSetRecoilState(showConversationState)
  const [isModalConvOpen, setIsOpenModalConversation] = useRecoilState(
    isOpenModalConversation
  )

  const openModalConversation = useCallback(
    () => setIsOpenModalConversation((s) => !s),
    [setIsOpenModalConversation]
  )

  const _saveConversation = useCallback(
    (accountInter) => {
      if (!accountInter) return

      setLoading(true)
      myDbQuery(endPointPost('conversations'), token, { accountInter }, 'POST')
        .then((newConversation) => {
          if (newConversation.errors || newConversation.error)
            throw new Error(newConversation.error || 'ERROR OCCURED !')

          setConversations((convs) => {
            if (convs.filter((conv) => conv.id === newConversation?.id)[0]) {
              return convs.map(c => {
                if(c.id === newConversation?.id) return newConversation
                return c
              })
            }
            return [newConversation, ...convs]
          })
          showConversation(newConversation)
          setIsOpenModalConversation(false)
        })
        .catch((err) => console.log(err))
        .finally(() => {
          setLoading(false)
        })
    },
    [setConversations, setIsOpenModalConversation, showConversation, token]
  )
  return (
    <Modal
      title="Who do you want to chat ?"
      colorTitle="gray"
      className="w-full md:w-4/12"
      isOpen={isModalConvOpen}
      onClose={openModalConversation}
      opacity="10"
    >
      <div className="flex flex-col w-full space-y-2 dark:text-white">
        {/* Contact
        <input
          placeholder="Search contacts..."
          className="px-3 py-2 rounded-full shadow dark:shadow-none text-sm focus:outline-none border dark:border-gray-700 focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 dark:bg-gray-800 text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75"
        /> */}
        {users.map((usr) => {
          if (usr?.id !== user?.id)
            return (
              <div
                className="flex items-center justify-between p-2 rounded-md shadow hover:bg-gray-50 dark:shadow-sm dark:border dark:border-gray-800 dark:hover:bg-gray-800 dark:hover:bg-opacity-50 transition-colors"
                key={usr.id}
              >
                <div className="flex items-center">
                  <div className="relative w-10 h-10 rounded-full shadow mr-2">
                    <Image
                      src={usr?.accounts[0]?.photo || DEFAULT_AVATAR}
                      layout="fill"
                      className="rounded-full object-cover"
                      alt={usr?.accounts[0]?.email}
                    />
                  </div>
                  <h1 className="font-bold text-gray-800 dark:text-white ">
                    {!usr?.accounts[0]?.shortName
                      ? usr.username
                      : usr?.accounts[0]?.shortName}
                  </h1>
                </div>
                <button
                  onClick={() => _saveConversation(usr?.accounts[0]?.id)}
                  className="flex items-center space-x-1 s_btn_primary"
                >
                  {loading ? (
                    <Spinner />
                  ) : (
                    <>
                      <ChatIcon className="h-5" />
                      <span>Message</span>
                    </>
                  )}
                </button>
              </div>
            )
          return null
        })}
      </div>
    </Modal>
  )
}

export default CreateConversation
