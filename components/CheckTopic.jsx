import Tippy from '@tippyjs/react'
import { useCallback } from 'react'

/**
 * CHECKLABEK
 * @param {{topic: Topic, isChecked: boolean, onChange: () => void}} props
 * @returns {JSX.Element}
 */
function CheckTopic({ topic, isChecked, onChange }) {
  const handleChecked = useCallback(
    (e) => {
      onChange(e.target.checked, topic)
    },
    [onChange, topic]
  )

  const { id, label, description } = topic
  return (
    <Tippy content={description}>
      <label
        htmlFor={id + label}
        className="group flex items-center space-x-1 text-xs px-2 font-medium bg-indigo-500 dark:bg-indigo-900 bg-opacity-10 text-indigo-800 dark:text-indigo-300 rounded-md py-1 mr-2 mb-1 cursor-pointer"
      >
        <input
          id={id + label}
          onChange={handleChecked}
          checked={isChecked}
          type="checkbox"
          className="border-indigo-100 dark:border-indigo-900 group-hover:border-indigo-200 dark:group-hover:border-indigo-800 focus:border-surfer bg-indigo-50 dark:bg-gray-800 checked:bg-indigo-500 dark:checked:bg-indigo-800 focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 rounded-md cursor-pointer"
        />
        <span>{label}</span>
      </label>
    </Tippy>
  )
}

export default CheckTopic
