//@ts-check
import Image from 'next/image'
import { formatDistanceToNow } from 'date-fns'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import Rating from 'react-rating-stars-component'
import { accountListState, currentUserLogin, postListState } from '../atoms'
import { COLORS_RATING, DEFAULT_AVATAR, DELETE_ENTITY } from '../utils/const'
import Dropdown from './Dropdown'
import {
  DotsVerticalIcon,
  PencilIcon,
  StarIcon,
} from '@heroicons/react/outline'
import { ExclamationIcon, TrashIcon } from '@heroicons/react/solid'
import Toast from './Toast'
import { toast } from 'react-toastify'
import { formatNumber, myDbQuery } from '../utils'
import { endPointDelete, endPointRate } from '../utils/api.config'
import Tippy from '@tippyjs/react'
import { useCallback, useState } from 'react'
import EditComment from './EditComment'
import InteractionUsers from './InteractionUsers'
import useRateAverage from '../utils/hooks/useRateAverage'
import ListReplies from './ListReplies'
import LinkPreviewProfil from './LinkPreviewProfil'
import Modal from './Modal'

/**
 *  COMMENT COMPONENT
 * @param {{comment: Comments, token: string, postId: number, isCommentlock }} props
 * @returns {JSX.Element}
 */
function Comment({ comment, token, postId, isCommentlock }) {
  const user = useRecoilValue(currentUserLogin)
  const [isShow, setIsShow] = useState(false)
  const [rating, setRating] = useRateAverage(comment?.accounts)
  const accounts = useRecoilValue(accountListState)
  const [account] = accounts.filter((acc) => acc.id === comment.account_id)
  const setPostList = useSetRecoilState(postListState)
  const isEdited =
    comment.created_at.toString() == comment.updated_at.toString()

  const _toggleModal = useCallback(() => {
    setIsShow((s) => !s)
  }, [])

  const submitRate = useCallback(
    async (rate) => {
      setRating(rate)
      const data = {
        comment_id: comment.id,
        rating: rate,
      }
      try {
        /**
         * @type {Comments & ErrorFetch}
         */
        const c = await myDbQuery(endPointRate('comments'), token, data, 'POST')

        if (c.errors || c.error)
          throw new Error(c.error || JSON.stringify(c.errors))
        setPostList((posts) => {
          return posts.map((post) => {
            if (post.id !== comment.post_id) return post

            return {
              ...post,
              comments: post.comments.map((cmt) => {
                if (c.id !== cmt.id) return cmt

                return c
              }),
            }
          })
        })
      } catch (error) {
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [comment.id, comment.post_id, setPostList, setRating, token]
  )

  const opts = [
      [
        {
          icon: <ExclamationIcon className="w-5 h-5 mr-2" />,
          label: 'Report',
          action: () => {},
        },
      ],
    ],
    moreOptions = [
      {
        icon: <PencilIcon className="w-5 h-5 mr-2" />,
        label: 'Edit comment',
        action: async () => {
          isCommentlock ? alert('Comments are muted.') : _toggleModal()
        },
      },
      {
        icon: <TrashIcon className="w-5 h-5 mr-2" />,
        label: 'Delete comment',
        color: 'red',
        action: async () => {
          if (!confirm(DELETE_ENTITY('comment'))) return
          setPostList((p) =>
            p.map((p) => {
              if (p.id !== postId) return p
              return {
                ...p,
                comments: p.comments.filter((cmt) => cmt.id !== comment.id),
              }
            })
          )
          try {
            const response = await myDbQuery(
              endPointDelete('comments', comment.id),
              token,
              null,
              'DELETE'
            )
            if (response.errors || response.error)
              throw new Error(response.error || 'ERROR OCCURED !')
            toast(
              <Toast
                title="Système"
                message={`Le commentaire a bien été supprimé.`}
                type="success"
              />
            )
          } catch (error) {
            toast(
              <Toast title="Système" message={error.message} type="error" />
            )
          }
        },
      },
    ],
    options =
      user.accounts[0].id === comment.account_id ? [...opts, moreOptions] : opts

  return (
    <div className="flex flex-col">
      <div className="flex items-center justify-between">
        <div className="flex items-center">
          <div className="relative w-8 h-8 rounded-full shadow mr-2">
            <Image
              src={account?.photo || DEFAULT_AVATAR}
              layout="fill"
              className="rounded-full object-cover"
              alt={account.email}
            />
          </div>
          <div className="flex flex-col -space-y-0.5 max-w-[400px]">
            <div className="flex space-x-1 items-center">
              <LinkPreviewProfil account={account} />
              {isEdited ? null : (
                <>
                  <span className="text-gray-500">&middot;</span>
                  <p className="text-gray-500 text-xs flex items-center space-x-1">
                    Edited
                  </p>
                </>
              )}
            </div>
            <p className="text-gray-500 text-xs">
              {formatDistanceToNow(new Date(comment.created_at), {
                addSuffix: true,
              })}
            </p>
          </div>
        </div>
        <Dropdown
          trigger={
            <button
              title="options"
              className="text-gray-500 p-1 rounded-full hover:text-indigo-500 flex justify-center items-center transition-colors"
            >
              <DotsVerticalIcon className="h-5" />
            </button>
          }
          options={options}
        />
      </div>
      <div className="w-11/12 mx-auto mr-2 mt-1">
        {isShow ? (
          <EditComment
            comment={comment}
            token={token}
            closeOnDone={_toggleModal}
          />
        ) : (
          <p
            className="text-gray-600 dark:text-gray-300 text-sm line-clamp-3 hover:line-clamp-none"
            dangerouslySetInnerHTML={{ __html: comment.content }}
          />
        )}
      </div>
      <div className="flex w-full h-4 mr-2 mt-2 justify-between items-center px-4">
        <div className="flex items-center space-x-2">
          <Tippy
            content={`${formatNumber(comment?.accounts?.length)} reactions`}
          >
            <div>
              <Rating
                count={5}
                value={rating}
                emptyIcon={<StarIcon className="h-1 w-1" />}
                halfIcon={<StarIcon className="h-1 w-1" />}
                onChange={submitRate}
                activeColor={COLORS_RATING[COLORS_RATING.length - 1]}
              />
            </div>
          </Tippy>
          <ListReplies
            token={token}
            comment={comment}
            account={account}
            postId={postId}
          />
        </div>
        <div className="flex justify-end items-center space-x-2 font-semibold text-xs text-indigo-500">
          <InteractionUsers user={user.id} interators={comment?.accounts} />
        </div>
      </div>
    </div>
  )
}

export default Comment
