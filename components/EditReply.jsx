//@ts-check
import { CheckIcon, XIcon } from '@heroicons/react/outline'
import Image from 'next/image'
import { memo, useCallback, useState } from 'react'
import TextareaAutosize from 'react-textarea-autosize'
import { toast } from 'react-toastify'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { currentUserLogin, postListState } from '../atoms'
import { myDbQuery } from '../utils'
import { endPointPut } from '../utils/api.config'
import { DEFAULT_AVATAR } from '../utils/const'
import Spinner from './Spinner'
import Toast from './Toast'

/**
 * EDIT REPLY COMPONENT
 * @param {{reply: Reply, token: string, postId: number, closeEdit: (s: boolean) => void}} props
 * @returns {JSX.Element}
 */
function EditReply({ reply, closeEdit, token, postId }) {
  const user = useRecoilValue(currentUserLogin)
  const [loading, setLoading] = useState(false)
  const [content, setContent] = useState(reply.content || '')
  const setCommentListByPost = useSetRecoilState(postListState)
  const isChange = content.trim() !== reply.content

  const close = useCallback(() => closeEdit(false), [closeEdit])

  const submitReply = useCallback(
    async (e) => {
      if (
        (e.charCode !== 13 && e.code !== 'Enter' && e.type !== 'click') ||
        e.shiftKey
      )
        return
      if (!content.trim()) return
      if (!isChange) return close()
      const data = {
        commentId: reply.comment_id,
        content,
      }
      try {
        setLoading(true)
        const repl = await myDbQuery(
          endPointPut('reply', reply.id),
          token,
          data,
          'PUT'
        )
        setLoading(false)
        if (repl.errors || repl.error)
          throw new Error(repl.error || JSON.stringify(repl.errors))

        close()
        setCommentListByPost((posts) =>
          posts.map((post) => {
            if (post.id !== postId) return post
            return {
              ...post,
              comments: post.comments.map((cmt) => {
                if (cmt.id !== reply.comment_id) return cmt
                return {
                  ...cmt,
                  replies: cmt.replies.map((r) => {
                    if (r.id !== reply.id) return r

                    return repl
                  }),
                }
              }),
            }
          })
        )
        toast(
          <Toast
            title="System"
            message="La réponse a bien été mise à jour."
            type="success"
          />,
          {
            autoClose: 5000,
            closeButton: false,
          }
        )
      } catch (error) {
        setLoading(false)
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [
      close,
      content,
      isChange,
      postId,
      reply.comment_id,
      reply.id,
      setCommentListByPost,
      token,
    ]
  )

  const handleContent = useCallback((e) => setContent(e.target.value), [])

  return (
    <div className="group flex items-end space-x-1 m-2 sticky bottom-0">
      {loading ? (
        <Spinner />
      ) : (
        <Image
          src={user.accounts[0].photo || DEFAULT_AVATAR}
          width="35"
          height="35"
          className="rounded-full flex-shrink object-cover"
          alt={user.username}
        />
      )}
      <TextareaAutosize
        disabled={loading}
        autoFocus
        required
        rows={1}
        value={content}
        onChange={handleContent}
        onKeyPress={submitReply}
        placeholder="Reply here..."
        className={`border-none flex-grow w-full ${
          loading ? 'bg-gray-100 cursor-not-allowed' : 'bg-white'
        } shadow dark:bg-gray-800 px-3 py-1 text-sm focus:ring-0 focus:border-b-1 dark:text-white focus:border-indigo-500 rounded-md`}
      />
      <div className="flex items-center space-x-1">
        <button
          title="Update"
          type="button"
          onClick={submitReply}
          disabled={!isChange || content.trim().length <= 0}
          className={`text-xs whitespace-nowrap rounded-full p-1  ${
            content.trim().length <= 0 || !isChange
              ? 'bg-gray-300 dark:bg-gray-500 dark:text-gray-700 cursor-not-allowed'
              : 'bg-indigo-500 hover:bg-indigo-600'
          } text-white  transition-colors shadow-sm`}
        >
          <CheckIcon className="h-5" />
        </button>
        <button
          title="Cancel"
          type="button"
          className="text-xs whitespace-nowrap rounded-full p-1 bg-red-100 hover:bg-red-200 dark:bg-red-900 dark:hover:bg-red-800  text-red-500 transition-colors shadow-sm"
          onClick={close}
        >
          <XIcon className="h-5" />
        </button>
      </div>
    </div>
  )
}

export default memo(EditReply)
