import { useCallback } from 'react'
import { useRecoilState } from 'recoil'
import { selectedTopic } from '../atoms'

/**
 * TOPIC COMPONENT
 * @param {{topic: Topic, active?: boolean}} props
 * @returns {JSX.Element}
 */
function Topic({ topic }) {
  const [topicSelected, setSelectedTopic] = useRecoilState(selectedTopic)

  const _toggleSelected = useCallback(() => {
    setSelectedTopic((s) => (!s ? topic : s.id === topic.id ? null : topic))
  }, [setSelectedTopic, topic])
  return (
    <a
      onClick={_toggleSelected}
      title={topic?.description}
      className="flex flex-col justify-center items-center space-y-1 cursor-pointer hover:scale-105 transform transition duration-300 "
    >
      <p
        className={`text-sm ${
          topicSelected && topic.id === topicSelected.id
            ? 'text-white font-extrabold bg-indigo-500'
            : 'text-indigo-500 bg-indigo-100 dark:bg-indigo-900 dark:bg-opacity-40'
        } px-3 py-1 rounded-full`}
      >
        {topic.label}
      </p>
    </a>
  )
}

export default Topic
