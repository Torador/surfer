// @ts-check
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import Image from 'next/image'
import { Transition } from '@headlessui/react'
import {
  ArrowDownIcon,
  ArrowUpIcon,
  CameraIcon,
  PlusIcon,
  SearchIcon,
  TrashIcon,
} from '@heroicons/react/outline'
import { convertToRaw } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import { ACCEPT_FILES_POST, LIMIT_SIZE } from '../utils/const'
import Modal from './Modal'
import Tippy from '@tippyjs/react'
import CheckTopic from './CheckTopic'
import Input from './Input'
import TextareaAutosize from 'react-textarea-autosize'
import { isOpenDialogPostState, postListState, topicListState } from '../atoms'
import { useCallback, useRef, useState } from 'react'
import { toast } from 'react-toastify'
import Toast from './Toast'
import { useFetch } from '../utils/hooks/useFetch'
import { endPointPost } from '../utils/api.config'
import EditorHtml from './EditorHtml'
import Loader from './Loader'
import { ToradorFilterDom } from '../utils'
import { EditorState } from 'draft-js'
import Badge from './Badge'
import router from 'next/router'

/**
 * MODAL POST
 * @param {{titleModal?: string, token: string}} props
 * @returns {JSX.Element}
 */
function ModalPost({ titleModal = 'New post 🚀', token }) {
  const myDbQuery = useFetch()
  const filePickerRef = useRef(null)

  // @ts-ignore
  const tps = useRecoilValue(topicListState)
  const [isCommentlock, setIsCommentLock] = useState(false)
  const [activeEditor, setActiveEditor] = useState(false)
  const setPostList = useSetRecoilState(postListState)
  const [isModalTopic, setIsModalTopic] = useState(false)
  const [fileToUpload, setFileToUpload] = useState([])
  const [resultSearch, setResultSearch] = useState([])
  /**
   * @type {[Topic[], import('react').Dispatch<import('react').SetStateAction<Topic[]>>]}
   */
  const [topic, setTopic] = useState([])
  /**
   * @type {[any,  import('react').Dispatch<import('react').SetStateAction<any>>]}
   */
  const [content, setContent] = useState('')
  const [title, setTitle] = useState('')
  const [selectedFile, setSelectedFile] = useState([])
  const [loading, setLoading] = useState(false)
  const [isOpenModalPost, setOpenModal] = useRecoilState(isOpenDialogPostState)

  const _savePost = useCallback(
    async (e) => {
      e.preventDefault()
      e.stopPropagation()
      if (loading) return
      let sizeImages = 0
      fileToUpload.forEach((file) => {
        sizeImages += file.size
      })
      if (sizeImages > LIMIT_SIZE)
        return toast(
          <Toast
            title="Système"
            message={`La taille de vos fichiers ne doit pas dépasser 10Mo. Taille totale actuelle: ${(
              sizeImages / 1_000_000
            ).toFixed(2)} Mo`}
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: 5000,
          }
        )

      const data = {
        title,
        content:
          typeof content == 'object'
            ? draftToHtml(convertToRaw(content.getCurrentContent()))
            : content,
        isCommentlock: isCommentlock.toString(),
        files: fileToUpload,
        topics: topic.map((t) => Number(t.id)),
      }
      const check = []
      for (const p in data) {
        switch (typeof data[p]) {
          case 'string':
            if (data[p].trim() === '') check.push(true)
            break
          case 'number':
            break
          default:
            if (p === 'topics' && data[p].length <= 0) check.push(true)
            break
        }
      }

      if (check.includes(true))
        return toast(
          <Toast
            title="Système"
            message="Veuillez remplir les champs requis SVP !"
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: 5000,
          }
        )

      try {
        setLoading(true)
        const post = await myDbQuery(endPointPost('posts'), {
          token,
          body: data,
          method: 'POST',
          type: 'formdata',
        })
        setLoading(false)
        if (post.errors || post.error)
          throw new Error(post.error || 'ERROR OCCURED !')

        setPostList((p) => [
          {
            ...post,
            files: selectedFile.map((file, i) => ({
              id: i + 1,
              filename: fileToUpload[i].name,
              url_img: file,
            })),
          },
          ...p,
        ])
        setOpenModal(false)
        setTitle('')
        setContent(typeof content == 'object' ? EditorState.createEmpty() : '')
        setFileToUpload([])
        setSelectedFile([])
        setIsCommentLock(false)
        setTopic([])

        toast(
          <Toast
            title="Système"
            message="Your post has been successfully created."
            type="success"
          />,
          {
            closeButton: false,
            autoClose: 5000,
          }
        )
        if (router.pathname !== '/home') router.push('/home')
      } catch (error) {
        setLoading(false)
        toast(<Toast title="Système" message={error.message} type="error" />, {
          closeButton: false,
          autoClose: 5000,
        })
      }
    },
    [
      content,
      fileToUpload,
      isCommentlock,
      loading,
      myDbQuery,
      selectedFile,
      setOpenModal,
      setPostList,
      title,
      token,
      topic,
    ]
  )

  const onFileChange = useCallback((e) => {
    if (e.target.files.length <= 0) return

    setFileToUpload((file) => [...file, ...e.target.files])

    for (let i = 0; i < e.target.files.length; i++) {
      const reader = new FileReader()
      reader.readAsDataURL(e.target.files[i])
      reader.onload = (readerEvent) => {
        setSelectedFile((f) => [...f, readerEvent.target.result])
      }
    }
  }, [])

  const removeFile = useCallback((index) => {
    setSelectedFile((s) => s.filter((_, i) => i !== Number(index)))
    setFileToUpload((s) => s.filter((_, i) => i !== Number(index)))
  }, [])

  const handleTopicModal = (e) => {
    e.stopPropagation()
    e.preventDefault()
    setIsModalTopic((s) => !s)
  }

  const handleSelectTopic = (isChecked, item) => {
    if (isChecked) setTopic((t) => [...t, item])
    else setTopic((t) => t.filter((it) => it.id !== item.id))
  }

  const handleSearch = useCallback(
    (e) => {
      const { value } = e.target
      if (value.trim()) {
        /**
         * @type {Array<Topic>}
         */
        const resultSearch = ToradorFilterDom(value.trim(), tps)
        setResultSearch(resultSearch)
      } else {
        setResultSearch([])
      }
    },
    [setResultSearch, tps]
  )

  const openModalPost = useCallback(
    () => setOpenModal((s) => !s),
    [setOpenModal]
  )

  const toggleEditor = useCallback(async () => {
    if (typeof content === 'string')
      //await navigator?.clipboard?.writeText(content)

      activeEditor ? setContent(content) : setContent(EditorState.createEmpty())
    setActiveEditor((s) => !s)
  }, [activeEditor, content])
  return (
    <Modal
      title={titleModal}
      colorTitle="gray"
      className="w-full md:w-7/12"
      isOpen={isOpenModalPost}
      onClose={openModalPost}
      opacity="10"
    >
      <form className="flex flex-col p-2 space-y-2" onSubmit={_savePost}>
        <input
          type="file"
          multiple
          accept={ACCEPT_FILES_POST}
          name="files[]"
          ref={filePickerRef}
          onChange={onFileChange}
          className="hidden"
        />
        {selectedFile.length > 0 ? (
          <div className={`grid grid-cols-3 gap-2 rounded-md p-5`}>
            {selectedFile.map((file, i) => (
              <Transition
                as="div"
                show={!!selectedFile[i]}
                enter="ease-out duration-300 transform"
                enterFrom="opacity-0 translate-x-0"
                enterTo="opacity-100 translate-x-1"
                key={i}
                className="aspect-w-5 aspect-h-5 group transition-shadow hover:shadow-lg rounded-md"
              >
                <div className="absolute z-50 opacity-0 group-hover:opacity-100 transition duration-300">
                  <button
                    type="button"
                    onClick={(e) => {
                      e.stopPropagation()
                      e.preventDefault()
                      removeFile(i)
                    }}
                  >
                    <TrashIcon className="h-5 w-5 text-red-500 filter drop-shadow-md" />
                  </button>
                </div>
                <Image
                  layout="fill"
                  src={file}
                  className="rounded-md object-cover"
                  alt={i.toString()}
                />
              </Transition>
            ))}
            <button
              title="Add file(s)"
              type="button"
              className="border border-indigo-200 hover:border-indigo-300 dark:border-indigo-900 text-indigo-500 hover:text-indigo-600 border-dashed rounded-md w-full flex flex-col items-center justify-center space-y-2 transition-colors"
              onClick={() => filePickerRef.current?.click()}
            >
              <PlusIcon className="h-5" />
            </button>
          </div>
        ) : (
          <button
            title="Click to upload file(s)"
            type="button"
            className="border border-indigo-200 dark:border-indigo-900 border-dashed rounded-md py-12 flex flex-col items-center space-y-2"
            onKeyPress={(e) => {
              if (e.code === 'Enter') {
                filePickerRef.current?.click()
              }
            }}
            onClick={() => filePickerRef.current?.click()}
          >
            <div className="mx-auto w-12 h-12 flex items-center justify-center rounded-full bg-indigo-100 dark:bg-indigo-900 ">
              <CameraIcon
                className="h-5 w-5 text-indigo-500"
                aria-hidden="true"
              />
            </div>
            <p className="text-xl text-indigo-500 font-semibold">
              Upload a photo !
            </p>
          </button>
        )}
        <div className="flex flex-col space-y-2">
          <div className="flex justify-between items-center">
            <button
              type="button"
              onClick={handleTopicModal}
              className="text-gray-500 active:text-indigo-500 focus:text-indigo-500 hover:text-indigo-500 bg-gray-50 dark:bg-gray-800 shadow-sm hover:shadow transition duration-300 py-2 px-3 w-3/12 rounded-md flex items-center space-x-1 justify-center"
            >
              <span>
                {!isModalTopic
                  ? topic.length > 0
                    ? `${topic.length} ${
                        topic.length > 1 ? 'topics' : 'topic'
                      } selected`
                    : 'Select topics *'
                  : 'Close'}
              </span>
              {isModalTopic ? (
                <ArrowUpIcon className="h-4" />
              ) : (
                <ArrowDownIcon className="h-4" />
              )}
            </button>
            <div className="flex items-center space-x-1 group">
              <label
                htmlFor="isCommentLock"
                className="text-sm text-gray-800 dark:text-gray-500"
              >
                Lock comments ?
              </label>
              <input
                onChange={(e) => setIsCommentLock(e.target.checked)}
                type="checkbox"
                name="isCommentLock"
                id="isCommentLock"
                className="border-indigo-100 dark:border-indigo-900 group-hover:border-indigo-200 dark:group-hover:border-indigo-800 focus:border-surfer bg-indigo-50 dark:bg-gray-800 focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 rounded-md cursor-pointer"
              />
            </div>
          </div>

          {isModalTopic && (
            <div className="flex flex-col space-y-1">
              <div className="flex items-center justify-between space-x-2">
                <label
                  className="relative flex items-center transition duration-300 focus:outline-none px-2 py-3 border border-indigo-100 dark:border-gray-800 rounded-lg shadow-inner focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 bg-indigo-50 dark:bg-gray-800 text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 w-full"
                  id="search_topic_form"
                >
                  <SearchIcon className="h-5 absolute top-1/2 transform -translate-y-1/2 text-gray-500" />
                  <input
                    id="search_topic_form"
                    autoFocus
                    className="ml-6 bg-transparent border-none outline-none ring-0 w-10/12"
                    placeholder="Search Topic here..."
                    onChange={handleSearch}
                  />
                  {topic.length > 0 && (
                    <span className="absolute top-1/2 right-1 transform -translate-y-1/2">
                      <Badge
                        color="indigo"
                        label={topic.length.toString()}
                        description="Topic(s) Selected"
                        shape
                      />
                    </span>
                  )}
                </label>
                <Tippy content="Add Topic" placement="right">
                  <button
                    type="button"
                    className="hidden bg-gray-100 p-2 text-gray-500 hover:text-indigo-500 rounded-md shadow-sm hover:shadow transition duration-300"
                  >
                    <PlusIcon className="h-5" />
                  </button>
                </Tippy>
              </div>
              {resultSearch.length > 0 ? (
                <div className="flex flex-wrap items-center -mb-2">
                  {resultSearch?.map((tp) => {
                    return (
                      <CheckTopic
                        key={tp.id}
                        topic={tp}
                        isChecked={!!topic.filter((t) => t.id === tp.id)[0]}
                        onChange={handleSelectTopic}
                      />
                    )
                  })}
                </div>
              ) : (
                <div className="flex flex-wrap items-center -mb-2">
                  {tps?.map((tp, i) => {
                    if (i <= 11) {
                      return (
                        <CheckTopic
                          key={tp.id}
                          topic={tp}
                          isChecked={!!topic.filter((t) => t.id === tp.id)[0]}
                          onChange={handleSelectTopic}
                        />
                      )
                    }
                  })}
                  {tps.length > 11 && (
                    <Badge label={`+${tps.length - 11}`} color="indigo" shape />
                  )}
                </div>
              )}
            </div>
          )}
        </div>
        <div>
          <label
            htmlFor="titlePost"
            className="text-gray-800 dark:text-gray-500 text-sm"
          >
            Title *
          </label>
          <Input
            id="titlePost"
            required
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Enter title of post."
          />
        </div>
        <div>
          <div className="flex items-center justify-between">
            <label
              htmlFor="content"
              className="text-gray-800 dark:text-gray-500 text-sm"
            >
              Content *
            </label>
            <label
              htmlFor="typeType"
              className="text-gray-800 dark:text-gray-500 text-xs flex items-center space-x-1 cursor-pointer"
            >
              <span>Display Editor</span>
              <input
                id="typeType"
                type="checkbox"
                className="border-indigo-100 dark:border-indigo-900 group-hover:border-indigo-200 dark:group-hover:border-indigo-800 focus:border-surfer bg-indigo-50 dark:bg-gray-800 focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 rounded-md cursor-pointer"
                onChange={toggleEditor}
                checked={activeEditor}
              />
            </label>
          </div>
          {activeEditor ? (
            <EditorHtml content={content} setContent={setContent} />
          ) : (
            <TextareaAutosize
              required
              placeholder="Enter content"
              onChange={(e) => setContent(e.target.value)}
              value={
                typeof content === 'object'
                  ? draftToHtml(convertToRaw(content.getCurrentContent()))
                  : content
              }
              id="content"
              className="transition duration-300 focus:outline-none px-2 py-3 border border-indigo-100 dark:border-gray-800 rounded-lg shadow-inner focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 bg-indigo-50 dark:bg-gray-900 text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 w-full"
            />
          )}
        </div>

        <div className="flex justify-end items-center space-x-2">
          <button
            disabled={loading}
            type="submit"
            className={`${
              loading
                ? 'text-gray-500 bg-gray-200 dark:bg-gray-700 cursor-not-allowed'
                : 'text-white bg-indigo-500 dark:bg-indigo-800 hover:bg-indigo-600 dark:hover:bg-indigo-900 hover:shadow-xl'
            } shadow-sm transition duration-300 py-2 px-3 w-3/12 rounded-md flex items-center space-x-1 justify-center`}
          >
            {loading ? <Loader color="text-indigo-500 fill-current" /> : 'Post'}
          </button>
          <button
            disabled={loading}
            type="button"
            className="text-gray-500 active:text-indigo-500 focus:text-indigo-500 hover:text-indigo-500 bg-gray-50 dark:bg-gray-800 shadow-sm hover:shadow transition duration-300 py-2 px-3 rounded-md flex items-center space-x-1 justify-center"
            onClick={openModalPost}
          >
            Cancel
          </button>
        </div>
      </form>
    </Modal>
  )
}

export default ModalPost
