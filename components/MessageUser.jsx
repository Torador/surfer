import { format } from 'date-fns'
import { decipher } from '../utils'

/**
 * MESSAGE FOR USER REMOTE
 * @param {{message: Message}} props
 * @returns {JSX.Element}
 */
function MessageUser({ message }) {
  return (
    <div className="flex flex-col space-y-1 items-end">
      <h1 className="text-gray-500 font-bold text-xs">
        {format(new Date(message.created_at), 'HH:mm')}
      </h1>
      <div className="max-w-[400px] rounded-xl rounded-tr-none shadow text-sm bg-gradient-to-tr from-indigo-500 to-blue-400 dark:to-blue-600 text-white p-3 line-clamp-6 hover:line-clamp-none">
        {decipher()(message.content)}
      </div>
    </div>
  )
}

export default MessageUser
