//@ts-check
/**
 * INPUT COMPONENT
 * @param {{type?: 'text' | 'password' | 'number' | 'tel' |'email' |'file', name?: string, id?: string, placeholder?: string, pattern?: string, accept?: string, required?: boolean, value?: string, defaultValue?: string, onChange?: (e: import("react").ChangeEvent<HTMLInputElement>) => void}} props
 * @returns {JSX.Element}
 * @author Jordan Kagmeni (Torador)
 */
function Input({
  type = 'text',
  name = '',
  id = '',
  placeholder = '',
  pattern = '',
  accept = '',
  ...props
}) {
  return (
    <input
      type={type}
      name={name}
      id={id || name}
      className={`transition duration-300 focus:outline-none px-2 py-3 border border-indigo-100 dark:border-gray-800 rounded-lg shadow-inner focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 ${
        // @ts-ignore
        props.disabled
          ? 'bg-gray-400 dark:bg-gray-800 cursor-not-allowed'
          : 'bg-indigo-50 dark:bg-gray-900'
      } text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 w-full`}
      placeholder={placeholder}
      accept={accept}
      {...props}
    />
  )
}

export default Input
