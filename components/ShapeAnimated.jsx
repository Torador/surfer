/**
 *  SHAPE ANIMATED COMPONENT
 * @param {{ color: string, position: string}} props
 * @returns {JSX.Element}
 */
function ShapeAnimated({ color, position }) {
  return (
    <div
      className={`absolute ${position} w-72 h-72 bg-${color}-300 rounded-full mix-blend-multiply filter blur-xl opacity-30 animate-blob`}
    ></div>
  )
}

export default ShapeAnimated
