//@ts-check
import Image from 'next/image'
import { ReplyIcon } from '@heroicons/react/outline'
import Tippy from '@tippyjs/react'
import { memo, useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useSetRecoilState } from 'recoil'
import { postListState } from '../atoms'
import { formatNumber, myDbQuery } from '../utils'
import { endPointGetBySlug } from '../utils/api.config'
import { DEFAULT_AVATAR } from '../utils/const'
import CreateReply from './CreateReply'
import EmptyReply from './EmptyReply'
import Modal from './Modal'
import Reply from './Reply'
import Spinner from './Spinner'
import Toast from './Toast'

function ListReplies({ comment, token, postId, account }) {
  const { replies, id: ID } = comment
  const setCommentListByPost = useSetRecoilState(postListState)
  const [isShow, setIsShow] = useState(false)
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    if (!isShow) return
    setLoading(true)
    myDbQuery(endPointGetBySlug('reply', ID), token, null, 'GET', 'json')
      .then((rep) => {
        setLoading(false)
        if (rep.errors || rep.error)
          throw new Error(rep.error || JSON.stringify(rep.errors))

        setCommentListByPost((posts) =>
          posts.map((post) => {
            if (post.id !== postId) return post
            return {
              ...post,
              comments: post.comments.map((comment) => {
                if (comment.id !== ID) return comment

                return {
                  ...comment,
                  replies: rep,
                }
              }),
            }
          })
        )
      })
      .catch((err) => {
        setLoading(false)
        console.log(err)
        toast(
          <Toast
            title="System"
            message={err.message || 'Error Occured !'}
            type="error"
          />,
          {
            autoClose: 5000,
            closeButton: false,
          }
        )
      })
  }, [ID, isShow, postId, setCommentListByPost, token])

  const _toggleModal = useCallback(() => setIsShow((s) => !s), [setIsShow])
  return (
    <Modal
      title={
        <div className="flex items-center space-x-2">
          <Tippy
            content={
              <span dangerouslySetInnerHTML={{ __html: comment.content }} />
            }
          >
            <span>
              <Image
                width="40"
                height="40"
                src={account.photo || DEFAULT_AVATAR}
                className="rounded-full shadow-sm object-cover"
                alt={account.email}
              />
            </span>
          </Tippy>
          <span>
            {formatNumber(isNaN(replies?.length) ? 0 : replies?.length) || 0}{' '}
            Replies{' '}
          </span>
        </div>
      }
      colorTitle="gray"
      className="w-full md:w-7/12"
      isOpen={isShow}
      onClose={_toggleModal}
      trigger={
        <Tippy
          content={`${
            formatNumber(isNaN(replies?.length) ? 0 : replies?.length) || 0
          } replies`}
        >
          <button
            onClick={_toggleModal}
            type="button"
            className="px-2 py-1 rounded-md outline-none hover:shadow-sm hover:bg-indigo-50 dark:hover:bg-gray-800 flex items-center space-x-1 text-indigo-500 dark:hover:text-indigo-400 text-xs transition duration-300"
          >
            <ReplyIcon className="h-4" />
            <span>Reply</span> <span>&middot;</span>
            <span>
              {formatNumber(isNaN(replies?.length) ? 0 : replies?.length) || 0}
            </span>
          </button>
        </Tippy>
      }
    >
      <>
        <div className="my-4 overflow-y-auto h-[450px] flex flex-col space-y-4 max-w-[700px] mx-auto p-2">
          {replies?.length <= 0 && (
            <div className="flex flex-col items-center justify-center">
              <EmptyReply />
              <h1 className="text-xl text-indigo-500 font-bold">
                No reply yet.
              </h1>
            </div>
          )}
          {replies?.length > 0 &&
            replies?.map((reply) => (
              <Reply
                key={reply.id}
                reply={reply}
                postId={postId}
                token={token}
              />
            ))}
          {loading && <Spinner double size="8" />}
        </div>
        <div className="py-2 px-1 shadow-sm bg-gray-50 dark:bg-gray-900 rounded-md relative">
          <CreateReply token={token} commentId={ID} postId={postId} />
        </div>
      </>
    </Modal>
  )
}

export default memo(ListReplies)
