import { PaperAirplaneIcon, PaperClipIcon } from '@heroicons/react/outline'
import { useCallback, useState } from 'react'
import TextareaAutosize from 'react-textarea-autosize'
import { toast } from 'react-toastify'
import { useSetRecoilState } from 'recoil'
import { messageListState } from '../atoms'
import { cipher, myDbQuery } from '../utils'
import { endPointPost } from '../utils/api.config'
import Toast from './Toast'

function CreateMessage({ token, conversation }) {
  const [loading, setLoading] = useState(false)
  const [content, setContent] = useState('')
  const setMessages = useSetRecoilState(messageListState)

  const _sendMessage = useCallback(
    /**
     * @param {FormEvent<HTMLFormElement>} e
     */
    async (e) => {
      if (
        (e.charCode !== 13 && e.code !== 'Enter' && e.type !== 'click') ||
        e.shiftKey
      )
        return

      if (!content.trim()) return

      const message = {
        conversationId: conversation.id,
        content: cipher()(content),
        format: 'text',
      }

      try {
        const newMessage = await myDbQuery(
          endPointPost('messages'),
          token,
          message,
          'POST'
        )

        if (newMessage.errors || newMessage.error)
          throw new Error(newMessage?.error || 'Error Occured !')

        setContent('')

        setMessages((msg) => [...msg, newMessage])
      } catch (error) {
        setLoading(false)
        console.log(err)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [content, conversation.id, setMessages, token]
  )

  const handleMessage = useCallback((e) => setContent(e.target.value), [])
  return (
    <div className="flex items-start pb-14 pt-1 border-t border-gray-200 dark:border-gray-800 min-h-[120px] max-h-[122px] overflow-y-auto">
      <button
        title="Files"
        className="pt-3 pl-4 dark:text-gray-400 text-gray-600"
      >
        <PaperClipIcon className="h-5" />
      </button>
      <TextareaAutosize
        autoFocus
        required
        rows={1}
        value={content}
        onChange={handleMessage}
        onKeyPress={_sendMessage}
        disabled={loading}
        placeholder="Type your message..."
        className="w-full outline-none border-none focus:ring-0 h-10 bg-transparent placeholder-gray-400 dark:placeholder-gray-600 px-5 text-sm dark:text-white"
      />
      <button
        type="button"
        title="Send"
        disabled={!!content || loading}
        onClick={_sendMessage}
        className="pt-3 px-4 dark:text-gray-400 text-gray-600 hover:text-indigo-500 transition-colors"
      >
        <PaperAirplaneIcon className="h-5" />
      </button>
    </div>
  )
}

export default CreateMessage
