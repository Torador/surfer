import { useState } from 'react'
import Modal from '../components/Modal'
import FormProfile from './FormProfile'

/**
 * MODAL DRAG PIC
 * @param {{title?: string | JSX.Element}} props
 * @returns {JSX.Element}
 *
 */
function ModalProfile({
  me,
  token,
  title = 'Fill in your account information',
}) {
  const [isShow, setIsShow] = useState(true)

  return (
    <Modal title={title} isOpen={isShow}>
      <FormProfile setIsModal={setIsShow} me={me} token={token} />
    </Modal>
  )
}

export default ModalProfile
