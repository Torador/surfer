import router from 'next/router'
import { useCallback, useRef, useState } from 'react'
import { toast } from 'react-toastify'
import { useRecoilValue } from 'recoil'
import { currentUserLogin } from '../atoms'
import { myDbQuery } from '../utils'
import { endPointPut } from '../utils/api.config'
import Input from './Input'
import Loader from './Loader'
import Modal from './Modal'
import ProfilImg from './ProfilImg'
import Toast from './Toast'

function ModalInfoComplete({ isShow, token }) {
  const currentUser = useRecoilValue(currentUserLogin)
  const [loading, setLoading] = useState(false)
  const accountInfo = currentUser?.accounts[0]
  const filePickerRef = useRef(null)

  const _saveInfo = useCallback(
    async (e) => {
      setLoading(true)
      e.preventDefault()
      e.stopPropagation()
      const arrDate = e.target[4].value.split('-')
      const birthdate = `${arrDate[2]}/${arrDate[1]}/${arrDate[0]}`
      const account = {
        firstname: e.target[2].value,
        lastname: e.target[3].value,
        birthdate,
        email: accountInfo.email,
        phone: e.target[5].value,
        address: e.target[6].value,
        city: e.target[7].value,
        codePostal: e.target[8].value,
        job: e.target[9].value,
      }
      if (e.target[0].files[0]) account.picture = e.target[0].files[0]

      try {
        const response = await myDbQuery(
          endPointPut('accounts', null),
          token,
          account,
          'PUT',
          account.picture ? 'formdata' : 'json'
        )

        setLoading(false)
        if (response.errors || response.error)
          throw new Error(response.error || 'ERROR OCCURED !')

        router.reload()
      } catch (error) {
        setLoading(false)
        toast(<Toast title="Système" message={error.message} type="error" />, {
          closeButton: false,
          autoClose: 5000,
        })
      }
    },
    [accountInfo?.email, token]
  )

  return (
    <Modal
      isOpen={isShow}
      title="What's more about you ?"
      colorTitle="gray"
      className="w-full md:w-6/12"
    >
      <form onSubmit={_saveInfo} className="flex flex-col p-2 space-y-2">
        <div className="flex items-center justify-center">
          <div className="group shadow relative w-44 h-44 rounded-full">
            <ProfilImg
              filePickerRef={filePickerRef}
              photoUrl={accountInfo?.photo}
            />
          </div>
        </div>
        <div className="flex items-center pt-10 space-x-2">
          <div className="flex-grow">
            <label htmlFor="firtname_account" className="s_label">
              Firstname
            </label>
            <Input
              required
              id="firtname_account"
              placeholder="Enter your firstname"
            />
          </div>
          <div className="flex-grow">
            <label htmlFor="lastname_account" className="s_label">
              Lastname
            </label>
            <Input
              required
              id="lastname_account"
              placeholder="Enter your lastname"
            />
          </div>
        </div>
        <div className="flex items-center space-x-2">
          <div className="flex-grow">
            <label htmlFor="birthday_account" className="s_label">
              Birthday
            </label>
            <Input
              type="date"
              id="birthday_account"
              placeholder="yyyy/mm/dd"
              max="2007-12-31"
              required
            />
          </div>
          <div className="flex-shrink">
            <label htmlFor="phone_account" className="s_label">
              Phone
            </label>
            <Input
              type="tel"
              id="phone_account"
              placeholder="Enter your phone number"
              required
            />
          </div>
        </div>
        <div className="flex items-center space-x-2">
          <div className="flex-grow">
            <label htmlFor="address_account" className="s_label">
              Address
            </label>
            <Input
              required
              id="address_account"
              placeholder="Enter your address"
            />
          </div>
          <div className="flex-grow">
            <label htmlFor="city_account" className="s_label">
              City
            </label>
            <Input required id="city_account" placeholder="Enter your city" />
          </div>
          <div className="flex-grow">
            <label htmlFor="cp_account" className="s_label">
              Street Code
            </label>
            <Input
              type="number"
              required
              id="cp_account"
              placeholder="Enter your street code"
            />
          </div>
        </div>

        <div className="flex flex-col">
          <label htmlFor="job_account" className="s_label">
            Job
          </label>
          <Input required id="job_account" placeholder="Enter your job" />
        </div>

        <div className="flex items-center justify-end pt-5">
          <button
            disabled={loading}
            type="submit"
            className={`${
              loading
                ? 'text-gray-500 bg-gray-200 dark:bg-gray-700 cursor-not-allowed'
                : 'text-white bg-indigo-500 dark:bg-indigo-800 hover:bg-indigo-600 dark:hover:bg-indigo-900 hover:shadow-xl'
            } shadow-sm transition duration-300 py-2 px-3 w-3/12 rounded-md flex items-center space-x-1 justify-center`}
          >
            {loading ? (
              <Loader color="text-indigo-500 fill-current" />
            ) : (
              'Save Info'
            )}
          </button>
        </div>
      </form>
    </Modal>
  )
}

export default ModalInfoComplete
