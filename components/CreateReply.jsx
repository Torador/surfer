import Image from 'next/image'
import { useCallback, useState } from 'react'
import TextareaAutosize from 'react-textarea-autosize'
import { toast } from 'react-toastify'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { currentUserLogin, postListState } from '../atoms'
import { myDbQuery } from '../utils'
import { endPointPost } from '../utils/api.config'
import { DEFAULT_AVATAR } from '../utils/const'
import Spinner from './Spinner'
import Toast from './Toast'

function CreateReply({ token, commentId, postId }) {
  const user = useRecoilValue(currentUserLogin)
  const setCommentListByPost = useSetRecoilState(postListState)
  const [loading, setLoading] = useState(false)
  const [content, setContent] = useState('')

  const submitReply = useCallback(
    async (e) => {
      if (
        (e.charCode !== 13 && e.code !== 'Enter' && e.type !== 'click') ||
        e.shiftKey
      )
        return
      if (!content.trim()) return
      const data = {
        commentId,
        content,
      }
      try {
        setLoading(true)
        const reply = await myDbQuery(
          endPointPost('reply'),
          token,
          data,
          'POST'
        )

        setLoading(false)
        if (reply.errors || reply.error)
          throw new Error(reply.error || JSON.stringify(reply.errors))

        setContent('')

        setCommentListByPost((posts) =>
          posts.map((post) => {
            if (post.id !== postId) return post
            return {
              ...post,
              comments: post.comments.map((comment) => {
                if (comment.id !== commentId) return comment

                return {
                  ...comment,
                  replies: [reply, ...comment.replies],
                }
              }),
            }
          })
        )
      } catch (error) {
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [commentId, content, postId, setCommentListByPost, token]
  )

  const handleContent = useCallback((e) => setContent(e.target.value), [])

  return (
    <div className="flex items-end space-x-1 m-2 sticky bottom-0">
      {loading ? (
        <Spinner />
      ) : (
        <Image
          src={user.accounts[0].photo || DEFAULT_AVATAR}
          width="35"
          height="35"
          objectFit="cover"
          className="rounded-full flex-shrink"
          alt={user.username}
        />
      )}
      <TextareaAutosize
        disabled={loading}
        autoFocus
        required
        rows={1}
        value={content}
        onChange={handleContent}
        onKeyPress={submitReply}
        placeholder="Reply here..."
        className={`border-none flex-grow w-full ${
          loading ? 'bg-gray-100 cursor-not-allowed' : 'bg-white'
        } shadow-sm dark:bg-gray-800 px-3 py-1 text-sm focus:ring-0 focus:border-b-1 dark:text-white focus:border-indigo-500 rounded-md`}
      />
      <button
        type="button"
        disabled={content.trim().length <= 0 || loading}
        onClick={submitReply}
        className={`text-xs whitespace-nowrap rounded-md px-2 py-1  ${
          content.trim().length <= 0
            ? 'bg-gray-300 dark:bg-gray-800 dark:text-gray-700 cursor-not-allowed'
            : 'bg-indigo-500 hover:bg-indigo-600'
        } text-white  transition-colors shadow-sm`}
      >
        Add reply
      </button>
    </div>
  )
}

export default CreateReply
