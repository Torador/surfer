import Link from 'next/link'

/**
 * TAG COMPONENT
 * @param {{label: string, description?: string}} props
 * @returns {JSX.Element}
 */
function TagTopic({ label, description = '' }) {
  return (
    <Link href="#">
      <a
        className="px-3 py-1 rounded-sm bg-gray-100 text-gray-500 hover:shadow-sm hover:text-indigo-500 transition duration-300 text-xs whitespace-nowrap mr-1 mb-0.5"
        title={description}
      >
        {label}
      </a>
    </Link>
  )
}

export default TagTopic
