import { Transition } from '@headlessui/react'
import {
  CheckCircleIcon,
  XCircleIcon,
  InformationCircleIcon,
  ExclamationCircleIcon,
} from '@heroicons/react/outline'
import Image from 'next/image'
import { memo, useState } from 'react'
import { parseDate } from '../utils'

/**
 * TOAST COMPONENT
 * @param {{ logoUrl?: string, title: string, message: string, date?: Date, type?: 'default' | 'success' | 'warning' | 'error' | 'info', actions?: JSX.Element }} props
 * @returns {JSX.Element}
 * @author Jordan Kagmeni (Torador)
 */
function Toast({
  logoUrl = '/logo.png',
  type = 'default',
  date,
  title,
  message,
  actions,
}) {
  const [dp, setDp] = useState(true)
  let borderColor = ''
  switch (type) {
    case 'error':
      borderColor = 'border-red-300 dark:border-red-900'
      break
    case 'info':
      borderColor = 'border-surfer dark:border-surfer'
      break
    case 'success':
      borderColor = 'border-green-300 dark:border-green-900'
      break
    case 'warning':
      borderColor = 'border-yellow-300 dark:border-yellow-900'
      break
    default:
      borderColor = 'border-gray-200 dark:border-gray-700'
      break
  }
  return (
    <Transition
      onClick={() => setDp(false)}
      as="div"
      enter="transform translate-x-4 transition duration-[300ms]"
      enterFrom="opacity-0 translate-x-10"
      enterTo="opacity-1 -translate-x-4"
      leave="transform translate-x-4 transition duration-[300ms]"
      leaveFrom="opacity-1 -translate-x-4"
      leaveTo="opacity-0 translate-x-10"
      className={`s_toast ${borderColor}`}
      show={dp}
    >
      <div className="flex flex-col gap-y-2">
        <div className="flex justify-between items-center">
          <div className="flex gap-x-1 item-center">
            <div className="relative h-5 w-5">
              <Image
                src={logoUrl}
                className="rounded-full shadow-sm filter brightness-105"
                layout="fill"
                alt="logo_toast"
              />
            </div>
            <h3 className="text-gray-500 dark:text-gray-400 text-sm font-light flex-grow">
              Surfer
            </h3>
          </div>
          <div className="text-gray-500 dark:text-gray-400 text-xs flex items-center">
            {date ? parseDate(date) : "À l'instant"}
          </div>
        </div>
        <div className="flex justify-between item-center">
          <h3 className="text-gray-800 dark:text-gray-200 text-sm font-bold flex-grow truncate w-[200px]">
            {title}
          </h3>
          {type === 'success' && (
            <CheckCircleIcon className="h-5 text-green-500" />
          )}
          {type === 'error' && <XCircleIcon className="h-5 text-red-500" />}
          {type === 'info' && (
            <InformationCircleIcon className="h-5 text-surfer" />
          )}
          {type === 'warning' && (
            <ExclamationCircleIcon className="h-5 text-yellow-500" />
          )}
        </div>
        <div className="flex">
          <p className="text-sm text-gray-500 dark:text-gray-300">{message}</p>
        </div>
        {actions}
      </div>
    </Transition>
  )
}

export default memo(Toast)
