import Image from 'next/image'
import { EyeIcon, StarIcon } from '@heroicons/react/outline'
import Tippy from '@tippyjs/react'
import router from 'next/router'
import Rating from 'react-rating-stars-component'
import React, { useCallback, useState } from 'react'
import { formatNumber } from '../utils'
import { COLORS_RATING, DEFAULT_AVATAR } from '../utils/const'
import Modal from './Modal'
import Spinner from './Spinner'
import { currentUserLogin } from '../atoms'
import { useRecoilValue } from 'recoil'

function RateDetails({ rateAccounts, changeRate }) {
  const [isShowDetails, setIsShowDetails] = useState(false)
  const user = useRecoilValue(currentUserLogin)

  const _toggleDetailRate = useCallback(() => setIsShowDetails((s) => !s), [])
  return (
    <Modal
      isOpen={isShowDetails}
      trigger={
        <Tippy content={`${formatNumber(rateAccounts?.length)} reactions`}>
          <button
            onClick={_toggleDetailRate}
            className="flex justify-center items-center space-x-0.5 rounded-full p-2 hover:bg-indigo-50 dark:hover:bg-gray-800 hover:shadow-sm transition duration-300"
          >
            <span>{formatNumber(rateAccounts?.length)}</span>
            <EyeIcon className="h-5 w-5" />
          </button>
        </Tippy>
      }
      onClose={_toggleDetailRate}
      title="Reactions"
      colorTitle="gray"
      className="w-full md:w-5/12"
    >
      <div className="flex flex-col w-full space-y-2">
        {rateAccounts?.map((account) => (
          <div
            className="flex justify-between w-full items-center rounded-md shadow-sm dark:shadow-none dark:bg-transparent border-b dark:border-gray-800 p-2 hover:bg-gray-100 dark:hover:bg-gray-800 dark:bg-opacity-60 transition-colors bg-white"
            key={account.id}
          >
            <div className="flex items-center space-x-2">
              <button
                type="button"
                onClick={() => router.push(`/profil/${account?.slug}`)}
                className="relative h-14 w-14 rounded-full"
              >
                <Image
                  src={account?.photo || DEFAULT_AVATAR}
                  layout="fill"
                  alt={account.email}
                  className="object-cover rounded-full"
                />
              </button>
              <div className="flex flex-col items-start">
                <h1 className="text-sm font-bold text-gray-800 dark:text-gray-50 max-w-[150px] truncate">
                  {account?.lastname} {account?.firstname}
                </h1>
                {account.job ? (
                  <p className="text-gray-500 text-xs max-w-[190px] truncate">
                    {account.job} &middot; {account.city}
                  </p>
                ) : (
                  <p className="text-gray-500 text-xs max-w-[190px] truncate">
                    {account.city}
                  </p>
                )}
              </div>
            </div>
            <div className="flex items-center space-x-1">
              <Rating
                count={5}
                value={account.Rating}
                emptyIcon={<StarIcon className="h-3 w-3" />}
                halfIcon={<StarIcon className="h-3 w-3" />}
                edit={user.accounts[0]?.id === account.id}
                onChange={changeRate}
                activeColor={COLORS_RATING[COLORS_RATING.length - 1]}
              />
              {/* <button
                type="button"
                className="px-2 py-1 rounded-full text-white font-semibold text-xs shadow bg-gradient-to-tr from-indigo-500 to-blue-400 dark:to-blue-600 hover:shadow-lg transform active:scale-95 transition duration-300"
              >
                Follow
              </button> */}
            </div>
          </div>
        ))}
      </div>
    </Modal>
  )
}

export default RateDetails
