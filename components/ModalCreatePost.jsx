import { PlusCircleIcon, UserAddIcon } from '@heroicons/react/outline'
import { useEffect, useState, useCallback } from 'react'
import Modal from '../components/Modal'
import CreatePost from './CreatePost'
import FormProfile from './FormProfile'

/**
 * MODAL DRAG PIC
 * @param {{className: string, title?: string | JSX.Element}} props
 * @returns {JSX.Element}
 *
 */
function ModalCreatePost({ className, title = 'Create a new post' }) {
  const [isShow, setIsShow] = useState(false)
 
  const _toggleModal = useCallback(() => {
    setIsShow((s) => !s)
  }, [])

  const t = (
    <>
      <button
        className={`hidden sm:inline ${className}`}
        onClick={_toggleModal}
      >
        <PlusCircleIcon className="navBtn" />
      </button>
      <button className={`sm:hidden ${className}`} onClick={_toggleModal}>
        <UserAddIcon className="h-5" />
      </button>
    </>
  )

  return (
    <Modal trigger={t} title={title} isOpen={isShow} onClose={_toggleModal}>
      <CreatePost />
    </Modal>
  )
}

export default ModalCreatePost
