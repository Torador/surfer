import Image from 'next/image'
import { Transition } from '@headlessui/react'
import { toast } from 'react-toastify'
import TextareaAutosize from 'react-textarea-autosize'
import { DEFAULT_AVATAR } from '../utils/const'
import { useCallback, useState } from 'react'
import { myDbQuery } from '../utils'
import { endPointPost } from '../utils/api.config'
import { useSetRecoilState } from 'recoil'
import { postListState } from '../atoms'
import Spinner from './Spinner'
import Toast from './Toast'

/**
 * CREATE COMMENT COMPONENT
 * @param {{isShow: boolean, photoUser: string, isCommentlock: boolean, postId: number, token: string, defaultValue?: string}} props
 * @returns {JSX.Element}
 */
function CreateComment({
  isShow,
  photoUser,
  isCommentlock,
  postId,
  token,
  defaultValue = '',
}) {
  const [content, setContent] = useState(defaultValue)
  const [loading, setLoading] = useState(false)
  const setCommentListByPost = useSetRecoilState(postListState)

  const submitComment = useCallback(
    async (e) => {
      if (
        (e.charCode !== 13 && e.code !== 'Enter' && e.type !== 'click') ||
        e.shiftKey
      )
        return
      if (!content.trim()) return
      const data = {
        postId,
        content,
      }
      try {
        setLoading(true)
        const comment = await myDbQuery(
          endPointPost('comments'),
          token,
          data,
          'POST'
        )

        setLoading(false)
        if (comment.errors || comment.error)
          throw new Error(comment.error || JSON.stringify(comment.errors))

        setContent('')

        setCommentListByPost((posts) =>
          posts.map((post) => {
            if (post.id !== postId) return post
            return {
              ...post,
              comments: [comment, ...post.comments],
            }
          })
        )
      } catch (error) {
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [content, postId, setCommentListByPost, token]
  )

  const handleContent = useCallback((e) => setContent(e.target.value), [])
  return (
    <Transition
      as="div"
      className="flex items-center space-x-1 m-2"
      show={isShow}
      enter="transform transition duration-150"
      enterFrom="opacity-0 translate-y-0"
      enterTo="opacity-1 translate-y-1"
      leave="transform transition duration-150"
      leaveFrom="opacity-1 translate-y-1"
      leaveTo="opacity-0 translate-y-0"
    >
      {loading ? (
        <Spinner />
      ) : (
        <Image
          src={photoUser || DEFAULT_AVATAR}
          width="35"
          height="35"
          objectFit="cover"
          className="rounded-full flex-shrink"
          alt={photoUser}
        />
      )}
      <TextareaAutosize
        disabled={isCommentlock || loading}
        title={isCommentlock ? `Comments are muted.` : 'Add Comments.'}
        autoFocus
        required
        value={content}
        onChange={handleContent}
        onKeyPress={submitComment}
        placeholder="Add your comment here..."
        className={`border-none flex-grow w-full ${
          isCommentlock || loading
            ? 'bg-gray-100 cursor-not-allowed'
            : 'bg-white'
        } shadow-sm dark:bg-gray-900 px-3 py-2 text-sm focus:ring-0 focus:border-b-1 dark:text-white focus:border-indigo-500 rounded-md`}
      />
      <button
        type="button"
        disabled={content.trim().length <= 0}
        onClick={submitComment}
        className={`text-xs whitespace-nowrap rounded-md px-2 py-1  ${
          content.trim().length <= 0
            ? 'bg-gray-300 dark:bg-gray-800 dark:text-gray-700 cursor-not-allowed'
            : 'bg-indigo-500 hover:bg-indigo-600'
        } text-white  transition-colors shadow-sm`}
      >
        Add comment
      </button>
    </Transition>
  )
}

export default CreateComment
