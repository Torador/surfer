import Link from 'next/link'
import React, { memo, useCallback, useMemo, useState } from 'react'
import {
  BadgeCheckIcon,
  BriefcaseIcon,
  ChatIcon,
  ClockIcon,
  DotsVerticalIcon,
  ExclamationIcon,
  LocationMarkerIcon,
  PencilIcon,
  ShareIcon,
  TrashIcon,
} from '@heroicons/react/outline'
import { LockClosedIcon, LockOpenIcon, StarIcon } from '@heroicons/react/solid'
import { formatDistanceToNow } from 'date-fns'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import Tippy from '@tippyjs/react'
import Rating from 'react-rating-stars-component'
import Dropdown from './Dropdown'
import TagTopic from './Tag'
import InteractorList from './InteractionUsers'
import { COLORS_RATING, DELETE_ENTITY } from '../utils/const'
import {
  accountListState,
  postListState,
  selectedTopic,
  suggestionListState,
} from '../atoms'
import { formatNumber, myDbQuery } from '../utils'
import {
  endPointCustom,
  endPointDelete,
  endPointLock,
  endPointRate,
} from '../utils/api.config'
import { toast } from 'react-toastify'
import Toast from './Toast'
import Badge from './Badge'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import CommentList from './CommentList'
import CarouselFiles from './CarouselFiles'
import useRateAverage from '../utils/hooks/useRateAverage'
import PhotoLink from './photoLink'
import RateDetails from './RateDetails'

// @ts-check

/**
 * POST COMPONENT
 * @param {{post: Post, user: User, token: string}} props
 * @returns {JSX.Element}
 */
function Post({ post, user, token }) {
  const [rating, setRating] = useRateAverage(post?.accounts)
  const [isLoading, setIsLoading] = useState(false)
  const [isShow, setIsShow] = useState(false)
  const topicSelected = useRecoilValue(selectedTopic)

  const [accounts, setAccounts] = useRecoilState(accountListState)
  const setPostList = useSetRecoilState(postListState)
  const setSuggestion = useSetRecoilState(suggestionListState)

  const opts = [
    [
      {
        icon: <ShareIcon className="w-5 h-5 mr-2" />,
        label: 'Share post',
        action: async () => {
          try {
            await navigator.clipboard.writeText(
              `${process.env.NEXT_PUBLIC_HOST}/posts/${post.slug}`
            )
            toast(
              <Toast
                title="System"
                message="The link of the post has been copied to the clipboad."
                type="info"
              />,
              {
                autoClose: 5000,
                closeButton: false,
              }
            )
          } catch (error) {
            toast(
              <Toast
                title="System"
                message="Error occured when copied in clipboad."
                type="error"
              />,
              {
                autoClose: 5000,
                closeButton: false,
              }
            )
          }
        },
      },
      {
        icon: <ExclamationIcon className="w-5 h-5 mr-2" />,
        label: 'Report',
        action: () => {},
      },
    ],
  ]

  const [account] = useMemo(
    () => accounts?.filter((acc) => acc?.id === post?.account_id),
    [accounts, post?.account_id]
  )

  const moreOptionUser = [
    {
      icon: <PencilIcon className="w-5 h-5 mr-2" />,
      label: 'Edit post',
      action: () => {},
    },
    {
      icon: post.is_comment_lock ? (
        <LockClosedIcon className="w-5 h-5 mr-2" />
      ) : (
        <LockOpenIcon className="w-5 h-5 mr-2" />
      ),
      label: post.is_comment_lock ? 'Unlock comments' : 'Lock comments',
      action: async () => {
        setPostList((pt) =>
          pt.map((p) => {
            if (p.id !== post.id) return p
            return {
              ...p,
              is_comment_lock: !p.is_comment_lock,
            }
          })
        )
        try {
          const response = await myDbQuery(
            endPointLock('posts', post.id),
            token
          )
          if (response.errors || response.error)
            throw new Error(response.error || 'ERROR OCCURED !')
          toast(
            <Toast
              title="Système"
              message={`Les commentaires ont bien été ${
                post.is_comment_lock ? 'activés' : 'désactivés'
              }.`}
              type="success"
            />
          )
        } catch (error) {
          console.log(error)
          toast(<Toast title="Système" message={error.message} type="error" />)
        }
      },
    },
    {
      icon: <TrashIcon className="w-5 h-5 mr-2" />,
      label: 'Delete post',
      color: 'red',
      action: async () => {
        if (!confirm(DELETE_ENTITY('post'))) return
        setPostList((p) => p.filter((p) => p.id !== post.id))
        try {
          const response = await myDbQuery(
            endPointDelete('posts', post.id),
            token,
            null,
            'DELETE'
          )
          if (response.errors || response.error)
            throw new Error(response.error || 'ERROR OCCURED !')
          toast(
            <Toast
              title="Système"
              message={`Le post a bien été supprimé.`}
              type="success"
            />
          )
        } catch (error) {
          toast(<Toast title="Système" message={error.message} type="error" />)
        }
      },
    },
  ]
  /**
   * @type {OptionDropdown}
   */
  const options =
    user.accounts[0].id === post.account_id ? [...opts, moreOptionUser] : opts

  const handleShowComment = useCallback(() => {
    !post.is_comment_lock && setIsShow((s) => !s)
  }, [post.is_comment_lock])

  const submitRate = useCallback(
    async (rate) => {
      setRating(rate)
      const data = {
        post_id: post.id,
        rating: rate,
      }
      try {
        /**
         * @type {Post}
         */
        const p = await myDbQuery(endPointRate('posts'), token, data, 'POST')

        if (p.errors || p.error)
          throw new Error(p.error || JSON.stringify(p.errors))
        setPostList((posts) => {
          return posts.map((post) => {
            if (post.id === p.id) {
              return p
            }
            return post
          })
        })
      } catch (error) {
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [post.id, setPostList, setRating, token]
  )

  const follow = useCallback(async () => {
    try {
      setIsLoading(true)
      const ctc = await myDbQuery(
        endPointCustom('follow', `create/${account.contacts[0].id}`),
        token
      )
      setIsLoading(false)
      if (ctc.errors || ctc.error)
        throw new Error(ctc.error || JSON.stringify(ctc.errors))

      const [newFollower] = ctc.followers.filter(
        (item) => item.account_id === user.accounts[0].id
      )

      setAccounts((accs) => {
        return accs.map((acc) => {
          if (acc.id !== account.id) return acc

          return {
            ...acc,
            contacts: [
              {
                ...acc.contacts[0],
                followers: [
                  ...acc.contacts[0].followers,
                  newFollower || user.accounts[0].followers[0],
                ],
              },
            ],
          }
        })
      })
      setSuggestion((accs) => {
        return accs.map((acc) => {
          if (acc.id !== account.id) return acc

          return {
            ...acc,
            contacts: [
              {
                ...acc.contacts[0],
                followers: [
                  ...acc.contacts[0].followers,
                  newFollower || user.accounts[0].followers[0],
                ],
              },
            ],
          }
        })
      })
    } catch (error) {
      console.log(error)
      toast(<Toast title="System" message="Error Occured !" type="error" />, {
        autoClose: 5000,
        closeButton: false,
      })
    }
  }, [
    account.contacts,
    account.id,
    setAccounts,
    setSuggestion,
    token,
    user.accounts,
  ])
  const unfollow = useCallback(async () => {
    try {
      setIsLoading(true)
      const contact = await myDbQuery(
        endPointCustom('follow', `delete/${account.contacts[0].id}`),
        token,
        null,
        'DELETE'
      )
      setIsLoading(false)
      if (contact.errors || contact.error)
        throw new Error(contact.error || JSON.stringify(contact.errors))

      setAccounts((accs) => {
        return accs.map((acc) => {
          if (acc.id !== account.id) return acc

          return {
            ...acc,
            contacts: [
              {
                ...acc.contacts[0],
                followers: acc.contacts[0].followers.filter(
                  (flw) => flw.account_id !== user.accounts[0].id
                ),
              },
            ],
          }
        })
      })
      setSuggestion((accs) => {
        return accs.map((acc) => {
          if (acc.id !== account.id) return acc

          return {
            ...acc,
            contacts: [
              {
                ...acc.contacts[0],
                followers: acc.contacts[0].followers.filter(
                  (flw) => flw.account_id !== user.accounts[0].id
                ),
              },
            ],
          }
        })
      })
    } catch (error) {
      console.log(error)
      toast(<Toast title="System" message="Error Occured !" type="error" />, {
        autoClose: 5000,
        closeButton: false,
      })
    }
  }, [
    account.contacts,
    account.id,
    setAccounts,
    setSuggestion,
    token,
    user.accounts,
  ])

  const isAlreadyFollow = useCallback(() => {
    if (account.contacts === undefined) return

    const [f] = account?.contacts[0]?.followers.filter(
      (follower) => follower.account_id === user.accounts[0].id
    )
    return !!f
  }, [account?.contacts, user.accounts])

  return (
    <div className="flex flex-col w-full border border-gray-200 sm:border-none shadow-sm sm:shadow dark:border dark:border-gray-800 rounded-md">
      <div
        className={`grid grid-cols-1 gap-y-3 p-3 bg-white dark:bg-gray-900 hover:bg-gray-50 relative transition-colors ${
          isShow ? 'rounded-t-md' : 'rounded-md'
        }`}
      >
        <div className="flex items-center justify-between">
          <div className="flex items-center justify-start flex-grow">
            <PhotoLink account={account} />
            <div className="flex flex-col justify-between max-w-[400px]">
              <div className="flex space-x-2 items-center">
                <Link href={`/profil/${account?.slug}`}>
                  <a className="truncate font-extrabold hover:underline text-md dark:text-gray-50">
                    {!account?.shortName ? 'Guest' : account?.shortName}
                  </a>
                </Link>
                {account?.shortName && account?.photo && (
                  <BadgeCheckIcon className="h-5 w-5 text-indigo-500" />
                )}
              </div>
              <p className="text-gray-500 text-xs flex items-center space-x-1">
                {/* <div className="flex space-x-1 items-center">
                  <BriefcaseIcon className="h-3 w-3" />
                  <span>{account?.email}</span>
                </div>
                <div className="w-0.5 h-0.5 rounded-full bg-gray-400" /> */}
                {account.city && (
                  <>
                    <div className="flex space-x-1 items-center">
                      <LocationMarkerIcon className="h-3 w-3" />
                      <span>{account.city}</span>
                    </div>
                    <div className="w-0.5 h-0.5 rounded-full bg-gray-400" />
                  </>
                )}

                {account.job && (
                  <>
                    <div className="flex space-x-1 items-center">
                      <BriefcaseIcon className="h-3 w-3" />
                      <span>{account.job}</span>
                    </div>
                    <div className="w-0.5 h-0.5 rounded-full bg-gray-400" />
                  </>
                )}

                <div className="flex space-x-1 items-center">
                  <ClockIcon className="h-3 w-3" />
                  <span className="text-[10px] text-gray-500">
                    {formatDistanceToNow(new Date(post.created_at), {
                      addSuffix: true,
                    })}
                  </span>
                </div>
              </p>
            </div>
          </div>
          <div className="flex space-x-2 items-center">
            <Tippy
              content={
                !isAlreadyFollow()
                  ? `Stay informed of ${
                      !account.shortName
                        ? user.username
                        : account.shortName.split(' ')[0]
                    }'s activities`
                  : `Unfollow ${
                      !account.shortName
                        ? user.username
                        : account.shortName.split(' ')[0]
                    }`
              }
            >
              {account.id !== user.accounts[0].id &&
                (isAlreadyFollow() ? (
                  <button
                    disabled={isLoading}
                    onClick={unfollow}
                    className="text-indigo-500 text-xs hover:underline font-bold"
                  >
                    Unfollow
                  </button>
                ) : (
                  <button
                    disabled={isLoading}
                    onClick={follow}
                    className="text-indigo-500 text-xs hover:underline font-bold"
                  >
                    Follow
                  </button>
                ))}
            </Tippy>
            <Dropdown
              trigger={
                <button
                  title="options"
                  className="text-gray-500 p-1 rounded-full hover:text-indigo-500 flex justify-center items-center transition-colors"
                >
                  <DotsVerticalIcon className="h-5" />
                </button>
              }
              options={options}
            />
          </div>
        </div>
        {post?.topics?.length > 0 && (
          <div className="flex flex-wrap items-center -mb-2">
            {post.topics.map((topic, i) => {
              if (i <= 11) {
                return (
                  <Badge
                    color={
                      topicSelected && topic.id === topicSelected.id
                        ? 'yellow'
                        : 'indigo'
                    }
                    key={topic.id}
                    label={topic.label}
                    description={topic.description}
                  />
                )
              }
            })}
            {post.topics.length > 11 && (
              <TagTopic label={`+${formatNumber(post.topics.length - 11)}`} />
            )}
          </div>
        )}
        <h1 className="mt-3 font-bold text-2xl text-gray-800 dark:text-gray-100 line-clamp-2">
          {post.title}
        </h1>
        <p
          className="text-gray-500 dark:text-gray-400 text-sm line-clamp-3"
          dangerouslySetInnerHTML={{
            __html: post.content,
          }}
        />
        {post?.files?.length > 0 && <CarouselFiles files={post?.files} />}
        <div
          className={
            post?.files?.length > 1
              ? 'flex items-center justify-between -mt-5'
              : 'flex items-center justify-between'
          }
        >
          <div className="flex flex-col z-10">
            <Tippy
              content={
                post?.accounts?.filter(
                  (acc) => acc.id === user?.accounts[0].id
                )[0]?.Rating
                  ? 'Vous avez attribué(e) une note sur ce post.'
                  : 'Cliquez pour attribuer une note 😎'
              }
            >
              <span>
                <Rating
                  size={24}
                  count={5}
                  value={rating}
                  emptyIcon={<StarIcon className="h-3 w-3" />}
                  halfIcon={<StarIcon className="h-3 w-3" />}
                  onChange={submitRate}
                  activeColor={COLORS_RATING[COLORS_RATING.length - 1]}
                />
              </span>
            </Tippy>
            <InteractorList
              interators={post.accounts}
              user={user.accounts[0].id}
            />
          </div>
          <div className="flex items-center font-semibold text-xs text-indigo-500 z-10">
            <RateDetails
              rateAccounts={post?.accounts}
              changeRate={submitRate}
            />

            <Tippy content={`${post?.comments?.length} comments`}>
              <button
                title={
                  post.is_comment_lock
                    ? `Comments are muted by ${
                        user.accounts[0].id === post.account_id
                          ? 'You'
                          : account.shortName
                      }`
                    : ''
                }
                disabled={post.is_comment_lock}
                className={`flex-shrink flex justify-center items-center space-x-0.5 rounded-full p-2 ${
                  post.is_comment_lock
                    ? 'hover:bg-gray-50 text-gray-500 cursor-not-allowed dark:hover:bg-gray-800'
                    : 'hover:bg-indigo-50 dark:hover:bg-gray-800'
                } hover:shadow-sm transition duration-300`}
                onClick={handleShowComment}
              >
                <span>{formatNumber(post?.comments?.length)}</span>
                <ChatIcon className="h-5 w-5" />
              </button>
            </Tippy>
          </div>
        </div>
      </div>
      <CommentList
        comments={post?.comments || []}
        postId={post.id}
        token={token}
        photoUser={user?.accounts[0]?.photo}
        isCommentlock={post.is_comment_lock}
        isShow={isShow}
      />
    </div>
  )
}

export default memo(Post)
