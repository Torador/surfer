import { useEffect, useState } from 'react'
import Image from 'next/image'
import { myDbQuery } from '../utils'
import { endPointCustom } from '../utils/api.config'
import { DEFAULT_AVATAR } from '../utils/const'
import Spinner from './Spinner'
import { useRecoilState, useRecoilValue } from 'recoil'
import { currentUserLogin, suggestionListState } from '../atoms'
import Suggestion from './Suggestion'

function ListSuggestion({ token }) {
  const [loading, setLoading] = useState(false)
  const user = useRecoilValue(currentUserLogin)

  const [suggestions, setSuggestion] = useRecoilState(suggestionListState)
  useEffect(() => {
    setLoading(true)
    myDbQuery(endPointCustom('follow', 'suggestions'), token)
      .then((suggestions) => {
        setSuggestion(suggestions)
      })
      .catch((err) => {
        console.log(err)
      })
      .finally(() => {
        setLoading(false)
      })
  }, [setSuggestion, token])

  if (loading && !suggestions.length) return <Spinner color="indigo" double />
  if (suggestions.length === 0)
    return <p className="dark:text-white">No Suggestion !</p>
  return suggestions.map((suggestion) => {
    if (
      !suggestion?.contacts[0]?.followers?.filter(
        (f) => f.account_id === user.accounts[0].id
      )[0]
    ) {
      return (
        <Suggestion key={suggestion.id} suggestion={suggestion} token={token} />
      )
    }
  })
}

export default ListSuggestion
