/* eslint-disable @next/next/no-img-element */

/**
 *  CHIP COMPONENT
 * @param {{url: string, color: string }} props
 * @returns {JSX.Element}
 */
function Chip({ url, color = 'gray' }) {
  return (
    <span slot="avatar">
      <span
        className={`flex
          relative
          w-4
          h-4
          bg-${color}-500
          justify-center
          items-center
          m-1
          mr-2
          ml-0
          my-0
          text-xs
          rounded-full`}
      >
        <img className="rounded-full" alt={url} src={url} />
      </span>
    </span>
  )
}

export default Chip
