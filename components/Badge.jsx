import Tippy from '@tippyjs/react'
import { BG_COLORS, TEXT_COLORS } from '../utils/const'

/**
 * BASIC BADGE COMPONENT
 * @param {{type?: 'basic' | 'solid', label: string, description?: string, color?: string, shape?: boolean, sizeX?: string | number, sizeY?: string | number, removeMargin?: boolean}} props
 * @returns {JSX.Element}
 */
function Badge({
  type = 'basic',
  label,
  description = '',
  color = 'gray',
  shape = false,
  sizeX = '2',
  sizeY = '0.5',
  removeMargin = false,
}) {
  const badge = (
    <span
      className={`text-xs px-${sizeX} font-semibold ${BG_COLORS[color]} ${
        type === 'solid' ? '' : 'bg-opacity-10'
      }
      ${type === 'basic' ? `${TEXT_COLORS[color]}` : 'text-white'}
      ${shape ? 'rounded-full' : 'rounded-sm'}
      py-${sizeY} ${removeMargin ? '' : 'mr-2'} dark:shadow-sm`}
    >
      {label}
    </span>
  )

  if (!description) return badge
  return (
    <Tippy content={description} placement="bottom">
      {badge}
    </Tippy>
  )
}

export default Badge
