//@ts-check
import { Transition } from '@headlessui/react'
import Tippy from '@tippyjs/react'
import { PlusIcon } from '@heroicons/react/outline'
import { useCallback, useEffect, useState } from 'react'
import { useSetRecoilState } from 'recoil'
import { isOpenDialogPostState } from '../atoms'

/**
 *
 * @param {{me: User}} props
 * @returns {JSX.Element}
 */
function PostButton({ me }) {
  const [isShow, setShow] = useState(false)
  const setOpenModal = useSetRecoilState(isOpenDialogPostState)

  useEffect(() => {
    const onScroll = () => {
      setShow(window.scrollY > 188)
    }
    if (window !== undefined && me?.accounts[0]?.shortName)
      window.addEventListener('scroll', onScroll)

    return () => window.removeEventListener('scroll', onScroll)
  }, [me?.accounts, isShow])

  const openModalPost = useCallback(() => {
    setOpenModal(true)
  }, [setOpenModal])

  return (
    <div className="sticky bottom-10 flex justify-end pr-8 z-40 md:z-0 lg:hidden">
      <Tippy content="Add new post 🚀">
        <div>
          <Transition
            show={isShow}
            onClick={openModalPost}
            disabled={!me?.accounts[0]?.shortName}
            as="button"
            className="transform hover:scale-110 shadow-xl flex justify-center items-center p-5 rounded-full bg-gradient-to-tr from-indigo-400 dark:from-indigo-700 to-blue-500 dark:to-blue-600 text-white z-30 trantision duration-300"
            enter="transform transition duration-[300ms]"
            enterFrom="opacity-0 scale-50"
            enterTo="opacity-100 scale-125"
            leave="transform transition duration-[300ms]"
            leaveFrom="opacity-1 scale-125"
            leaveTo="opacity-0 scale-50"
          >
            <PlusIcon className="h-6 w-6" />
          </Transition>
        </div>
      </Tippy>
    </div>
  )
}

export default PostButton
