//@ts-check
import { memo } from 'react'
import Tippy from '@tippyjs/react'
import Image from 'next/image'
import { DEFAULT_AVATAR, LIMIT_VIEW_PERSONS_RATING } from '../utils/const'
import { formatNumber } from '../utils'
import Badge from './Badge'

/**
 *
 * @param {{interators: Account[], user: number}} param0
 * @returns
 */
function InteractorList({ interators, user }) {
  if (!interators) return null
  const length = interators.length - 1

  return (
    <div className="flex -space-x-2 overflow-hidden cursor-pointer">
      {interators.map((interator, i) => {
        if (i <= LIMIT_VIEW_PERSONS_RATING) {
          return (
            <Tippy
              key={i}
              content={`${
                interator.id === user
                  ? 'You'
                  : interator.shortName
                  ? interator.shortName
                  : interator.email
              }: ${interator.Rating}`}
            >
              <div className="relative h-6 w-6 rounded-full">
                <Image
                  className="inline-block rounded-full ring-2 ring-white dark:ring-gray-800"
                  layout="fill"
                  objectFit="cover"
                  src={interator.photo || DEFAULT_AVATAR}
                  alt={interator.email}
                  loading="lazy"
                />
              </div>
            </Tippy>
          )
        }
      })}
      {length > LIMIT_VIEW_PERSONS_RATING && (
        <Badge
          removeMargin
          shape
          color="indigo"
          sizeY={1}
          sizeX={2}
          label={`+${formatNumber(length - LIMIT_VIEW_PERSONS_RATING)}`}
          description={`${formatNumber(
            length - LIMIT_VIEW_PERSONS_RATING
          )} other ${
            length - LIMIT_VIEW_PERSONS_RATING > 1
              ? 'persons have'
              : 'person has'
          } rate this post.`}
        />
      )}
    </div>
  )
}

export default memo(InteractorList)
