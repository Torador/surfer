//@ts-check
import Link from 'next/link'
import { memo } from 'react'
import Divider from './Divider'
import Input from './Input'
import InputPassword from './InputPassword'
import Loader from './Loader'
import SocialButton from './SocialButton'

/**
 * FORM SIGN IN COMPONENT
 * @param {{ handleSubmit: (e: import('react').FormEvent) => void, isLoading: boolean }} props
 * @returns {JSX.Element}
 */
function FormSignIn({ handleSubmit, isLoading }) {
  return (
    <form onSubmit={handleSubmit} className="flex flex-col space-y-6 w-[300px]">
      <Input required type="email" placeholder="Enter email" />
      <div className="pb-2 flex flex-col space-y-0.5">
        <InputPassword placeholder="Password" />
        <Link href="/recovery">
          <a className="text-right text-xs font-semibold text-gray-400 hover:text-blue-500 transition-colors pr-3">
            Recovery Password
          </a>
        </Link>
      </div>
      <button
        disabled={isLoading}
        type="submit"
        className="transform bg-indigo-500 hover:-translate-y-1 hover:shadow-md hover:bg-indigo-600 py-3 px-2 text-white rounded-md transition duration-300 flex justify-center font-semibold w-full shadow-2xl"
      >
        {isLoading ? <Loader /> : 'Sign In'}
      </button>
      <div className="flex pt-5 w-full">
        <Divider text="or continue with" orientation="horizontal" />
      </div>
      <div className="flex justify-around">
        <SocialButton
          path="/SM/google.svg"
          title="Sign with Google"
          link={`${process.env.NEXT_PUBLIC_HOST_SERVER}/google/redirect`}
        />
        <SocialButton
          path="/SM/twitter.svg"
          title="Sign with Twitter"
          link={`${process.env.NEXT_PUBLIC_HOST_SERVER}/twitter/redirect`}
        />
        <SocialButton
          path="/SM/facebook.svg"
          title="Sign with Facebook"
          link={`${process.env.NEXT_PUBLIC_HOST_SERVER}/facebook/redirect`}
        />
      </div>
    </form>
  )
}

export default memo(FormSignIn)
