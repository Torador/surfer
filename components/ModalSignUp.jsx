import Modal from './Modal'
import { useState, useCallback } from 'react'
import FormSignUp from './FormSignUp'
import Toast from '../components/Toast'
import { toast } from 'react-toastify'
import { SYS_REGISTER } from '../utils/const'
import { UserAddIcon } from '@heroicons/react/outline'
import { endPointPost } from '../utils/api.config'
import { jsonFetch } from '../utils'

/**
 * MODAL SIGN UP COMPONENT
 * @param {{className: string, title?: string, triggerButton: string | JSX.Element}} props
 * @returns {JSX.Element}
 */
function ModalSignUp({ className, title = 'Sign Up', triggerButton }) {
  const [isShow, setIsShow] = useState(false)
  const [isLoading, setIsLoading] = useState(false)

  const handleSubmit = useCallback(
    /**
     * @param {import('react').FormEvent} e
     */
    async (e) => {
      e.stopPropagation()
      e.preventDefault()

      /**
       *  @type {{username: string, password: string}}
       */
      const dataUser = {
        email: e.target[0].value,
        username: e.target[1].value,
        password: e.target[2].value,
      }

      const confirmPass = e.target[4].value

      if (dataUser.email.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_REGISTER}
            message="Veuillez entrer votre nom d'utilisateur SVP."
            type="warning"
          />,

          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (dataUser.username.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_REGISTER}
            message="Veuillez entrer votre nom d'utilisateur SVP."
            type="warning"
          />,

          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (dataUser.username.toString().length < 2)
        return toast(
          <Toast
            title={SYS_REGISTER}
            message="Votre nom d'utilisateur doit être de 02 caractères minimum."
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (dataUser.password.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_REGISTER}
            message="Veuillez entrer votre mot de passe SVP."
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (dataUser.password.toString().length <= 8)
        return toast(
          <Toast
            title={SYS_REGISTER}
            message="Votre mot de passe doit être de 08 caractères minimum."
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (confirmPass.toString().trim() === '')
        return toast(
          <Toast
            title={SYS_REGISTER}
            message="Veuillez confirmer votre mot de passe"
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      if (confirmPass.toString().trim() !== dataUser.password.trim())
        return toast(
          <Toast
            title={SYS_REGISTER}
            message="Vos mots de passes ne sont pas identiques."
            type="warning"
          />,
          {
            closeButton: false,
            autoClose: true,
          }
        )

      const response = await jsonFetch(
        endPointPost('users'),
        {
          method: 'POST',
          body: JSON.stringify({ ...dataUser }),
        },
        false,
        SYS_REGISTER,
        setIsLoading
      )

      if (!response) {
        e.target[0].value = ''
        e.target[1].value = ''
        e.target[0].focus()
        return
      }

      toast(
        <Toast
          title={SYS_REGISTER}
          message="Félicitations, vous êtes un utilisateur de Surfer désormais."
          type="success"
        />,
        {
          closeButton: false,
          autoClose: true,
        }
      )
      setIsShow(false)
    },
    []
  )

  const _toggleModal = useCallback(() => {
    setIsShow((s) => !s)
  }, [])

  const t = (
    <>
      <button
        className={`hidden sm:inline ${className}`}
        onClick={_toggleModal}
      >
        {triggerButton}
      </button>
      <button className={`sm:hidden ${className}`} onClick={_toggleModal}>
        <UserAddIcon className="h-5" />
      </button>
    </>
  )

  return (
    <Modal trigger={t} title={title} isOpen={isShow} onClose={_toggleModal}>
      <FormSignUp isLoading={isLoading} handleSubmit={handleSubmit} />
    </Modal>
  )
}

export default ModalSignUp
