/**
 * HEADER ICON
 * @param {{Icon: JSX.Element, active: boolean}} props
 * @returns
 */
function HeaderIcon({ Icon, active }) {
  return (
    <div className="flex items-center cursor-pointer md:px-10 h-12 md:hover:bg-indigo-50 md:dark:hover:bg-indigo-900 md:dark:hover:bg-opacity-40 rounded-xl active:border-b-2 active:border-indigo-500 group">
      <Icon
        className={`h-7 text-center sm:h-5 md:h-7 mx-auto group-hover:text-indigo-500 ${
          active ? 'text-indigo-500' : 'text-gray-500'
        }`}
      />
    </div>
  )
}

export default HeaderIcon
