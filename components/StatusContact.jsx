import Tippy from '@tippyjs/react'
import { memo } from 'react'

function StatusContact({ shortName, initialName, username }) {
  const bgColors = [
    'bg-blue-100',
    'bg-pink-100',
    'bg-yellow-100',
    'bg-red-100',
    'bg-purple-100',
    'bg-indigo-100',
    'bg-green-100',
    'bg-fuchsia-100',
  ]

  return (
    <Tippy content={!shortName ? username : shortName}>
      <button className="flex item-center justify-center">
        <div
          className={`relative rounded-full flex items-center justify-center h-12 w-12 ${
            bgColors[Math.floor(Math.random() * (bgColors.length - 1))]
          } font-bold text-gray-800 hover:shadow-sm transition-shadow`}
        >
          <span className="h-3 w-3 rounded-full border-2 border-gray-50 dark:border-gray-900 absolute bottom-0 right-1 bg-green-500" />
          {!shortName
            ? username.split(' ').length > 2
              ? username.split(' ')[0] + ' ' + username.split(' ')[1]
              : username[0]
            : initialName}
        </div>
      </button>
    </Tippy>
  )
}

export default memo(StatusContact)
