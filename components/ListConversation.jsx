import { memo, useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import Image from 'next/image'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
  conversationListState,
  currentUserLogin,
  pagePaginate,
  showConversationState,
  userListState,
} from '../atoms'
import { decipher, diffDate, myDbQuery } from '../utils'
import { endPointGetAllPaginate } from '../utils/api.config'
import Toast from './Toast'
import { DEFAULT_AVATAR } from '../utils/const'

function ListConversation({ token }) {
  const users = useRecoilValue(userListState)
  const currentUser = useRecoilValue(currentUserLogin)
  const setShowConversation = useSetRecoilState(showConversationState)
  const [loading, setLoading] = useState(false)
  const [meta, setMeta] = useState({})
  const [{ conversationsPage }, setPage] = useRecoilState(pagePaginate)
  const [conversations, setConversations] = useRecoilState(
    conversationListState
  )

  useEffect(() => {
    setLoading(true)
    myDbQuery(endPointGetAllPaginate('conversations', conversationsPage), token)
      .then(
        /**
         * @param {{meta: MetaPagination, data: Conversations[], errors?: any[], error?: string}} conversations
         */
        (conversations) => {
          setLoading(false)
          if (conversations.errors || conversations.error)
            throw new Error(conversations?.error || 'Error Occured !')

          setConversations(conversations.data)
          setMeta(conversations.meta)
        }
      )
      .catch((err) => {
        setLoading(false)
        console.log(err)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      })
  }, [conversationsPage, setConversations, token])

  const loadMoreConversations = useCallback(() => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
      document.documentElement.offsetHeight
    )
      return

    meta.next_page_url &&
      setPage((s) => ({ ...s, conversationsPage: s.conversationsPage + 1 }))
  }, [meta.next_page_url, setPage])

  useEffect(() => {
    window.addEventListener('scroll', loadMoreConversations)

    return () => window.removeEventListener('scroll', loadMoreConversations)
  }, [loadMoreConversations])

  const getPhoto = useCallback(
    (interlocutor) => {
      const user = users.filter(
        (user) => user.accounts[0].id === interlocutor
      )[0]
      return user?.accounts[0].photo
    },
    [users]
  )

  const getUsername = useCallback(
    (interlocutor) => {
      const user = users.filter(
        (user) => user?.accounts[0]?.id === interlocutor
      )[0]
      return user?.username.split(' ').length > 2
        ? user.username.split(' ')[0] + ' ' + user.username.split(' ')[1]
        : user?.username
    },
    [users]
  )
  return (
    <div
      id="conversations"
      className={`flex flex-col overflow-y-auto ${
        conversations.length > 0 ? 'max-h-[364px]' : 'h-full'
      }`}
    >
      {conversations.length > 0 ? (
        conversations.map((conversation) => {
          const interlocutorId =
            conversation.account_inter === currentUser.accounts[0]?.id
              ? conversation.account_init
              : conversation.account_inter
          return (
            <button
              type="button"
              key={conversation.id}
              onClick={() => setShowConversation(conversation)}
              className="flex items-center justify-between py-2 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:bg-opacity-40 transition-colors px-6 border-b border-gray-200 dark:border-gray-800 cursor-pointer"
            >
              <div className="flex items-center space-x-3">
                <div className="relative w-12 h-12 rounded-full">
                  <Image
                    src={getPhoto(interlocutorId) || DEFAULT_AVATAR}
                    layout="fill"
                    className="rounded-full object-cover"
                    alt={`pic_account_${interlocutorId}`}
                  />
                </div>
                <div className="flex flex-col space-y-0.5 justify-center max-w-[190px]">
                  <h2 className="text-sm font-bold text-gray-700 dark:text-white">
                    {getUsername(interlocutorId)}
                  </h2>
                  <p className="text-gray-500 text-xs truncate text-left">
                    {conversation?.messages &&
                    conversation?.messages[0]?.content
                      ? decipher()(conversation?.messages[0]?.content)
                      : 'Aucun message.'}
                  </p>
                </div>
              </div>
              <div className="flex flex-col items-end">
                <span className="text-[10px] font-medium text-gray-500">
                  {diffDate(conversation?.created_at)}
                </span>
                {/* <span className="w-5 h-5 flex justify-center items-center rounded-full text-xs text-indigo-500 bg-indigo-50 shadow">
                4
              </span> */}
              </div>
            </button>
          )
        })
      ) : (
        <div className="flex flex-col space-y-5 my-10 flex-1 items-center justify-center">
          <p className="text-gray-500 text-sm">Your conversation is empty !</p>
        </div>
      )}
    </div>
  )
}

export default memo(ListConversation)
