//@ts-check
import Image from 'next/image'
import { CheckIcon, XIcon } from '@heroicons/react/outline'
import { memo, useCallback, useState } from 'react'
import TextareaAutosize from 'react-textarea-autosize'
import { toast } from 'react-toastify'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { currentUserLogin, postListState } from '../atoms'
import { myDbQuery } from '../utils'
import { endPointPut } from '../utils/api.config'
import Spinner from './Spinner'
import Toast from './Toast'
import { DEFAULT_AVATAR } from '../utils/const'

//@ts-check

/**
 * EDIT COMMENT
 * @param {{comment: Comments, token: string ,closeOnDone: () => void}} props
 * @returns {JSX.Element}
 */
function EditComment({ comment, token, closeOnDone }) {
  const user = useRecoilValue(currentUserLogin)
  const [content, setContent] = useState(comment.content)
  const [loading, setLoading] = useState(false)
  const setCommentListByPost = useSetRecoilState(postListState)
  const isChange = content.trim() !== comment.content

  const submitComment = useCallback(
    async (e) => {
      if (
        (e.charCode !== 13 && e.code !== 'Enter' && e.type !== 'click') ||
        e.shiftKey
      )
        return
      if (!content.trim()) return
      if (!isChange) return closeOnDone()
      const data = {
        postId: comment.post_id,
        content,
      }
      try {
        setLoading(true)
        const c = await myDbQuery(
          endPointPut('comments', comment.id),
          token,
          data,
          'PUT'
        )
        setLoading(false)
        if (c.errors || c.error)
          throw new Error(c.error || JSON.stringify(c.errors))

        closeOnDone()
        setCommentListByPost((posts) =>
          posts.map((post) => {
            if (post.id !== comment.post_id) return post
            return {
              ...post,
              comments: post.comments.map((cmt) => {
                if (cmt.id !== comment.id) return cmt
                return {
                  ...cmt,
                  content,
                }
              }),
            }
          })
        )
        toast(
          <Toast
            title="System"
            message="Le commentaire a bien été mis à jour."
            type="success"
          />,
          {
            autoClose: 5000,
            closeButton: false,
          }
        )
      } catch (error) {
        setLoading(false)
        console.log(error)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      }
    },
    [
      closeOnDone,
      comment.id,
      comment.post_id,
      content,
      isChange,
      setCommentListByPost,
      token,
    ]
  )

  const handleContent = useCallback((e) => setContent(e.target.value), [])
  return (
    <div className="flex items-end space-x-1 m-2 sticky bottom-0">
      {loading ? (
        <Spinner />
      ) : (
        <Image
          src={user.accounts[0].photo || DEFAULT_AVATAR}
          width="35"
          height="35"
          className="rounded-full flex-shrink object-cover"
          alt={user.username}
        />
      )}
      <TextareaAutosize
        disabled={loading}
        autoFocus
        required
        rows={1}
        value={content}
        onChange={handleContent}
        onKeyPress={submitComment}
        placeholder="Reply here..."
        className={`border-none flex-grow w-full ${
          loading ? 'bg-gray-100 cursor-not-allowed' : 'bg-white'
        } shadow dark:bg-gray-800 px-3 py-1 text-sm focus:ring-0 focus:border-b-1 dark:text-white focus:border-indigo-500 rounded-md`}
      />
      <div className="flex items-center space-x-1">
        <button
          title="Update"
          type="button"
          onClick={submitComment}
          disabled={!isChange || content.trim().length <= 0}
          className={`text-xs whitespace-nowrap rounded-full p-1  ${
            content.trim().length <= 0 || !isChange
              ? 'bg-gray-300 dark:bg-gray-500 dark:text-gray-700 cursor-not-allowed'
              : 'bg-indigo-500 hover:bg-indigo-600'
          } text-white  transition-colors shadow-sm`}
        >
          <CheckIcon className="h-5" />
        </button>
        <button
          title="Cancel"
          type="button"
          className="text-xs whitespace-nowrap rounded-full p-1 bg-red-100 hover:bg-red-200 dark:bg-red-900 dark:hover:bg-red-800 text-red-500 transition-colors shadow-sm"
          onClick={closeOnDone}
        >
          <XIcon className="h-5" />
        </button>
      </div>
    </div>
  )
}

export default memo(EditComment)
