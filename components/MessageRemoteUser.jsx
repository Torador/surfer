import { format } from 'date-fns'
import { decipher } from '../utils'

/**
 * MESSAGE COMING FROM USER REMOTE
 * @param {{message: Message}} props
 * @returns {JSX.Element}
 */
function MessageRemoteUser({ message }) {
  return (
    <div className="flex flex-col space-y-1 max-w-[400px] w-max">
      <h1 className="text-gray-500 font-bold text-xs">
        {format(new Date(message.created_at), 'HH:mm')}
      </h1>
      <div className="rounded-xl shadow text-sm rounded-tl-none bg-gray-700 bg-opacity-80 p-3 text-white line-clamp-6 hover:line-clamp-none">
        {decipher()(message.content)}
      </div>
    </div>
  )
}

export default MessageRemoteUser
