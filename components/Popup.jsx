import { Popover, Transition } from '@headlessui/react'
import { Fragment } from 'react'

function Popup({ trigger, classNameBtn, children, classSize = 'max-w-xs' }) {
  return (
    <Popover className="relative">
      {({ open }) => (
        <>
          <Popover.Button
            className={`
                ${open ? '' : 'text-opacity-90'} ${classNameBtn}`}
          >
            {trigger}
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel
              className={`absolute z-10 w-screen px-4 mt-3 transform -translate-x-56 left-1/2 sm:px-0 border border-gray-100 dark:border-gray-800 rounded-sm ${classSize}`}
            >
              {children}
            </Popover.Panel>
          </Transition>
        </>
      )}
    </Popover>
  )
}

export default Popup
