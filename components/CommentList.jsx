import { Transition } from '@headlessui/react'
import CreateComment from './CreateComment'
import Comment from './Comment'
import { memo, useCallback, useEffect, useState } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import { currentUserLogin, postListState } from '../atoms'
import { myDbQuery } from '../utils'
import { endPointGetBySlug } from '../utils/api.config'
import Spinner from './Spinner'
import Toast from './Toast'
import { toast } from 'react-toastify'
import { ChevronDownIcon } from '@heroicons/react/solid'

/**
 *  COMMENT LIST COMPONENT
 * @param {{comments: Comments[], isShow: boolean, photoUser: string, isCommentlock: boolean, postId: number, token: string}} props
 * @returns {JSX.Element}
 */
function CommentList({
  comments,
  isShow,
  photoUser,
  isCommentlock,
  postId,
  token,
}) {
  const [limit, setLimit] = useState(10)
  const setCommentListByPost = useSetRecoilState(postListState)
  const user = useRecoilValue(currentUserLogin)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    if (!isShow) return
    setLoading(true)
    myDbQuery(endPointGetBySlug('comments', postId), token, null, 'GET', 'json')
      .then((comments) => {
        setLoading(false)
        if (comments.errors || comments.error)
          throw new Error(comments.error || JSON.stringify(comments.errors))

        setCommentListByPost((posts) =>
          posts.map((post) => {
            if (post.id !== postId) return post
            return {
              ...post,
              comments: [...comments],
            }
          })
        )
      })
      .catch((err) => {
        setLoading(false)
        console.log(err)
        toast(
          <Toast
            title="System"
            message={err.message || 'Error Occured !'}
            type="error"
          />,
          {
            autoClose: 5000,
            closeButton: false,
          }
        )
      })
  }, [isShow, postId, setCommentListByPost, token])

  const moreTenComments = useCallback(() => setLimit((l) => l + 10), [])

  return (
    <div className="bg-gray-50 dark:bg-gray-900 rounded-b-md shadow">
      <CreateComment
        postId={postId}
        token={token}
        isCommentlock={isCommentlock}
        photoUser={photoUser}
        isShow={isShow}
      />
      <Transition
        show={isShow}
        as="div"
        enter="transform transition duration-150"
        enterFrom="opacity-0 translate-y-0"
        enterTo="opacity-1 translate-y-1"
        leave="transform transition duration-150"
        leaveFrom="opacity-1 translate-y-1"
        leaveTo="opacity-0 translate-y-0"
        className="flex flex-col"
      >
        {comments.length > 0 && (
          <>
            <div className="flex flex-col px-2 space-y-5 border-t border-gray-200 dark:border-gray-700 py-4">
              {comments.map((comment, i) => {
                if (i < limit) {
                  return (
                    <Comment
                      key={comment.id}
                      postId={postId}
                      comment={comment}
                      user={user}
                      token={token}
                      isCommentlock={isCommentlock}
                    />
                  )
                }
              })}
              {loading && (
                <div className="flex w-full justify-center my-1">
                  <Spinner />
                </div>
              )}
              {comments.length > limit && (
                <button
                  onClick={moreTenComments}
                  type="button"
                  className="group rounded-full flex items-center justify-center w-full border border-gray-200 space-x-1 text-sm font-semibold text-gray-500 py-1 hover:bg-gray-100 transition-colors"
                >
                  <span>View more comments</span>
                  <ChevronDownIcon className="h-5 transform group-hover:translate-y-0.5 transition-transform" />
                </button>
              )}
            </div>
          </>
        )}
      </Transition>
    </div>
  )
}

export default memo(CommentList)
