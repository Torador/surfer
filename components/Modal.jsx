//@ts-check
import { Dialog, Transition } from '@headlessui/react'
import { Fragment, memo } from 'react'
import { XCircleIcon } from '@heroicons/react/outline'

// @ts-check

/**
 * @typedef {'0' | '5' | '10' | '20' | '25' | '30' | '40 '| '50' | '60' | '70' | '75' | '80' | '90' | '95' | '100'} Opacity
 */

/**
 *  MODAL COMPONENT
 * @param {{trigger?: JSX.Element, title: string | JSX.Element, className?: string, colorTitle?: string, children: JSX.Element, isOpen: boolean, opacity?: Opacity, onClose: () => void }} props
 * @returns {JSX.Element}
 * @author Jordan Kagmeni (Torador)
 */
function Modal({
  trigger = null,
  title,
  className = 'w-full max-w-md',
  colorTitle = 'indigo',
  children,
  isOpen,
  opacity = '90',
  onClose,
}) {
  return (
    <>
      {trigger}
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-40 overflow-y-auto"
          onClose={() => {}}
        >
          <div className="min-h-screen px-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 backdrop-filter backdrop-blur-md bg-gray-800 bg-opacity-40 dark:bg-gray-700 dark:bg-opacity-90 cursor-not-allowed" />
            </Transition.Child>

            {/* This element is to trick the browser into centering the modal contents. */}
            <span
              className="inline-block h-screen align-middle"
              aria-hidden="true"
            >
              &#8203;
            </span>
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300 transform"
              enterFrom="opacity-0 scale-75"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200 transform"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-125"
            >
              <div className={`s_modal bg-opacity-${opacity} ${className}`}>
                <div className="flex justify-between items-center">
                  <Dialog.Title
                    as="h3"
                    className={`text-2xl text-${colorTitle}-700 font-bold leading-6 dark:text-white w-3/4 truncate py-4`}
                  >
                    {title}
                  </Dialog.Title>
                  <button onClick={onClose} className="s_btn_close">
                    <XCircleIcon className="h-5" />
                  </button>
                </div>

                <div className="mt-6">{children}</div>
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
    </>
  )
}

export default memo(Modal)
