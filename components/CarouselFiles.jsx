/* eslint-disable @next/next/no-img-element */
import Image from 'next/image'
import { Dialog, Transition } from '@headlessui/react'
import { EyeIcon, XCircleIcon } from '@heroicons/react/outline'
import { Carousel } from 'react-responsive-carousel'
import { Fragment, useCallback, useState } from 'react'
//@ts-check

/**
 * CAROUSEL FILES COMPONENT
 * @param {{files: FilesPost[]}} props
 * @returns {JSX.Element}
 */
function CarouselFiles({ files }) {
  const [isPreview, setPreview] = useState(false)
  const handlePreview = useCallback(() => {
    setPreview((s) => !s)
  }, [])
  return (
    <div className="relative">
      <PreviewButton handlePreview={handlePreview} />
      <Transition appear show={isPreview} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 z-40 overflow-y-auto"
          onClose={handlePreview}
        >
          <div className="min-h-screen select-none cursor-pointer">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0"
              enterTo="opacity-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
            >
              <Dialog.Overlay className="fixed inset-0 backdrop-filter backdrop-blur-sm bg-gray-800 bg-opacity-80" />
            </Transition.Child>

            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300 transform"
              enterFrom="opacity-0 scale-75"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200 transform"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-50"
            >
              <div className="relative">
                <button
                  onClick={handlePreview}
                  className="s_btn_close absolute -top-2 z-10"
                >
                  <XCircleIcon className="h-10" />
                </button>

                {files.length > 1 ? (
                  <Carousel
                    emulateTouch
                    useKeyboardArrows
                    swipeable
                    showArrows
                    showIndicators={false}
                  >
                    {files.map((file) => (
                      <img
                        key={file.id}
                        src={file?.url_img.replace(
                          'http://0.0.0.0:3333',
                          process.env.NEXT_PUBLIC_HOST_SERVER
                        )}
                        loading="lazy"
                        className="rounded-sm transform scale-75"
                        alt={file.filename}
                      />
                    ))}
                  </Carousel>
                ) : (
                  <div className="flex justify-center">
                    <img
                      src={
                        files[0]?.url_img.replace(
                          'http://0.0.0.0:3333',
                          process.env.NEXT_PUBLIC_HOST_SERVER
                        ) || '/logo.png'
                      }
                      alt={files[0]?.filename}
                      className="rounded-sm transform scale-50"
                    />
                  </div>
                )}
              </div>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition>
      {files.length > 1 && (
        <Carousel
          emulateTouch
          showIndicators
          useKeyboardArrows
          swipeable
          stopOnHover
          showArrows={false}
        >
          {files.map((file) => (
            <div
              className="aspect-w-6 aspect-h-7 rounded-sm shadow-md flex justify-center items-end select-none hover:cursor-pointer"
              key={file.id}
            >
              <Image
                src={files[0]?.url_img.replace(
                  'http://0.0.0.0:3333',
                  process.env.NEXT_PUBLIC_HOST_SERVER
                )}
                layout="fill"
                className="rounded-sm object-cover"
                alt={file.filename}
              />
            </div>
          ))}
        </Carousel>
      )}
      {files.length === 1 && (
        <div className="relative w-full min-h-[600px] md:min-h-[670px] rounded-sm shadow-md flex justify-center items-end overflow-hidden">
          <PreviewButton handlePreview={handlePreview} />
          <Image
            src={
              files[0]?.url_img.replace(
                'http://0.0.0.0:3333',
                process.env.NEXT_PUBLIC_HOST_SERVER
              ) || '/logo.png'
            }
            layout="fill"
            alt={files[0]?.filename}
            className="rounded-sm object-cover"
          />
        </div>
      )}
    </div>
  )
}

/**
 * PREVIEW BUTTON
 * @param {{handlePreview: () => void}} props
 * @returns {JSX.Element}
 */
const PreviewButton = ({ handlePreview }) => (
  <button
    onClick={handlePreview}
    className="absolute top-2 left-2 z-10 text-gray-500 transform hover:text-indigo-500 transition-colors"
  >
    <EyeIcon className="h-5 filter drop-shadow-2xl" />
  </button>
)
export default CarouselFiles
