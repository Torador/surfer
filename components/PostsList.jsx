import React, { memo, useCallback, useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { useRecoilState, useRecoilValue, useSetRecoilState } from 'recoil'
import {
  currentUserLogin,
  isOpenDialogPostState,
  pagePaginate,
  postListState,
} from '../atoms'
import { getPostsByTopicSelected } from '../selectors'
import { myDbQuery } from '../utils'
import { endPointGetAllPaginate } from '../utils/api.config'
import EmptyPosts from './EmptyPosts'
import Loader from './Loader'
import Post from './Post'
import Spinner from './Spinner'
import Toast from './Toast'

//@ts-check

/**
 *  POST LIST COMPONENT
 * @param {{token: string}} props
 * @returns {JSX.Element}
 */
function PostsList({ token }) {
  const [loading, setLoading] = useState(false)
  /**
   * @type {[MetaPagination, import('react').Dispatch<import('react').SetStateAction<MetaPagination[]>>]}
   */
  const [meta, setMeta] = useState({})
  const setOpenModal = useSetRecoilState(isOpenDialogPostState)
  const setPosts = useSetRecoilState(postListState)
  const posts = useRecoilValue(getPostsByTopicSelected)
  const [{ PostsPage }, setPage] = useRecoilState(pagePaginate)

  /**
   * @type {User}
   */
  const currentUserInfo = useRecoilValue(currentUserLogin)

  useEffect(() => {
    setLoading(true)
    myDbQuery(endPointGetAllPaginate('posts', PostsPage), token)
      .then(
        /**
         * @param {{meta: MetaPagination, data: Post[], errors?: any[], error?: string}} posts
         */
        (posts) => {
          setLoading(false)
          if (posts.errors || posts.error)
            throw new Error(posts?.error || 'Error Occured !')

          setPosts(posts.data)
          setMeta(posts.meta)
        }
      )
      .catch((err) => {
        setLoading(false)
        console.log(err)
        toast(<Toast title="System" message="Error Occured !" type="error" />, {
          autoClose: 5000,
          closeButton: false,
        })
      })
  }, [PostsPage, setPosts, token])

  const loadMorePosts = useCallback(() => {
    if (
      window.innerHeight + document.documentElement.scrollTop !==
      document.documentElement.offsetHeight
    )
      return

    meta.next_page_url && setPage((s) => ({ ...s, PostsPage: s.PostsPage + 1 }))
  }, [meta.next_page_url, setPage])

  useEffect(() => {
    window.addEventListener('scroll', loadMorePosts)

    return () => window.removeEventListener('scroll', loadMorePosts)
  }, [loadMorePosts])

  const openModalPost = useCallback(() => setOpenModal(true), [setOpenModal])

  if (posts.length <= 0) {
    return (
      <div className="flex justify-center mx-auto md:mx-0 items-center max-w-[300px] md:min-w-[600px] bg-white dark:bg-gray-900 rounded-md md:shadow-sm relative">
        <button
          onClick={openModalPost}
          className={
            !currentUserInfo?.accounts[0]?.shortName
              ? 'hidden'
              : 'absolute inset-0 backdrop-filter backdrop-blur-sm rounded-md p-5 text-2xl text-indigo-500 text-center font-bold hover:scale-105 active:scale-105 focus:scale-105 transition-transform w-full'
          }
        >
          Create your new post now 🚀 !
        </button>
        <EmptyPosts />
      </div>
    )
  }
  return (
    <div className="flex flex-col space-y-4 max-w-[600px] lg:min-w-[600px] items-center pb-10 z-10">
      {!posts && loading ? (
        <Loader color="text-indigo-500 fill-current" />
      ) : (
        posts?.map((post) => (
          <Post
            key={post.id}
            post={post}
            user={currentUserInfo}
            token={token}
          />
        ))
      )}
      {loading && (
        <div className="flex items-center justify-center">
          <Spinner double />
        </div>
      )}
    </div>
  )
}

export default memo(PostsList)
