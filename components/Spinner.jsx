/**
 *  SPINNER COMPONENT
 * @param {{size?: string | number, color?: string, double?: boolean}} props
 * @returns {JSX.Element}
 */
function Spinner({ size = '5', color, double = false }) {
  return (
    <div className="flex justify-center items-center">
      <div
        className={`animate-spin rounded-full h-${size} w-${size} ${
          double ? 'border-t-2' : ''
        } border-b-2 ${color ? `border-${color}-500` : 'border-indigo-500'}`}
      ></div>
    </div>
  )
}

export default Spinner
