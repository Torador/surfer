import { Fragment, memo } from 'react'
import { Menu, Transition } from '@headlessui/react'

/**
 * DROPDOWN COMPONENT
 * @param {{trigger: JSX.Element, options: OptionDropdown}} props
 * @returns {JSX.Element}
 */
function Dropdown({ trigger, options }) {
  return (
    <Menu as="div" className="relative z-30">
      <div>
        <Menu.Button>{trigger}</Menu.Button>
      </div>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 w-56 origin-top-right bg-white divide-y dark:bg-black dark:bg-opacity-80 filter dark:backdrop-blur-md divide-gray-100 dark:divide-gray-900 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
          {options.map((items, i) => {
            return (
              <div className="px-1 py-1" key={i}>
                {items.map((item, i) => (
                  <Menu.Item key={i}>
                    {({ active }) => (
                      <button
                        onClick={item.action}
                        className={`${
                          active
                            ? `bg-${item.color || 'indigo'}-50 dark:bg-${
                                item.color || 'indigo'
                              }-900 dark:bg-opacity-40 text-${
                                item.color || 'indigo'
                              }-500 dark:text-${item.color || 'indigo'}-500`
                            : 'text-gray-900 dark:text-gray-500'
                        } group flex rounded-md items-center w-full px-2 py-2 text-sm`}
                      >
                        {active ? item.activeIcon || item.icon : item.icon}
                        {item.label}
                      </button>
                    )}
                  </Menu.Item>
                ))}
              </div>
            )
          })}
        </Menu.Items>
      </Transition>
    </Menu>
  )
}

export default memo(Dropdown)
