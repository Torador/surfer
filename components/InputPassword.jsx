import { useCallback, useRef, useState } from 'react'
import { EyeIcon, EyeOffIcon } from '@heroicons/react/outline'

/**
 * INPUT PASSWORD ELEMENT
 * @param {{placeholder: string, name?: string, id?: string}} props
 * @returns {JSX.Element}
 * @author Jordan Kagmeni (Torador)
 */
function InputPassword({
  placeholder = 'Entrez votre mot de passe',
  name = 'password',
  id = 'password',
}) {
  const inputPwd = useRef(null)
  const [eyeOff, setEyeOff] = useState(false)

  const _toggleViewPwd = useCallback((e) => {
    e.stopPropagation()

    const attr = inputPwd.current.getAttribute('type')
    attr == 'password' && inputPwd.current.setAttribute('type', 'text')
    attr == 'text' && inputPwd.current.setAttribute('type', 'password')
    setEyeOff((s) => !s)
  }, [])

  return (
    <div className="relative w-full">
      <input
        ref={inputPwd}
        type="password"
        name={name}
        id={id}
        className="transition duration-300 focus:outline-none px-2 py-3 border border-indigo-100 dark:border-gray-800 rounded-lg shadow-inner focus:border-surfer dark:focus:border-surfer placeholder-gray-400 dark:placeholder-gray-700 bg-indigo-50 dark:text-white dark:bg-gray-900 text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 w-full"
        placeholder={placeholder}
        required
      />
      <button
        type="button"
        title={eyeOff ? 'Masquer le contenu' : 'Voir le contenu'}
        onClick={_toggleViewPwd}
        className="absolute right-2 top-1/2 transform -translate-x-2 -translate-y-1/2 focus-visible:ring-2 focus-visible:ring-surfer focus-visible:ring-opacity-75 focus:outline-none rounded-md text-gray-400 dark:text-gray-500 hover:text-surfer dark:hover:text-surfer transition duration-300"
      >
        {eyeOff ? <EyeIcon className="h-5" /> : <EyeOffIcon className="h-5" />}
      </button>
    </div>
  )
}

export default InputPassword
