import { CameraIcon, PhotographIcon } from '@heroicons/react/outline'
import Loader from './Loader'

/**
 * FORM SIGN UP COMPONENT
 * @param {{ handleSubmit: () => void, isLoading: boolean }} props
 * @returns {JSX.Element}
 */

function CreatePost({ handleSubmit, isLoading }) {
  return (
    <div className="flex relative px-48 mr-10 pr-44 my-48 items-center justify-center">
      <form onSubmit={handleSubmit} className="flex flex-row space-y-6">
        <div className="w-full flex flex-col justify-center items-center border-4 rounded-lg px-10">
          <div className="bg-red-500 w-20 h-6 justify-center items-center mr-4">
            <CameraIcon className="justify-center mx-6" />
          </div>
          <div className="items-start justify-start mx-1 bg-yellow-500">
            <PhotographIcon className="justify-items-center mx-6" />
          </div>

          <input
            className="border-2 rounded-none text-white bg-indigo-500 hover:bg-indigo-600 cursor-pointer"
            type="file"
            placeholder="Drag your picture and videos here"
            accept="image/*, video/*"
          />

          <button
            type="submit"
            className="transform bg-indigo-500 hover:-translate-y-1 hover:shadow-md hover:bg-indigo-600 py-3 px-2 text-white rounded-md transition duration-300 flex justify-center font-semibold w-full shadow-2xl"
          >
            {isLoading ? <Loader /> : 'Create new post'}
          </button>
        </div>
      </form>
    </div>
  )
}

export default CreatePost
