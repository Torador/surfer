import { PlusIcon } from '@heroicons/react/outline'
import { useCallback, useState } from 'react'
import { useRecoilState, useSetRecoilState } from 'recoil'
import { modalTopic, topicListState } from '../atoms'
import TextareaAutosize from 'react-textarea-autosize'
import Input from './Input'
import Modal from './Modal'
import Spinner from './Spinner'
import { myDbQuery } from '../utils'
import { endPointPost } from '../utils/api.config'
import { toast } from 'react-toastify'
import Toast from './Toast'

function CreateTopic({ token }) {
  const [show, setShow] = useRecoilState(modalTopic)
  const [loading, setLoading] = useState(false)
  const setTopicList = useSetRecoilState(topicListState)

  const toggleShow = useCallback(() => setShow((s) => !s), [setShow])

  const handleSaveTopic = useCallback(
    async (e) => {
      e.preventDefault()
      e.stopPropagation()
      if (loading) return
      const data = {
        label: e.target[0].value.trim(),
        description: e.target[1].value.trim(),
      }
      if (!data.label || !data.description)
        return toast(
          <Toast
            title="Système"
            message="Veuillez remplir tous les champs SVP."
            type="error"
          />
        )
      try {
        setLoading(true)
        const topic = await myDbQuery(
          endPointPost('topics'),
          token,
          data,
          'POST'
        )
        setLoading(false)
        if (topic.errors || topic.error)
          throw new Error(topic.error || 'ERROR OCCURED !')
        setTopicList((t) => [topic, ...t])
        toggleShow()

        toast(
          <Toast
            title="Système"
            message={`Le topic ${topic.label} a bien été crée.`}
            type="success"
          />
        )
      } catch (error) {
        toast(<Toast title="Système" message={error.message} type="error" />, {
          closeButton: false,
          autoClose: 5000,
        })
      }
    },
    [loading, setTopicList, toggleShow, token]
  )

  return (
    <Modal
      isOpen={show}
      onClose={toggleShow}
      title="New Topic"
      colorTitle="gray"
      trigger={
        <button
          type="button"
          onClick={toggleShow}
          className="group flex items-center transform active:scale-95 space-x-1 w-11/12 mx-auto text-gray-600 dark:text-gray-500 text-sm dark:hover:text-indigo-500 bg-white dark:bg-gray-800 hover:bg-indigo-50 dark:hover:bg-indigo-900 dark:hover:bg-opacity-25 hover:text-indigo-500 hover:shadow-sm shadow px-3 py-2 rounded-full transition duration-300 mb-2"
        >
          <span className="flex items-center justify-center p-1 rounded-full">
            <PlusIcon className="h-2 group-hover:text-indigo-500" />
          </span>
          <span>Create Topic</span>
        </button>
      }
    >
      <form className="flex flex-col space-y-4" onSubmit={handleSaveTopic}>
        <Input required placeholder="Enter label of topic" />

        <TextareaAutosize
          required
          placeholder="Enter description"
          className="transition duration-300 focus:outline-none px-2 py-3 border border-indigo-100 dark:border-gray-800 rounded-lg shadow-inner focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 bg-indigo-50 dark:bg-gray-900 text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75 w-full"
        />
        <button
          disabled={loading}
          type="submit"
          className={`${
            loading
              ? 'text-gray-500 bg-gray-200 dark:bg-gray-900 cursor-not-allowed'
              : 'text-white bg-indigo-500 dark:bg-indigo-600 hover:bg-indigo-600 dark:hover:bg-indigo-700 hover:shadow-xl'
          } shadow-sm transition duration-300 py-2 px-3 w-3/12 rounded-md flex items-center space-x-1 justify-center`}
        >
          {loading ? <Spinner double /> : 'Create'}
        </button>
      </form>
    </Modal>
  )
}

export default CreateTopic
