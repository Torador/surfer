import { selector } from 'recoil'
import {
  currentAccountId,
  currentAccountSlug,
  currentPostId,
  currentPostSlug,
  limitPage,
  pagePaginate,
  postListState,
  selectedTopic,
  tokenUser,
  topicListState,
} from '../atoms'
import { checkErrorAndRetry, myDbQuery } from '../utils'
import {
  endPointGetAll,
  endPointGetAllPaginate,
  endPointGetBySlug,
  endPointGetSingle,
} from '../utils/api.config'
import { RETRIES } from '../utils/const'

//@ts-check

export const getAccountsPerPage = selector({
  key: 'accountsPerPage',
  /**
   * @returns {Account[]}
   */
  get: async ({ get }) => {
    const { accountsPage: page } = get(pagePaginate)
    if (!page) return

    const token = get(tokenUser)
    if (!token) return

    const limit = get(limitPage)

    let retries = RETRIES

    while (retries) {
      const accounts = await myDbQuery(
        endPointGetAllPaginate('accounts', page, limit),
        token
      )
      if (accounts.errors || accounts.error) {
        checkErrorAndRetry(accounts, retries)
        return
      }
      break
    }

    return accounts
  },
})

export const getCurrentAccount = selector({
  key: 'currentAccountInfo',
  /**
   * @returns {Account}
   */
  get: async ({ get }) => {
    const currentId = get(currentAccountId)
    if (!currentId) return null

    const token = get(tokenUser)
    if (!token) return null

    let retries = RETRIES

    while (retries) {
      const currentAccInfo = await myDbQuery(
        endPointGetSingle('accounts', 'me'),
        token
      )
      if (currentAccInfo.errors || currentAccInfo.error) {
        checkErrorAndRetry(currentAccInfo, retries)
        return
      }
      break
    }

    return currentAccInfo
  },
})

export const getAccountBySlug = selector({
  key: 'getAccountBySlug',
  /**
   * @returns {Account}
   */
  get: async ({ get }) => {
    const currentSlug = get(currentAccountSlug)
    if (currentSlug) return null

    const token = get(tokenUser)
    if (!token) return null

    let retries = RETRIES

    while (retries) {
      const account = await myDbQuery(
        endPointGetBySlug('accounts', currentSlug),
        token
      )
      if (account.errors || account.error) {
        checkErrorAndRetry(account, retries)
        return
      }
      break
    }
    return account
  },
})

export const getPostsPerPage = selector({
  key: 'getPostsPerPage',
  /**
   * @returns {Post[]}
   */
  get: async ({ get }) => {
    const { PostsPage: page } = get(pagePaginate)
    if (!page) return null

    const token = get(tokenUser)
    if (!token) return null

    const limit = get(limitPage)

    let retries = RETRIES

    while (retries) {
      const posts = await myDbQuery(
        endPointGetAllPaginate('posts', page, limit),
        token
      )
      if (posts.errors || posts.error) return checkErrorAndRetry(posts, retries)
      break
    }

    return posts
  },
})

export const getCurrentPost = selector({
  key: 'currentPost',
  /**
   * @returns {Post}
   */
  get: async ({ get }) => {
    const currentId = get(currentPostId)
    if (!currentId) return null

    const token = get(tokenUser)
    if (!token) return null

    let retries = RETRIES
    while (retries) {
      const post = await myDbQuery(endPointGetSingle('posts', currentId), token)
      if (post.errors || post.error) return checkErrorAndRetry(post, retries)
      break
    }

    return post
  },
})

export const getPostBySlug = selector({
  key: 'getPostBySlug',
  /**
   * @returns {Post}
   */
  get: async ({ get }) => {
    const slug = get(currentPostSlug)
    if (!slug) return null

    const token = get(tokenUser)
    if (!token) return null

    let retries = RETRIES
    while (retries) {
      const post = await myDbQuery(endPointGetBySlug('posts', slug), token)
      if (post.errors || post.error) return checkErrorAndRetry(post, retries)
      break
    }

    return post
  },
})

export const getCommentsPerPage = selector({
  key: 'commentsPerPage',
  /**
   * @returns {Comment[]}
   */
  get: async ({ get }) => {
    const { commentsPage: page } = get(pagePaginate)
    const token = get(tokenUser)
    if (!token) return null

    const limit = get(limitPage)

    const retries = RETRIES
    while (retries) {
      const comments = await myDbQuery(
        endPointGetAllPaginate('comments', page, limit)
      )

      if (comments.errors || comments.error)
        return checkErrorAndRetry(comments, retries)
      break
    }

    return comments
  },
})

export const getTopics = selector({
  key: 'getTopics',
  get: async ({ get }) => {
    const token = get(tokenUser)
    if (!token) return null

    const retries = RETRIES

    while (retries) {
      const topics = await myDbQuery(endPointGetAll('topics'), token)

      if (topics.errors || topics.error)
        return checkErrorAndRetry(topics, retries)

      return topics
    }
  },
  /**
   * @param {Topic[]} newValue
   * @returns {void}
   */
  set: async ({ set }, newValue) => set(topicListState, newValue),
})

export const getPostsByTopicSelected = selector({
  key: 'getPostsByTopicSelected',
  get: ({ get }) => {
    const topicSelected = get(selectedTopic)
    const posts = get(postListState)
    if (!topicSelected) return posts

    return posts.filter((post) => {
      for (const topic of post.topics) {
        if (topic.id === topicSelected.id) return post
      }
    })
  },
})
