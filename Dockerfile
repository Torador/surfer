# base image
FROM node:lts-alpine
# create & set working directory
WORKDIR /usr/app
COPY package*.json ./
# install dependencies
RUN npm install
# copy source files
COPY . .
# start app
RUN npm run build
ENV NODE_ENV=production
EXPOSE 3000
CMD ["npm", "start"]