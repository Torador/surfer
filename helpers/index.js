import { KEY_COOKIE, TOKEN_CHECK_URL } from '../utils/const'

/**
 *  CHECK TOKEN ON SERVER SIDE
 * @param {NextApiRequestCookies} cookies
 */
export async function checkTokenSS(cookies) {
  const isValidCookie = await fetch(TOKEN_CHECK_URL, {
    headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
  }).then((res) => res.text())
  const v = JSON.parse(isValidCookie)?.errors

  return v
}
