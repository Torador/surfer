import Enzyme, { mount, shallow } from 'enzyme'
import Adapter from '@wojtekmaj/enzyme-adapter-react-17'
import SignIn from '../../pages/index'
import Input from '../../components/Input'
import InputPassword from '../../components/InputPassword'
import FormSignIn from '../../components/FormSignIn'
import FormSignUp from '../../components/FormSignUp'

Enzyme.configure({ adapter: new Adapter() })
describe('Tests App Surfer', () => {
  it('validates model on button click on signIn form', () => {
    const handleSubmit = jest.fn()
    const wrapper = mount(
      <FormSignIn isLoading={false} handleSubmit={handleSubmit} />
    )
    wrapper.find('button[type="submit"]').simulate('submit')
    expect(handleSubmit).toHaveBeenCalled()
  })

  it('validates model on button click on register form', () => {
    const handleSubmit = jest.fn()
    const wrapper = mount(
      <FormSignUp isLoading={false} handleSubmit={handleSubmit} />
    )
    wrapper.find('button[type="submit"]').simulate('submit')
    expect(handleSubmit).toHaveBeenCalled()
  })

  it('renders without crashing', () => {
    const handleSubmit = jest.fn()
    const wrapper = shallow(<SignIn />)
    const form = shallow(
      <FormSignIn isLoading={false} handleSubmit={handleSubmit} />
    )
    const heroLeft = (
      <div className="m-8 relative space-y-8 w-[400px] -top-20">
        <h1 className="text-7xl font-extrabold text-gray-800 dark:text-white tracking-wide">
          <span className="filter dark:drop-shadow-md">Sign In to</span>{' '}
          <span className="text-indigo-500 italic filter dark:drop-shadow-md">
            Surfer
          </span>
        </h1>
        <p className="text-gray-700 dark:text-gray-100 text-2xl dark:text-3xl font-semibold filter drop-shadow-sm">
          The best platform to achieve your career goals.
        </p>
      </div>
    )

    const UserNameField = (
      <Input required type="email" placeholder="Enter email" />
    )
    const passwordFied = <InputPassword placeholder="Password" />
    const buttonSubmit = (
      <button
        disabled={false}
        type="submit"
        className="transform bg-indigo-500 hover:-translate-y-1 hover:shadow-md hover:bg-indigo-600 py-3 px-2 text-white rounded-md transition duration-300 flex justify-center font-semibold w-full shadow-2xl"
      >
        Sign In
      </button>
    )

    expect(wrapper.contains(heroLeft)).toEqual(true)
    //expect(wrapper.getElement('form')).isEqual(true)
    expect(form.contains(UserNameField)).toEqual(true)
    expect(form.contains(passwordFied)).toEqual(true)
    expect(form.contains(buttonSubmit)).toEqual(true)
  })
})
