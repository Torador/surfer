const colors = require('tailwindcss/colors')

module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {
      content: {
        close: '"x"',
      },
      colors: {
        surfer: '#5881F2',
        fuchsia: colors.fuchsia,
      },
      animation: {
        blob: 'blob 7s infinite',
        fade: '0.8s ease 0s normal forwards 1 fadein',
      },
      keyframes: {
        blob: {
          '0%': {
            transform: 'translate(0px, 0px) scale(1)',
          },
          '33%': {
            transform: 'translate(30px, -50px) scale(1.1)',
          },
          '66%': {
            transform: 'translate(-20px, 20px) scale(0.9)',
          },
          '100%': {
            transform: 'tranlate(0px, 0px) scale(1)',
          },
        },
        fadein: {
          '0%': {
            opacity: '0',
          },
          '66%': {
            opacity: '0',
            transform: 'translateX(-20px)',
          },
          '100%': {
            opacity: 1,
          },
        },
      },
    },
  },
  variants: {
    extend: {
      textColor: [
        'responsive',
        'hover',
        'focus',
        'before',
        'after',
        'hover::before',
        'hover::after',
        'focus::before',
        'checked:hover',
        'checked:hover::before',
      ],
    },
  },
  plugins: [
    require('tailwind-scrollbar-hide'),
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
