module.exports = {
  reactStrictMode: true,
  experimental: {
    concurrentFeatures: true,
    serverComponents: true,
  },
  api: {
    domains: ['http://localhost:8080'],
  },
  images: {
    domains: [
      process.env.NEXT_PUBLIC_HOST_SERVER?.replace(':3333', '')?.replace(
        'http://',
        ''
      ) || '127.0.0.1',
      'lh3.googleusercontent.com',
      '0.0.0.0',
      'img.icons8.com',
    ],
  },
}
