import { atom, RecoilState } from 'recoil'

//@ts-check

export const modalTopic = atom({
  key: 'modalTopic',
  default: false,
})

export const isOpenDialogPostState = atom({
  key: 'isOpenDialogState',
  default: false,
})

export const tokenUser = atom({
  key: 'tokenUser',
  default: '',
})

export const pagePaginate = atom({
  key: 'pagePaginate',
  default: {
    accountsPage: 1,
    PostsPage: 1,
    commentsPage: 1,
    conversationsPage: 1,
    messagesPage: 1,
  },
})

export const limitPage = atom({
  key: 'limitPage',
  default: 20,
})

/**
 * @type {RecoilState<Account[]>}
 */
export const accountListState = atom({
  key: 'accountListState',
  default: [],
})

/**
 * @type {RecoilState<Message[]>}
 */
export const messageListState = atom({
  key: 'messageListState',
  default: [],
})

export const globalSocket = atom({
  key: 'globalSocket',
  default: {},
})

/**
 * @type {RecoilState<Topic | null>}
 */
export const selectedTopic = atom({
  key: 'selectedTopic',
  default: null,
})

/**
 * @type {RecoilState<Account[]>}
 */
export const suggestionListState = atom({
  key: 'suggestionListState',
  default: [],
})

export const conversationListState = atom({
  key: 'conversationListState',
  default: [],
})

export const showConversationState = atom({
  key: 'showConversationState',
  default: null,
})

export const isOpenModalConversation = atom({
  key: 'isOpenModalConversation',
  default: false,
})

/**
 * @type {RecoilState<User[]>}
 */
export const userListState = atom({
  key: 'userListState',
  default: [],
})

/**
 * @type {RecoilState<IsFollow[]>}
 */
export const isFollowList = atom({
  key: 'isFollowList',
  default: [],
})

/**
 * @type {RecoilState<User>}
 */
export const currentUserLogin = atom({
  key: 'currentUserLogin',
  default: null,
})

/**
 * @type {RecoilState<Post[]>}
 */
export const postListState = atom({
  key: 'postListState',
  default: [],
})

/**
 * @type {RecoilState<Comment[]>}
 */
export const commentListState = atom({
  key: 'commentListState',
  default: [],
})

/**
 * @type {Topic[]}
 */
export const topicListState = atom({
  key: 'topicListState',
  default: [],
})

export const currentAccountId = atom({
  key: 'currentAccountId',
  default: 0,
})

export const currentAccountSlug = atom({
  key: 'currentAccountSlug',
  default: '',
})

export const currentPostSlug = atom({
  key: 'currentPostSlug',
  default: '',
})

export const currentPostId = atom({
  key: 'currentPostId',
  default: 0,
})

export const currentCommentId = atom({
  key: 'currentCommentId',
  default: 0,
})
