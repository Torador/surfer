/**
<<<<<<< HEAD
 * @typedef {'users' | 'accounts' | 'posts' | 'comments' | 'reply' | 'topics' | 'follow'} Model
=======
 * @typedef {'users' | 'accounts' | 'posts' | 'comments' | 'reply' | 'topics' | 'follow' | 'conversations' | 'messages'} Model
>>>>>>> home-design
 */

/**
 * @typedef {'user' | 'admin'} Role
 */

/**
 * @typedef {'private' | 'public'} Visibility
 */

/**
 * @typedef {number | 'me'} ID
 */

/**
 * @typedef {'google' | 'gitbub' | 'facebook' | 'twitter' | 'discord' | 'linkedin'} Social
 */

/**
 * @typedef Topic
 * @property {number} [id]
 * @property {string} label
 * @property {string} [description]
 */

/**
 * @typedef Account
 * @property {number} [id]
 * @property {number} user_id
 * @property {string} firstname
 * @property {string} lastname
 * @property {string} [shortName]
 * @property {Date | string} birthdate
 * @property {string} email
 * @property {string} city
 * @property {string} address
 * @property {string} codePostal
 * @property {string} [slug]
 * @property {string} phone
 * @property {string} bio
 * @property {string | ArrayBuffer} photo
 * @property {string} job
 * @property {Contact[]} [contacts]
 * @property {Post[]} [creators]
 * @property {Follower[]} [followers]
 * @property {Post[]} [posts]
 * @property {number} [Rating]
 * @property {public | private} visibility
 * @property {string} [code_backup]
 * @property {string} [username]
 * @property {boolean} [status]
 * @property {boolean} [is_lock]
 * @property {boolean} [is_deleted]
 * @property {Date | string} [created_at]
 * @property {Date | string} [updatedAt]
 */

/**
 * @typedef Contact
 * @property {number} [id]
 * @property {number} account_id
 * @property {Follower[]} followers
 */

/**
 * @typedef Follower
 * @property {number} [id]
 * @property {number} account_id
 * @property {Contact[]} contacts
 */

/**
 * @typedef User
 * @property {number} [id]
 * @property {string} [role]
 * @property {string} [username]
 * @property {Account[]} [accounts]
 * @property {Date | string} [created_at]
 * @property {Date | string} [updatedAt]
 */

/**
 * @typedef FilesPost
 * @property {number} [id]
 * @property {number} post_id
 * @property {string} url_img
 * @property {string} filename
 * @property {string} mime_type
 * @property {number} size
 * @property {Date | string} [created_at]
 */

/**
 * @typedef Topic
 * @property {number} [id]
 * @property {string} label
 * @property {string} [description]
 * @property {string} [colorCode]
 * @property {Date | string} [created_at]
 */

/**
 * @typedef TopicPost
 * @property {number} [id]
 * @property {number | Topic} topic_id
 * @property {number | Post} post_id
 * @property {Date | string} [created_at]
 */

/**
 * @typedef Post
 * @property {number} [id]
 * @property {number} account_id
 * @property {string} title
 * @property {string} content
 * @property {string} [slug]
 * @property {FilesPost[]} [files]
 * @property {Topic[]} [topics]
 * @property {Account[]} [accounts]
 * @property {Comments[]} [comments]
 * @property {boolean} [is_comment_lock]
 * @property {boolean} [is_deleted]
 * @property {Date | string} [created_at]
 * @property {Date | string} [updated_at]
 */

/**
 * @typedef Comments
 * @property {number} [id]
 * @property {number} post_id
 * @property {number} [account_id]
 * @property {Account[]} [accounts]
 * @property {any[]} [replies]
 * @property {string} content
 * @property {boolean} [is_delete]
 * @property {Date | string} [created_at]
 * @property {Date | string} [updated_at]
 */

/**
 * @typedef Reply
 * @property {number} [id]
 * @property {string} content
 * @property {number} comment_id
 * @property {number} account_id
 * @property {Account[]} [replies]
 * @property {Date | string} [created_at]
 * @property {Date | string} [updated_at]
 */

/**
 * @typedef MediaMessage
 * @property {number} [id]
 * @property {number} [message_id]
 * @property {string} media_name
 * @property {string} url_media
 * @property {string} mime_type
 * @property {number} size
 * @property {Date | string} [created_at]
 */

/**
 * @typedef Message
 * @property {number} [id]
 * @property {number} conversation_id
 * @property {number} from_account_id
 * @property {number} to_account_id
 * @property {string} content
 * @property {string} format
 * @property {MediaMessage[]} [mediaMessage]
 * @property {boolean} have_seen
 * @property {boolean} is_delete
 * @property {Date | string} [created_at]
 */

/**
 * @typedef Conversation
 * @property {number} [id]
 * @property {number} account_init
 * @property {number} account_inter
 * @property {Message[]} [messages]
 * @property {Date | string} created_at
 */

/**
 * @typedef OptionDropdown
 * @type {Array<{activeIcon?: JSX.Element, icon: JSX.Element, label: string, color?: string, action: () => void}>[]}
 */

/**
 * @typedef MetaPagination
 * @property {string | null} next_page_url
 */

/**
<<<<<<< HEAD
=======
 * @typedef ErrorFetch
 * @property {string} error
 * @property {{field: string, message: string}[]} errors
 */

/**
>>>>>>> home-design
 * @typedef IsFollow
 * @property {number} contact_id
 * @property {number} follower_id
 */
