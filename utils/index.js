import { toast } from 'react-toastify'
import Toast from '../components/Toast'
import { getCookieValue } from './api.config'
import { format, formatDistanceStrict, formatRelative } from 'date-fns'
//@ts-check

/**
 * Parse Date
 * @param {Date} date
 * @returns {string}
 */
export const parseDate = (date) => new Intl.DateTimeFormat('FR-fr').format(date)

/**
 * CONVERTI UN TEXT EN TABLEAU DE CODE ASCII DE CHAQUE CHARACTERE
 * @param {string} text
 * @returns {Array<number>}
 */
const textToChars = (text) => text.split('').map((c) => c.charCodeAt(0))

/**
 * APPLIQUE LA CLE SECRETE AU MESSAGE AFIN DE LE CHIFFRER
 * @param {any} code
 * @returns {(salt: string) => any}
 */
const applySaltToChar = (code) => (salt) =>
  textToChars(salt.toString()).reduce((a, b) => a ^ b, code)

/**
 * CONVERTI UN CHARATERE EN HEXADECIMAL
 * @param {string} n
 */
const byteHex = (n) => ('0' + Number(n).toString(16)).substr(-2)

/**
 * CIPHER FUNCTION
 * @func cipher
 * @param {string} salt
 * @returns {(text: string) => string}
 * @description Prends la clé secrète et un message et le chiffre
 * @author Jordan Kagmeni (Torador)
 * @example
 * ```js
 * const text = 'algeloassurances';
 * const textEncoded = cipher(salt)(text);
 * ```
 */
export const cipher =
  (salt = process.env.NEXT_PUBLIC_KEY_SECRET) =>
  (text) =>
    text
      .split('')
      .map(textToChars)
      .map((code) => applySaltToChar(code)(salt))
      .map(byteHex)
      .join('')

/**
 * DECIPHER FUNCTION
 * @func decipher
 * @param {string} salt
 * @returns {(encoded: string) => string}
 * @description Prends la clé secrète et le message chiffré et le déchiffre
 * @author Jordan Kagmeni (Torador)
 * @example
 * ```js
 * const salt = process.env.KEY_ENCODE;
 * const msgToEncoded = 'algeloassurances';
 *
 * const encoded = cipher(salt)(msgToEncoded);
 * const msgDecoded = decipher(salt)(encoded);
 * // msgDecoded === msgToEncoded => true
 * ```
 */
export const decipher =
  (salt = process.env.NEXT_PUBLIC_KEY_SECRET) =>
  (encoded) =>
    encoded
      .match(/.{1,2}/g)
      .map((hex) => parseInt(hex, 16))
      .map((byte) => applySaltToChar(byte)(salt))
      .map((charCode) => String.fromCharCode(charCode))
      .join('')

/**
 * CRYPT USER DATA IDENTIFIANT
 * @param {string} username
 * @param {string} phoneNumber
 * @param {string} password
 */
export const cryptUserData = (username, phoneNumber, password) => {
  const userData = {
    phoneNumber: cipher()(phoneNumber),
    password: cipher()(password),
  }
  if (username) {
    return {
      username: cipher()(username),
      ...userData,
    }
  }
  return userData
}

/**
 * DECRYPT USER DATA IDENTIFIANT
 * @param {string} username
 * @param {string} phoneNumber
 * @param {string} password
 */
export const decryptUserData = (username, phoneNumber, password) => {
  const userData = {
    phoneNumber: decipher()(phoneNumber),
    password: decipher()(password),
  }
  if (username) {
    return {
      username: decipher()(username),
      ...userData,
    }
  }
  return userData
}

let code

/**
 * FECTH DATA ON SERVER AND TOAST ERROR IF OCCURED OR RETURS DATA
 * @param {RequestInfo} url
 * @param {RequestInit} params
 * @param {boolean | string} [authorization]
 * @param {string} [toastTitle]
 * @param {(l: boolean) => void} [setLoading]
 * @return {Promise<Object>}
 */
export async function jsonFetch(
  url,
  params,
  authorization = false,
  toastTitle = 'Système',
  setLoading
) {
  //let isFormData = false
  try {
    // Si on reçoit un FormData on le convertit en objet
    if (params.body instanceof FormData) {
      // @ts-ignore
      params.body = Object.fromEntries(params.body)
    }

    // Si on reçoit un objet on le convertit en chaine JSON
    if (params.body && typeof params.body === 'object') {
      params.body = JSON.stringify(params.body)
    }

    params = authorization
      ? {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
            Authorization: `bearer ${getCookieValue() || authorization}`,
          },
          ...params,
        }
      : {
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'X-Requested-With': 'XMLHttpRequest',
          },
          ...params,
        }
    setLoading && setLoading(true)
    const response = await fetch(url, params)
    if (response.status === 204) {
      return null
    }
    if (!response.ok) {
      if (response.status === 422) {
        code = 422
        throw new Error(
          JSON.stringify(JSON.parse(await response.text()).errors)
        )
      }

      throw new Error(
        JSON.parse(await response.text())?.message || 'Something wrong !'
      )
    }
    setLoading && setLoading(false)
    return (await response.json()) || (await response.text())
  } catch (error) {
    setLoading && setLoading(false)
    if (code === 401) {
      toast(
        <Toast
          title={toastTitle}
          message="Votre session est expirée. Veuillez vous authentifier SVP."
          type="error"
        />,
        {
          autoClose: 5000,
          closeButton: false,
        }
      )
    }
    if (code === 422) {
      const [err] = JSON.parse(error.message)
      toast(<Toast title={err.field} message={err.message} type="error" />, {
        autoClose: 5000,
        closeButton: false,
      })
    }
    toast(<Toast title={toastTitle} message={error.message} type="error" />, {
      autoClose: 5000,
      closeButton: false,
    })
  }
}

/**
 * DIFFERENCE BETWEEN DATE IN S, M, H, D, M, Y
 * @param {Date | string} startDate
 * @param {Date | string} [endDate]
 */
export const diffDate = (startDate, endDate = new Date()) => {
  const KEY = 'at '
  const HOUR = 3600
  const DAY = HOUR * 24

  const date = new Date(startDate)
  const diffInSecond = parseInt(
    formatDistanceStrict(date, endDate, {
      unit: 'second',
    }).replace('second', '')
  )

  if (diffInSecond < DAY) {
    if (parseInt(format(date, 'dd')) + 1 === parseInt(format(endDate, 'dd')))
      return 'Yesterday'
    return format(date, 'HH:mm')
  }

  if (diffInSecond > DAY && diffInSecond < 2 * DAY) return 'Yesterday'

  const result = formatRelative(date, endDate)
  if (!result.includes(KEY)) return result
  return result.split(KEY)[0]
}

/**
 * FECTH DATA ON SERVER AND TOAST ERROR IF OCCURED OR RETURS DATA
 * @param {RequestInfo} url
 * @param {object} [body]
 * @param {'json' | 'formdata'} [type]
 * @param {AbortSignal} [signal]
 * @param {string} token
 * @return {Promise<any>}
 */
export const myDbQuery = async (
  url,
  token,
  body = null,
  method = 'GET',
  type = 'json',
  signal
) => {
  let h = {}
  if (type === 'formdata') {
    const formdata = new FormData()
    for (const prop in body) {
      if (
        body[prop]?.length &&
        typeof body[prop] !== 'string' &&
        typeof body[prop] !== 'number'
      ) {
        body[prop].forEach((val) => {
          formdata.append(`${prop}[]`, val)
        })
      } else {
        formdata.append(prop, body[prop])
      }
    }
    body = formdata
  }

  if (
    (method === 'POST' && type !== 'formdata') ||
    ('PUT' && type !== 'formdata')
  ) {
    body = body && JSON.stringify(body)
    h = {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    }
  }

  const response = await fetch(url, {
    method,
    body,
    headers: { Authorization: `bearer ${token}`, ...h },
    signal,
  })

  if (!response.ok || response.status === 422)
    return { errors: JSON.parse(await response.text()) }
  else if (!response.ok) return { error: response.statusText }

  return await response.json()
}

/**
 *  CHECK ERROR AND RETRY IF RETRIES > 0
 * @param {{errors?: [], error?: string}} accounts
 * @param {number} retries
 */
export const checkErrorAndRetry = async (accounts, retries) => {
  console.log(accounts.errors || accounts.error)
  retries -= 1
  console.log(`retries left: ${retries}`)
  await new Promise((res) => setTimeout(res, 3000))
  if (retries === 0)
    throw new Error(JSON.stringify(accounts.errors || accounts.error))
}

/**
 * @author Jordan Kagmeni (Torador)
 * @copyright A'Access 2021
 */

/**
 * Stringify and caste value on lower case
 * @func serialize
 * @param {string} str
 * @description Stringify and caste value
 * @returns {string}
 */
function serialize(str) {
  return str.toString().replace(/\s/g, '').toLowerCase()
}

/**
 * @func ObjectValues
 * @param {object} value
 * @returns {string}
 */
function objectValues(value) {
  return Object.values(value).reduce((string, val) => {
    const test = val !== null && val !== undefined
    return (
      string +
      (typeof val === 'object' && val !== null
        ? serialize(objectValues(val))
        : test
        ? serialize(val)
        : '')
    )
  }, '')
}

/**
 * FILTERED VALUE IN ARRAY ITEM
 * @param {string} val
 * @param {Array} data
 * @returns {Array}
 * @author Jordan Kagmeni (Torador)
 */
export function ToradorFilterDom(val, data) {
  return data.filter((el) => {
    return val.length ? objectValues(el).includes(serialize(val)) : true
  })
}

/**
 * FORMAT NUMBER
 * @param {number} number
 * @returns {string}
 */
export const formatNumber = (number) =>
  new Intl.NumberFormat('fr-FR', {
    notation: 'compact',
    compactDisplay: 'short',
  }).format(number)
