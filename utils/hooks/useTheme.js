//@ts-check
import { useCallback, useEffect, useState } from 'react'
import { SURFER_THEME } from '../const'

/**
 * USE THEME HOOK
 */
function useTheme() {
  const [theme, setTheme] = useState('light')

  useEffect(() => {
    const t = localStorage.getItem(SURFER_THEME)
    if (t !== null && typeof t === 'string') {
      document.documentElement.setAttribute('class', t)
      setTheme(t)
    }
  }, [])

  const toggleTheme = useCallback(
    /**
     *
     * @param { 'light' | 'dark' | 'auto'} newTheme
     */
    (newTheme) => {
      document.documentElement.setAttribute('class', newTheme)
      localStorage.setItem(SURFER_THEME, newTheme)
      setTheme(newTheme)
    },
    []
  )

  return [theme, toggleTheme]
}

export default useTheme
