import { useState } from 'react'
import { useRecoilValue } from 'recoil'
import { currentUserLogin } from '../../atoms'

/**
 * USE RATE AVERAGE HOOK
 * @param {Account[]} accounts
 * @returns {[rating: number, setRating: (rate: number) => void]}
 */
function useRateAverage(accounts) {
  const user = useRecoilValue(currentUserLogin)
  const rateUser = accounts?.filter((acc) => acc.id === user?.accounts[0].id)[0]
    ?.Rating
  let totalRate = 0
  accounts?.forEach((acc) => {
    totalRate += acc.Rating
  })
  const [rating, setRating] = useState(
    rateUser || Math.floor(totalRate / accounts?.length)
  )

  return [rating, setRating]
}

export default useRateAverage
