import { useCallback, useEffect, useRef } from 'react'
import { myDbQuery } from '..'

export function useFetch() {
  const abortController = useRef(new AbortController())
  useEffect(() => () => abortController.current.abort(), [])

  return useCallback(
    /**
     *  USE FETCH HOOK WITH AUTO ABORT REQUEST
     * @param {string} url
     * @param {{token: string, body: null | object, method: 'GET' | 'POST' | 'PUT' | 'DELETE', type: 'json' | 'formdata'}} options
     */
    (url, options) => {
      return myDbQuery(
        url,
        options.token,
        options.body,
        options.method,
        options.type,
        abortController.current.signal
      )
    },
    []
  )
}
