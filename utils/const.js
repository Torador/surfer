export const SYS_AUTH = 'Authentication system'
export const SYS_REGISTER = 'Registration system'
export const SYS_PASSWORD = 'Password system '
export const KEY_COOKIE = 'surfer_token'
export const LOGIN_URL = `${process.env.NEXT_PUBLIC_HOST_API}/users/login`
export const LOGOUT_URL = `${process.env.NEXT_PUBLIC_HOST_API}/users/logout`
export const TOKEN_CHECK_URL = `${process.env.NEXT_PUBLIC_HOST_API}/token/check`
export const RETRIES = 2
export const COLORS_RATING = [
  '#EF4444',
  '#F87171',
  '#A5B4FC',
  '#818CF8',
  '#6366F1',
]
export const DEFAULT_AVATAR =
  '/user.png' ||
  'https://img.icons8.com/external-emojis-because-i-love-you-royyan-wijaya/32/000000/external-avatar-hana-emojis-general-ii-emojis-because-i-love-you-royyan-wijaya-31.png'
export const ACCEPT_FILES_POST = 'image/*' //You can add video/* for accept video
export const LIMIT_SIZE = 10_000_000

export const LINKS_FOOTER = [
  { label: 'About', url: '#' },
  { label: 'Careers', url: '#' },
  { label: 'Terms', url: '#' },
  { label: 'Privacy', url: '#' },
  { label: 'Acceptable Use', url: '#' },
  { label: 'Businesses', url: '#' },
  { label: 'Press', url: '#' },
  { label: 'Your Ad Choices', url: '#' },
  { label: 'Impressum', url: '#' },
]

export const BG_COLORS = {
  gray: 'bg-gray-500 dark:bg-gray-900',
  indigo: 'bg-indigo-500 dark:bg-indigo-900',
  green: 'bg-green-500 dark:bg-green-900',
  yellow: 'bg-yellow-500 dark:bg-yellow-900',
  red: 'bg-red-500 dark:bg-red-900',
  purple: 'bg-purple-500 dark:bg-purple-900',
  blue: 'bg-blue-500 dark:bg-blue-900',
}
export const TEXT_COLORS = {
  gray: 'text-gray-700 dark:text-gray-400',
  indigo: 'text-indigo-700 dark:text-indigo-400',
  green: 'text-green-700 dark:text-green-400',
  yellow: 'text-yellow-700 dark:text-gray-300',
  red: 'text-red-700 dark:text-red-400',
  purple: 'text-purple-700 dark:text-purple-400',
  blue: 'text-blue-700 dark:text-blue-400',
}

export const LIMIT_VIEW_PERSONS_RATING = 4

export const SURFER_THEME = 'surfer_theme'

/**
 * @param {'post' | 'comment' | 'reply'} entity
 * @returns {string}
 */
export const DELETE_ENTITY = (entity) =>
  `Are you sure you want to delete this ${entity} ?`
// &middot;
