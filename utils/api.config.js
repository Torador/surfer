//@ts-check
import Cookies from 'js-cookie'
import { KEY_COOKIE } from './const'

/**
 * GET COOKIE IN BROWSER ON CURRENT DOMAIN
 * @returns {string}
 */
export const getCookieValue = () => Cookies.get(KEY_COOKIE)

/**
 * SET COOKIE IN BROWSER ON CURRENT DOMAIN
 * @param {{token: string, expires_at: Date | string }} params
 * @returns {string}
 * @description Log user in this case
 */
export const setCookieValue = ({ token, expires_at }) =>
  Cookies.set(KEY_COOKIE, token, {
    expires: new Date(expires_at),
    sameSite: 'strict',
  })

/**
 * REMOVE COOKIE IN BROWSER ON CURRENT DOMAIN
 * @returns {void}
 * @description Logout user in this case
 */
export const removeCookie = () => Cookies.remove(KEY_COOKIE)

/**
 * @param {Model} model
 */
export const endPointGetAll = (model) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}`

/**
 *
 * @param {Model} model
 * @param {number} page
 * @param {number} [limit]
 */
export const endPointGetAllPaginate = (model, page, limit) =>
  limit
    ? `${process.env.NEXT_PUBLIC_HOST_API}/${model}/page/${page}/${limit}`
    : `${process.env.NEXT_PUBLIC_HOST_API}/${model}/page/${page}`

/**
 * @param {Model} model
 */
export const endPointRate = (model) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}/rate`

/**
 *
 * @param {Model} model
 * @param {ID} id
 */
export const endPointGetSingle = (model, id) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}/${id}`

/**
 * @param {Model} model
 * @param {string} slug
 */
export const endPointGetBySlug = (model, slug) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}/${slug}`

/**
 *
 * @param {Model} model
 */
export const endPointPost = (model) =>
  model === 'users'
    ? `${process.env.NEXT_PUBLIC_HOST_API}/${model}/register`
    : `${process.env.NEXT_PUBLIC_HOST_API}/${model}`

/**
 *
 * @param {Model} model
 * @param {ID} id
 */
export const endPointPut = (model, id) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}/update/${id}`

/**
 *
 * @param {Model} model
 * @param {ID} id
 */
export const endPointDelete = (model, id) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}/delete/${id}`

/**
 * @param {Role} role
 * @param {ID} id
 */
export const endPointPutRoleUser = (role, id) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/users/${role}/${id}`

/**
 * @param {ID} id
 * @param {Model} model
 */
export const endPointLock = (model, id) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}/tooglelock/${id}`

/**
 * @param {Visibility} visibility
 * @param {ID} id
 */
export const endPointPutVisibility = (visibility, id) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/accounts/${visibility}/${id}`
/**
 * @param {Model} model
 * @param {string} variables
 */
export const endPointCustom = (model, variables) =>
  `${process.env.NEXT_PUBLIC_HOST_API}/${model}/${variables}`
