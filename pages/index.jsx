//@ts-check
/* eslint-disable @next/next/no-img-element */
import Head from 'next/head'
import NavAuth from '../components/NavAuth'
import Auth from '../components/Auth'
import { KEY_COOKIE, TOKEN_CHECK_URL } from '../utils/const'

export default function Home() {
  return (
    <div className="overflow-hidden">
      <Head>
        <title>Surfer - Sign In or Register</title>
      </Head>
      <div className="max-w-5xl min-h-screen mx-auto">
        <NavAuth />
        <div className="grid grid-cols-1 md:grid-cols-2 gap-3 relative">
          {/* LEFT SECTION */}
          <div className="hidden md:flex md:min-h-screen items-center justify-center animate-fade">
            <div className="relative w-full">
              <div className="absolute top-0 -left-4 w-72 h-72 bg-indigo-300 dark:bg-indigo-700 rounded-full mix-blend-multiply filter blur-xl opacity-30 animate-blob" />
              <div className="absolute top-0 -right-4 w-72 h-72 bg-pink-300 dark:bg-pink-700 rounded-full mix-blend-multiply filter blur-xl opacity-30 animate-blob animation-delay-2000" />
              <div className="absolute -bottom-8 left-20 w-72 h-72 bg-yellow-300 dark:bg-yellow-700 rounded-full mix-blend-multiply filter blur-xl opacity-30 animate-blob animation-delay-4000" />
              <div className="hidden md:inline absolute -bottom-60 transform translate-x-32">
                <img
                  src="/SVG/Saly-1.svg"
                  alt="hero"
                  className="filter drop-shadow-sm"
                />
              </div>
              <div className="absolute -bottom-50 transform -translate-x-64 hidden xl:inline">
                <img
                  src="/SVG/Saly-2.svg"
                  alt="hero"
                  className="filter drop-shadow-sm"
                />
              </div>

              <div className="m-8 relative space-y-8 w-[400px] -top-20">
                <h1 className="text-7xl font-extrabold text-gray-800 dark:text-white tracking-wide">
                  <span className="filter dark:drop-shadow-md">Sign In to</span>{' '}
                  <span className="text-indigo-500 italic filter dark:drop-shadow-md">
                    Surfer
                  </span>
                </h1>
                <p className="text-gray-700 dark:text-gray-100 text-2xl dark:text-3xl font-semibold filter drop-shadow-sm">
                  The best platform to achieve your career goals.
                </p>
              </div>
            </div>
          </div>
          {/* END SECTION */}

          {/* RIGHT SECTION */}
          <Auth />
          {/* END SECTION */}
        </div>
      </div>
    </div>
  )
}

/**
 * @param {import('next').GetServerSidePropsContext} ctx
 */
export async function getServerSideProps({ req }) {
  const { cookies } = req
  if (KEY_COOKIE in cookies) {
    const isValidCookie = await fetch(TOKEN_CHECK_URL, {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.text())
    const v = JSON.parse(isValidCookie)?.errors
    if (v) {
      return {
        props: {},
      }
    } else {
      return {
        redirect: {
          permanent: false,
          destination: '/home',
        },
      }
    }
  }
  return {
    props: {},
  }
}
