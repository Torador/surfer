import { useEffect } from 'react'
import { useRecoilValue, useSetRecoilState } from 'recoil'
import NavBar from '../components/NavBar'
import TopicsList from '../components/TopicsList'
import { endPointGetAll, endPointGetSingle } from '../utils/api.config'
import { KEY_COOKIE } from '../utils/const'
import {
  accountListState,
  currentUserLogin,
  suggestionListState,
  tokenUser,
  topicListState,
} from '../atoms'
import PostsList from '../components/PostsList'
import PostButton from '../components/PostButton'
import FrontPostAdd from '../components/FrontPostAdd'
import Sidebar from '../components/Sidebar'
import ModalPost from '../components/ModalPost'
import { checkTokenSS } from '../helpers'
import ModalInfoComplete from '../components/ModalInfoComplete'
import ListSuggestion from '../components/ListSuggestion'
import Badge from '../components/Badge'
import { formatNumber } from '../utils'

/**
 *  HOME PAGE
 * @param {{me: User, token: string, topics: Topic[]}} props
 * @returns {JSX.Element}
 */
function Home({ me, token, topics, accounts, isComplete }) {
  const setCurrentUserInfo = useSetRecoilState(currentUserLogin)
  const setAccountList = useSetRecoilState(accountListState)
  const setTokenUser = useSetRecoilState(tokenUser)
  const setTopicList = useSetRecoilState(topicListState)
  const suggestions = useRecoilValue(suggestionListState)

  useEffect(() => {
    //document.body.classList.add('dark')
    setTokenUser(token)
    setCurrentUserInfo(me)
    setAccountList(accounts)
    setTopicList(topics)
  }, [
    token,
    setCurrentUserInfo,
    me,
    setAccountList,
    accounts,
    setTopicList,
    topics,
    setTokenUser,
  ])

  const isCompleteFullName = !!me.accounts[0]?.shortName // if shortName is null ? true : false

  return (
    <>
      <NavBar token={token} />
      <div className="min-h-screen flex px-2 sm:px-0">
        <ModalPost token={token} />
        <ModalInfoComplete isShow={isComplete} token={token} />
        {/* LEFT SECTION */}
        <aside className="hidden xl:inline flex-shrink min-w-[300px] shadow relative">
          <Sidebar token={token} />
        </aside>

        {/* MIDDLE SECTION */}
        <main className="flex-grow relative">
          <div className="flex flex-col space-y-8 overflow-x-hidden relative">
            {/* TOPICS LIST SECTION */}

            <TopicsList token={token} />

            <section className="flex flex-col space-y-12 md:space-y-5 pb-10 max-w-5xl md:max-w-7xl mx-auto mt-20">
              <FrontPostAdd isCompleteFullName={isCompleteFullName} me={me} />
              <div className="flex w-full rounded-xl">
                <PostsList token={token} />
              </div>
            </section>
          </div>
          <PostButton me={me} />
        </main>

        {/* RIGHT SECTION */}
        <aside className="hidden xl:inline flex-shrink min-w-[300px] shadow relative">
          <div className="fixed top-18 flex flex-col space-y-3 max-h-screen px-3 pt-2 pb-24 overflow-y-auto w-[300px] mx-auto">
            <div className="flex items-center w-full justify-between mb-2 mt-4">
              <div className="font-bold flex items-center space-x-1 text-gray-800 dark:text-white">
                <span>Suggestions</span>
                <Badge
                  label={formatNumber(
                    suggestions.filter((suggestion) => {
                      if (
                        !suggestion?.contacts[0]?.followers?.filter(
                          (f) => f.account_id === me.accounts[0].id
                        )[0]
                      )
                        return suggestion
                    }).length || 0
                  )}
                  color="indigo"
                  shape
                />
              </div>
              <button
                type="button"
                className="text-xs text-blue-500 hover:underline"
              >
                More
              </button>
            </div>
            <ListSuggestion token={token} />
          </div>
        </aside>
      </div>
    </>
  )
}

export default Home

/**
 * @param {import('next').GetServerSidePropsContext} ctx
 */

export async function getServerSideProps({ req }) {
  const { cookies } = req
  if (KEY_COOKIE in cookies) {
    const errors = await checkTokenSS(cookies)

    if (errors)
      return {
        redirect: {
          permanent: false,
          destination: '/',
        },
      }

    const me = await fetch(endPointGetSingle('users', 'me'), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())

    const topics = await fetch(endPointGetAll('topics'), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())

    const accounts = await fetch(endPointGetAll('accounts'), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())

    return {
      props: {
        me,
        topics,
        accounts,
        isComplete: !me.accounts[0]?.shortName,
        token: cookies[KEY_COOKIE],
      },
    }
  }

  return {
    redirect: {
      permanent: false,
      destination: '/',
      cookies: '',
    },
  }
}
