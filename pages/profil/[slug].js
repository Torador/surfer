//@ts-check
import {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react'
import Image from 'next/image'
import { format } from 'date-fns'
import NavBar from '../../components/NavBar'
import { DEFAULT_AVATAR, KEY_COOKIE } from '../../utils/const'
import { checkTokenSS } from '../../helpers/index'
import { useRecoilState, useSetRecoilState } from 'recoil'
import {
  accountListState,
  currentUserLogin,
  postListState,
  userListState,
} from '../../atoms'
import { formatNumber, myDbQuery } from '../../utils'
import {
  endPointCustom,
  endPointGetAll,
  endPointGetSingle,
  endPointPut,
} from '../../utils/api.config'
import Badge from '../../components/Badge'
import {
  BriefcaseIcon,
  CalendarIcon,
  ChatIcon,
  DotsHorizontalIcon,
  LocationMarkerIcon,
  PencilIcon,
} from '@heroicons/react/outline'
import { BorderTabs } from '../../components/Tabs'
import Post from '../../components/Post'
import { Tab } from '@headlessui/react'
import ModalPost from '../../components/ModalPost'
import ProfilImg from '../../components/ProfilImg'

/**
 * Show Profil Page
 * @param {{profil: Account, token: string, users: User[], me: User}} props
 * @returns {JSX.Element}
 */
function ShowProfil({ token, me, profil: info, users }) {
  console.log(info)
  const isMe = me.accounts[0].id === info.id
  const [profil, setProfil] = useState(info)
  const filePickerRef = useRef(null)
  const [user, setCurrentUserInfo] = useRecoilState(currentUserLogin)
  const [posts, setPosts] = useRecoilState(postListState)
  const setUserList = useSetRecoilState(userListState)
  const [accounts, setAccounts] = useRecoilState(accountListState)

  useEffect(() => {
    setProfil(info)
  }, [info])

  useEffect(() => {
    setCurrentUserInfo(me)
    setUserList(users)
    setAccounts([...users.map((user) => user.accounts[0])])
    setPosts(profil.creators)
  }, [
    users,
    me,
    profil.creators,
    setCurrentUserInfo,
    setPosts,
    setUserList,
    setAccounts,
  ])

  const _saveImgProfil = useCallback(() => {
    const files = filePickerRef.current?.files
    if (!files) return
    if (files.length <= 0) return

    const birthdate = user.accounts[0]?.birthdate
    myDbQuery(
      endPointPut('accounts', null),
      token,
      {
        ...user.accounts[0],
        picture: files[0],
        birthdate: birthdate
          ? `${birthdate.toString().split('-')[2]}/${
              birthdate.toString().split('-')[1]
            }/${birthdate.toString().split('-')[0]}`
          : '',
      },
      'PUT',
      'formdata'
    ).catch((err) => console.log(err))
  }, [user, token])

  const followers = useMemo(() => profil.contacts[0]?.followers, [profil])

  const following = useMemo(
    () => profil?.followers[0]?.contacts?.length,
    [profil]
  )

  const tabs = [
    `${formatNumber(profil?.creators?.length)} Posts`,
    `${formatNumber(profil?.posts?.length)} Reactions`,
    `${formatNumber(followers?.length)} Followers`,
    `${following} Following`,
  ]

  return (
    <div className="flex flex-col w-full">
      <NavBar active="#" token={token} />
      <ModalPost token={token} />
      <main className="mt-20 md:mt-0 grid grid-cols-1 md:grid-cols-5 gap-x-10 min-h-screen dark:text-white w-11/12 mx-auto md:max-w-5xl py-4 px-1">
        {/* SECTION LEFT */}
        <section className="col-span-3 flex flex-col space-y-4">
          <div className="flex flex-col space-y-2">
            <div className="flex items-center sm:items-start space-x-5">
              <div className="group flex-shrink relative w-20 h-16 sm:w-32 sm:h-32 rounded-full border border-gray-200 dark:border-gray-800">
                <ProfilImg
                  saveProfil={_saveImgProfil}
                  filePickerRef={filePickerRef}
                  photoUrl={profil?.photo}
                  isEdit={isMe}
                />
              </div>
              <div className="flex-grow flex flex-col">
                <div className="flex items-center justify-between">
                  <div>
                    <Badge label={profil?.visibility} color="indigo" />
                    {isMe ? <Badge label="You" color="yellow" /> : ''}
                  </div>
                  <button type="button" className="s_btn_secondary_rounded">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="m21 12-9.778-8v5.333c-1.926.45-3.5 1.172-4.722 2.167-1.222.995-2.389 2.495-3.5 4.5 1.333-.659 2.833-1.157 4.5-1.496 1.667-.34 2.908-.285 3.722.163V20L21 12Z"
                        className="icon_svg-stroke"
                        stroke="#666"
                        fill="none"
                        strokeWidth="1.5"
                        strokeLinejoin="round"
                      ></path>
                    </svg>
                  </button>
                </div>
                <h1
                  title={`${profil?.lastname || ''} ${profil?.firstname || ''}`}
                  className="text-3xl text-gray-800 dark:text-gray-100 font-bold truncate max-w-[420px]"
                >
                  {!profil?.shortName
                    ? users.filter(
                        (user) => user?.accounts[0]?.id === profil.id
                      )[0]?.username
                    : profil?.shortName}
                </h1>
                <div className="flex items-center py-2 space-x-1 text-sm text-gray-500 font-bold">
                  <button className="hover:underline">
                    {followers ? formatNumber(followers.length) : 0} followers
                  </button>
                  <span>&middot;</span>
                  <button className="hover:underline">
                    {following ? formatNumber(following) : 0} followings
                  </button>
                </div>
                <div className="flex items-center space-x-2">
                  <button className="flex items-center space-x-1 text-sm shadow-sm bg-indigo-500 hover:bg-indigo-600 px-4 py-1 rounded-full text-white font-semibold transition duration-300">
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g
                        className="text-white stroke-current"
                        strokeWidth="1.5"
                        fill="none"
                        fillRule="evenodd"
                      >
                        <path d="M12 12.5A2.25 2.25 0 1 0 12 8a2.25 2.25 0 0 0 0 4.5zm3 4a3 3 0 1 0-6 0"></path>
                        <path
                          d="M13.5 19.5h-6a3 3 0 0 1-3-3v-9a3 3 0 0 1 3-3h9a3 3 0 0 1 3 3v6m-2.5 6h5M19.5 17v5"
                          strokeLinecap="round"
                        ></path>
                      </g>
                    </svg>
                    <span>
                      {followers?.filter(
                        (f) => f.account_id === user?.accounts[0].id
                      )[0]
                        ? 'Unfollow'
                        : 'Follow'}
                    </span>
                  </button>
                  <button className="flex items-center space-x-1 text-sm border border-gray-200 dark:border-gray-700 hover:border-gray-300 dark:hover:border-gray-800 hover:bg-opacity-60 bg-gray-100 dark:bg-gray-800 hover:bg-gray-200 dark:hover:bg-gray-900 text-gray-500 px-4 py-1 rounded-full transition duration-300 font-semibold">
                    <ChatIcon className="h-5" />
                    <span>Message</span>
                  </button>
                  <button type="button" className="s_btn_secondary_rounded">
                    <DotsHorizontalIcon className="h-5" />
                  </button>
                </div>
              </div>
            </div>
            {profil?.bio && profil?.bio !== 'null' && (
              <p className="text-sm text-gray-500 line-clamp-3 hover:line-clamp-none text-justify">
                {profil.bio}
              </p>
            )}
          </div>
          <Tab.Group>
            <Tab.List className="flex items-center py-4 border-b border-gray-200 dark:border-gray-800 space-x-4">
              {tabs.map((tab, i) => (
                <Tab as={Fragment} key={i}>
                  {({ selected }) => (
                    <button
                      type="button"
                      className={`text-sm font-semibold relative ${
                        selected ? 'text-indigo-500' : 'text-gray-500'
                      }`}
                    >
                      {tab}
                      {selected && <BorderTabs />}
                    </button>
                  )}
                </Tab>
              ))}
            </Tab.List>
            <Tab.Panels className="mt-2">
              <Tab.Panel className="flex flex-col space-y-4">
                {posts?.map((post) => (
                  <Post key={post.id} post={post} user={me} token={token} />
                ))}
              </Tab.Panel>
              <Tab.Panel className="flex flex-col space-y-4">
                {profil?.posts?.map((post) => (
                  <Post key={post.id} post={post} user={me} token={token} />
                ))}
              </Tab.Panel>
              <Tab.Panel>
                {profil?.contacts[0]?.followers?.map((follower) => {
                  return accounts?.map((account) => {
                    if (account?.id !== follower.account_id) return
                    return (
                      <div
                        className="flex justify-between w-full items-center border-b border-gray-100 dark:border-gray-800 dark:bg-transparent p-2 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:bg-opacity-60 transition-colors"
                        key={follower.id}
                      >
                        <div className="flex items-center space-x-2">
                          <div className="relative h-14 w-14 rounded-full">
                            <Image
                              src={account?.photo || DEFAULT_AVATAR}
                              layout="fill"
                              alt={account.email}
                              className="object-cover rounded-full"
                            />
                          </div>
                          <div className="flex flex-col">
                            <h1 className="font-bold text-gray-800 dark:text-gray-50 max-w-[190px] truncate">
                              {account?.lastname} {account?.firstname}
                            </h1>
                            {account.job ? (
                              <p className="text-gray-500 text-xs max-w-[190px] truncate">
                                {account.job} &middot; {account.city}
                              </p>
                            ) : (
                              <p className="text-gray-500 text-xs max-w-[190px] truncate">
                                {account.city}
                              </p>
                            )}
                          </div>
                        </div>
                        {account?.id === me?.accounts[0].id && (
                          <button className="p-1 w-[70px] rounded-full text-white font-semibold text-xs shadow bg-gradient-to-tr from-indigo-500 to-blue-400 dark:to-blue-600 hover:shadow-lg transform active:scale-95 transition duration-300">
                            Unfollow
                          </button>
                        )}

                        {isMe && (
                          <button className="p-1 w-[60px] rounded-full text-white font-semibold text-xs shadow bg-gradient-to-tr from-red-500 to-pink-400 dark:to-red-600 hover:shadow-lg transform active:scale-95 transition duration-300">
                            Remove
                          </button>
                        )}
                      </div>
                    )
                  })
                })}
              </Tab.Panel>
              <Tab.Panel>
                {profil?.followers[0]?.contacts?.map((contact) => {
                  return accounts?.map((account) => {
                    if (account?.id !== contact.id) return
                    return (
                      <div
                        className="flex justify-between w-full items-center border-b border-gray-100 dark:border-gray-800 dark:bg-transparent p-2 hover:bg-gray-100 dark:hover:bg-gray-800 dark:hover:bg-opacity-60 transition-colors"
                        key={contact.id}
                      >
                        <div className="flex items-center space-x-2">
                          <div className="relative h-14 w-14 rounded-full">
                            <Image
                              src={account?.photo || DEFAULT_AVATAR}
                              layout="fill"
                              alt={account.email}
                              className="object-cover rounded-full"
                            />
                          </div>
                          <div className="flex flex-col">
                            <h1 className="font-bold text-gray-800 dark:text-gray-50 max-w-[190px] truncate">
                              {account?.lastname} {account?.firstname}
                            </h1>
                            {account.job ? (
                              <p className="text-gray-500 text-xs max-w-[190px] truncate">
                                {account.job} &middot; {account.city}
                              </p>
                            ) : (
                              <p className="text-gray-500 text-xs max-w-[190px] truncate">
                                {account.city}
                              </p>
                            )}
                          </div>
                        </div>
                        {account?.id === me?.accounts[0].id && (
                          <button className="p-1 w-[60px] rounded-full text-white font-semibold text-xs shadow bg-gradient-to-tr from-red-500 to-pink-400 dark:to-red-600 hover:shadow-lg transform active:scale-95 transition duration-300">
                            Remove
                          </button>
                        )}
                        {isMe && (
                          <button className="p-1 w-[70px] rounded-full text-white font-semibold text-xs shadow bg-gradient-to-tr from-indigo-500 to-blue-400 dark:to-blue-600 hover:shadow-lg transform active:scale-95 transition duration-300">
                            Unfollow
                          </button>
                        )}
                      </div>
                    )
                  })
                })}
              </Tab.Panel>
            </Tab.Panels>
          </Tab.Group>
        </section>
        {/* SECTION RIGHT */}
        <section className="col-span-2 flex flex-col py-2">
          <div className="flex items-center justify-between border-b border-gray-200 dark:border-gray-800 mb-3">
            <h1 className="font-semibold text-gray-800 dark:text-gray-100 py-3 text-xl">
              Credentials & highlights
            </h1>
            {isMe && (
              <button type="button" className="s_btn_secondary_rounded">
                <PencilIcon className="h-5" />
              </button>
            )}
          </div>
          {profil?.job ? (
            <div className="flex items-center space-x-2">
              <span className="p-1 rounded-full text-gray-600 dark:text-gray-500 bg-gray-200 dark:bg-gray-800 bg-opacity-50 dark:bg-opacity-50">
                <BriefcaseIcon className="h-4" />
              </span>
              <span className="text-gray-700 dark:text-gray-400">
                {profil?.job}
              </span>
            </div>
          ) : (
            isMe && (
              <button
                type="button"
                className="flex items-center space-x-2 mb-3 group"
              >
                <BriefcaseIcon className="h-5 text-gray-600 dark:text-gray-500" />
                <span className=" text-blue-700 dark:text-blue-500 group-hover:underline">
                  Add employment credential
                </span>
              </button>
            )
          )}
          {profil?.city ? (
            <div className="flex items-center space-x-2">
              <span className="p-1 rounded-full text-gray-600 dark:text-gray-500 bg-gray-200 dark:bg-gray-800 bg-opacity-50 dark:bg-opacity-50">
                <LocationMarkerIcon className="h-4" />
              </span>
              <span className="text-gray-700 dark:text-gray-400">
                {profil?.city}
              </span>
            </div>
          ) : (
            isMe && (
              <button
                type="button"
                className="flex items-center space-x-2 mb-3 group"
              >
                <LocationMarkerIcon className="h-5 text-gray-600 dark:text-gray-500" />
                <span className=" text-blue-700 dark:text-blue-500 group-hover:underline">
                  Add City
                </span>
              </button>
            )
          )}
          <div className="flex items-center space-x-2">
            <span className="p-1 rounded-full text-gray-600 dark:text-gray-500 bg-gray-200 dark:bg-gray-800 bg-opacity-50 dark:bg-opacity-50">
              <CalendarIcon className="h-4" />
            </span>
            <span className="text-gray-700 dark:text-gray-400">
              Joined at {format(new Date(profil?.created_at), 'MMMM yyyy')}
            </span>
          </div>
        </section>
      </main>
    </div>
  )
}

export default ShowProfil

/**
 * @param {import('next').GetServerSidePropsContext} ctx
 */

export async function getServerSideProps({ req }) {
  const { cookies } = req
  if (KEY_COOKIE in cookies) {
    const errors = await checkTokenSS(cookies)

    if (errors)
      return {
        redirect: {
          permanent: false,
          destination: '/',
        },
      }

    const me = await fetch(endPointGetSingle('users', 'me'), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())
    const users = await fetch(endPointGetAll('users'), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())

    const slug =
      req.url.split('?')[1]?.replace('slug=', '') ||
      req.url.replace('/profil/', '')

    const profil = await fetch(endPointCustom('accounts', `slug/${slug}`), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())

    return {
      props: {
        me,
        profil,
        users,
        token: cookies[KEY_COOKIE],
      },
    }
  }

  return {
    redirect: {
      permanent: false,
      destination: '/',
      cookies: '',
    },
  }
}
