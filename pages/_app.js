import { Router } from 'next/router'
import { RecoilRoot } from 'recoil'
import { ToastContainer } from 'react-toastify'
import Head from 'next/head'
import ProgressBar from '@badrap/bar-of-progress'
import '../styles/globals.css'
import 'tippy.js/dist/tippy.css'
import 'react-toastify/dist/ReactToastify.css'

const progress = new ProgressBar({
  size: 4,
  color: '#5881F2',
  className: 'z-50',
  delay: 100,
})

Router.events.on('routeChangeStart', progress.start)
Router.events.on('routeChangeComplete', progress.finish)
Router.events.on('routeChangeError', progress.finish)

function MyApp({ Component, pageProps }) {
  return (
    <RecoilRoot>
      <Head>
        <title>Surfer | Professionnal Social Network</title>

        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/favicon/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon/favicon-16x16.png"
        />
        <link rel="icon" href="/favicon/favicon.ico" type="image/x-icon" />
        <link rel="manifest" href="/favicon/site.webmanifest" />
        <meta
          name="description"
          content="Application Produce by Jordan, Youcef and Eddy."
        />
      </Head>
      <div className="bg-gray-50 dark:bg-black dark:bg-opacity-90 min-h-screen">
        <ToastContainer draggable hideProgressBar />
        <Component {...pageProps} />
      </div>
    </RecoilRoot>
  )
}

export default MyApp
