import { serialize } from 'cookie'

/**
 *
 * @param {import("next").NextApiRequest} req
 * @param {import("next").NextApiResponse} res
 */
export default function logout(req, res) {
  res.setHeader(
    'Set-Cookie',
    serialize('surfer_token', null, {
      httpOnly: true,
      expires: new Date(0),
      secure: process.env.NODE_ENV === 'production',
      sameSite: 'strict',
      path: '/',
    })
  )

  return res.json({})
}
