import { serialize } from 'cookie'

/**
 *
 * @param {import("next").NextApiRequest} req
 * @param {import("next").NextApiResponse} res
 */
export default function auth(req, res) {
  const { token } = JSON.parse(req.body)
  res.setHeader(
    'Set-Cookie',
    serialize('surfer_token', token, {
      httpOnly: true,
      maxAge: 60 * 60 * 24 * 3,
      secure: process.env.NODE_ENV === 'production',
      sameSite: 'strict',
      path: '/',
    })
  )

  return res.json({ ok: true })
}
