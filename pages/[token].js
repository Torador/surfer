//@ts-check
import { useEffect } from 'react'
import { TOKEN_CHECK_URL } from '../utils/const'

/**
 * PAGE DE REDIRECTION APRES AUTHENTIFICATION DELEGUE DEPUIS LE SERVEUR API
 * @returns {JSX.Element}
 */
function Token() {
  useEffect(() => {
    const token = window.location.href.split('3000/')[1].replace('#', '')
    fetch('/api/auth', {
      method: 'POST',
      body: JSON.stringify({ token }),
    }).then((_) => {
      window.close()
    })
  }, [])
  return (
    <div className="min-h-screen flex justify-center items-center bg-gray-100">
      <div className="bg-white shadow-md p-4 rounded-md">Redirection...</div>
    </div>
  )
}

export default Token

/**
 * @param {import('next').GetServerSidePropsContext} ctx
 */
export async function getServerSideProps({ req }) {
  const token = req.url.replace('/', '')
  const res = await fetch(TOKEN_CHECK_URL, {
    headers: { Authorization: `bearer ${token}` },
  })
  if (!res.ok) {
    return {
      redirect: {
        permanent: false,
        destination: '/',
      },
    }
  }
  return {
    props: {},
  }
}
