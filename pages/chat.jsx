import React, { useEffect, useRef } from 'react'
import io from 'socket.io-client'
import ListConversation from '../components/ListConversation'
import ModalPost from '../components/ModalPost'
import NavBar from '../components/NavBar'
import { KEY_COOKIE, TOKEN_CHECK_URL } from '../utils/const'
import { endPointGetAll, endPointGetSingle } from '../utils/api.config'
import { PlusIcon } from '@heroicons/react/outline'
import StatusContact from '../components/StatusContact'
import {
  accountListState,
  conversationListState,
  currentUserLogin,
  isOpenModalConversation,
  messageListState,
  showConversationState,
  userListState,
} from '../atoms'
import { useRecoilState, useSetRecoilState } from 'recoil'
import Conversation from '../components/Conversation'
import CreateConversation from '../components/CreateConversation'
import { toast } from 'react-toastify'
import Toast from '../components/Toast'
import { decipher } from '../utils'

export default function Chat({ token, me, users }) {
  const setUserList = useSetRecoilState(userListState)
  const setAccounts = useSetRecoilState(accountListState)
  const [user, setCurrentUserInfo] = useRecoilState(currentUserLogin)
  const setOpenConvModal = useSetRecoilState(isOpenModalConversation)
  const setMessages = useSetRecoilState(messageListState)
  const [converst, setShowConversation] = useRecoilState(showConversationState)
  const setConversations = useSetRecoilState(conversationListState)
  const socket = useRef()
  useEffect(() => {
    setUserList(users)
    setAccounts([...users.map((user) => user.accounts[0])])
    setCurrentUserInfo(me)
  }, [
    users,
    setUserList,
    setAccounts,
    setCurrentUserInfo,
    me,
    setShowConversation,
    setMessages,
  ])

  useEffect(() => {
    // connect to socket server
    socket.current = io(process.env.NEXT_PUBLIC_HOST_SERVER)

    // socket disconnet onUnmount if exists
    return () => socket.current.disconnect()
  }, [])

  useEffect(() => {
    socket.current.on('message:comming', (message) => {
      if (
        message.from_account_id === user?.accounts[0]?.id ||
        message.to_account_id === user?.accounts[0]?.id
      ) {
        setConversations((conv) =>
          conv.map((c) => {
            if (c.id === message.conversation_id)
              return { ...c, messages: [message] }
            return c
          })
        )
      }
      if (
        message.from_account_id !== user?.accounts[0]?.id &&
        message.to_account_id === user?.accounts[0]?.id &&
        converst?.id === message.conversation_id
      ) {
        setMessages((msg) => [
          ...msg.filter((mg) => mg.id !== message.id),
          message,
        ])
      }
    })
  }, [
    converst?.id,
    setConversations,
    setMessages,
    setShowConversation,
    user?.accounts,
    users,
  ])

  useEffect(() => {
    socket.current.on('message:comming', (message) => {
      if (me.accounts[0]?.id === message.to_account_id) {
        toast(
          <Toast
            title={
              users.filter(
                (usr) => usr.accounts[0].id === message?.from_account_id
              )[0]?.accounts[0]?.shortName
            }
            message={decipher()(message?.content)}
            type="info"
          />,
          {
            autoClose: 5000,
            closeButton: false,
          }
        )
      }
    })
  }, [me.accounts, users])

  return (
    <div className="flex flex-col fixed inset-0 min-h-screen">
      <NavBar token={token} active="chat" />
      <ModalPost token={token} />
      <CreateConversation token={token} />
      <main className="flex">
        <aside className="hidden lg:flex flex-col flex-shrink min-h-screen relative max-w-[600px] md:min-w-[350px] shadow dark:border-r border-gray-800">
          <div className="flex flex-col p-4 space-y-2 shadow-sm">
            <div className="flex flex-col items-center border-b border-gray-200 dark:border-gray-800 pb-5">
              <div className="flex item-center justify-center">
                <div className="relative rounded-full flex items-center justify-center h-24 w-24 bg-indigo-100 font-bold text-4xl text-gray-800">
                  <span className="h-5 w-5 rounded-full border-4 border-gray-50 dark:border-gray-900 absolute bottom-0 right-1 bg-green-500" />
                  {me.accounts[0].initialName || me.username[0]}
                </div>
              </div>
              <p className="font-semibold text-xl text-gray-800 dark:text-white">
                {me.accounts[0].shortName
                  ? me.accounts[0].shortName
                  : me.username.split(' ').length > 2
                  ? me.username.split(' ')[0] + ' ' + me.username.split(' ')[1]
                  : me.username}
              </p>
            </div>
            <div className="flex flex-col space-y-4 border-b border-gray-200 dark:border-gray-800">
              {/* <div className="flex items-center justify-between text-sm font-bold text-gray-700 dark:text-gray-100 ">
                  <span>Contacts online</span>
                  <span className="text-gray-500 font-semibold">24</span>
                </div> */}
              <div className="flex items-center space-x-2 overflow-scroll scrollbar-hide mb-3 px-5 last:mr-5 max-w-[300px]">
                <StatusContact
                  shortName={me?.accounts[0].shortName}
                  initialName={me?.accounts[0].initialName}
                  username={me.username}
                />
              </div>
            </div>
            <div className="flex flex-col space-y-1">
              <p className="text-sm font-semibold text-gray-700 dark:text-gray-100 flex items-center justify-between">
                <span>Chats</span>
                <button
                  title="New conversation"
                  type="button"
                  onClick={() => setOpenConvModal(true)}
                  className="px-2 py-1 rounded-md outline-none hover:shadow-sm hover:bg-indigo-50 dark:hover:bg-gray-800 flex items-center space-x-1 text-gray-500 hover:text-indigo-500 dark:hover:text-indigo-400 transition duration-300 transform active:scale-95"
                >
                  <PlusIcon className="h-5" />
                </button>
              </p>
              <input
                placeholder="Search chat..."
                className="px-2 py-1 rounded-full shadow dark:shadow-none text-sm focus:outline-none border dark:border-gray-700 focus:border-surfer dark:focus:border-surfer dark:text-white placeholder-gray-400 dark:placeholder-gray-700 dark:bg-gray-800 text-md focus-visible:ring-1 focus-visible:ring-surfer focus-visible:ring-opacity-75"
              />
            </div>
          </div>
          <ListConversation token={token} />
        </aside>
        <Conversation token={token} />
      </main>
    </div>
  )
}

/**
 * @param {import('next').GetServerSidePropsContext} ctx
 */

export async function getServerSideProps({ req }) {
  const { cookies } = req
  if (KEY_COOKIE in cookies) {
    const isValidCookie = await fetch(TOKEN_CHECK_URL, {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.text())
    const v = JSON.parse(isValidCookie)?.errors
    if (v) {
      return {
        redirect: {
          permanent: false,
          destination: '/',
        },
      }
    }
    const me = await fetch(endPointGetSingle('users', 'me'), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())
    const users = await fetch(endPointGetAll('users'), {
      headers: { Authorization: `bearer ${cookies[KEY_COOKIE]}` },
    }).then((res) => res.json())

    return {
      props: {
        token: cookies[KEY_COOKIE],
        me,
        users,
      },
    }
  }

  return {
    redirect: {
      permanent: false,
      destination: '/',
      cookies: '',
    },
  }
}
